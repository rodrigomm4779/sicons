<?php

use App\Http\Controllers\Tenant\Employee\DocumentTypeController;
use Illuminate\Routing\Router;

Route::group(['prefix' => 'app', 'middleware' => 'check_behavior'], function (Router $router) {
    $router->apiResource('document-types', DocumentTypeController::class);
});
