<?php

use App\Http\Controllers\Tenant\Payroll\BeneficiaryBadgeController;
use App\Http\Controllers\Tenant\Payroll\ManualPayrunController;
use App\Http\Controllers\Tenant\Payroll\PayrollSettingController;
use App\Http\Controllers\Tenant\Payroll\PayrunController;
use App\Http\Controllers\Tenant\Payroll\PayslipController;
use App\Http\Controllers\Tenant\Payroll\PayrollCertificateController;
use App\Http\Controllers\Tenant\Payroll\RunDefaultPayrun;
use Illuminate\Routing\Router;

Route::group(['prefix' => 'app', ], function (Router $router) {

    $router->get('settings/payrun', [PayrollSettingController::class, 'index'])
        ->name('payroll-settings.index');

    $router->post('settings/payrun', [PayrollSettingController::class, 'updateDefault'])
        ->name('payrun-period.update');

    $router->post('settings/payrun/audience', [PayrollSettingController::class, 'updateAudience'])
        ->name('payrun-audience.update');

    $router->post('settings/payrun/beneficiaries', [PayrollSettingController::class, 'updateBeneficiaries'])
        ->name('payrun-beneficiary.update');

    $router->apiResource('beneficiaries', BeneficiaryBadgeController::class);

    $router->get('payslip', [PayslipController::class, 'index'])
        ->name('payslips.index');

    $router->get('payslip/{payslip}/send', [PayslipController::class, 'sendPayslip'])
        ->name('individual-payslip.send');

    $router->get('payslip/{payslip}/delete', [PayslipController::class, 'destroy'])
        ->name('payslip.delete');

    $router->get('payslip/{payslip}/pdf', [PayslipController::class, 'showPdf'])
        ->name('payslip-pdf.index');

    $router->patch('payslip/{payslip}/update', [PayslipController::class, 'update'])
        ->name('payslip.update');
    
        //TODO
    $router->post('payslip/manual/store/{employee}', [PayslipController::class, 'manualStore'])
        ->name('payslip-manual.store'); 

        //TODO masive store  
    $router->post('payslip/manual/masive-store', [PayslipController::class, 'masiveStore'])
        ->name('payslip-manual.store'); 

    $router->get('payslip/send-monthly', [PayslipController::class, 'sendMonthlyPayslip'])
        ->name('bulk-payslip.send');

    $router->post('payrun/default', [RunDefaultPayrun::class, 'store'])
        ->name('default-payrun.run');

    $router->post('payrun/manual', [ManualPayrunController::class, 'store'])
        ->name('manual-payrun.run');

    $router->get('payruns', [PayrunController::class, 'index'])
        ->name('payruns.index');

    $router->delete('payruns/{payrun}', [PayrunController::class, 'delete'])
        ->name('payruns.delete');

    $router->get('payruns/show/{uid}', [PayrunController::class, 'show'])
        ->name('payruns.show');

    $router->patch('payruns/{payrun}', [ManualPayrunController::class, 'update'])
        ->name('payruns.update');

    $router->get('payruns/{payrun}', [ManualPayrunController::class, 'index'])
        ->name('payruns.index');

    $router->get('payruns/{payrun}/send-payslip', [PayrunController::class, 'sendPayslips'])
        ->name('payrun-payslips.send');

    $router->get('payroll-certificate', [PayrollCertificateController::class, 'index'])
        ->name('payroll-certificate.index');
    
    $router->post('payroll-certificate/generate', [PayrollCertificateController::class, 'generate'])
        ->name('payroll-certificate.generate');
    
    $router->get('payroll-certificate/view', [PayrollCertificateController::class, 'view'])
        ->name('payroll-certificate.generate');

    $router->get('payroll-certificate/export', [PayrollCertificateController::class, 'export'])
        ->name('payroll-certificate.export');
        
    $router->get('payroll-certificate/{payrollCertificate}/send', [PayrollCertificateController::class, 'sendPayslip'])
        ->name('individual-payroll-certificate.send');

    $router->get('payroll-certificate/{payrollCertificate}/delete', [PayrollCertificateController::class, 'destroy'])
        ->name('payroll-certificate.delete');

    

});