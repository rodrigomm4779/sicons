<?php

use App\Http\Controllers\Tenant\Maintenance\PensionRegimeController;
use App\Http\Controllers\Tenant\Maintenance\DocumentTypeController;
use App\Http\Controllers\Tenant\Maintenance\WorkerTypeController;
use App\Http\Controllers\Tenant\Maintenance\EducationLevelController;
use App\Http\Controllers\Tenant\Maintenance\OccupationalGroupController;
use App\Http\Controllers\Tenant\Maintenance\PublicEmployeeController;
use App\Http\Controllers\Tenant\Maintenance\FinancialEntityController;
use App\Http\Controllers\Tenant\Maintenance\AgencyController;
use App\Http\Controllers\Tenant\Maintenance\WorkerCategoryController;
use App\Http\Controllers\Tenant\Payroll\FinanceSourceController;
use Illuminate\Routing\Router;


Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('pension-regimes', PensionRegimeController::class);
});

Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('document-types', DocumentTypeController::class);
});

Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('worker-types', WorkerTypeController::class);
});

Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('education-levels', EducationLevelController::class);
});

Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('occupational-groups', OccupationalGroupController::class);
});

Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('public-employee-types', PublicEmployeeController::class);
});

Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('financial-entities', FinancialEntityController::class);
});

Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('finance-sources', FinanceSourceController::class);
});

Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('agencies', AgencyController::class);
});

Route::group(['prefix' => 'app', 'middleware' => 'can:view_departments'], function (Router $router) {
    $router->apiResource('worker-categories', WorkerCategoryController::class);
});

