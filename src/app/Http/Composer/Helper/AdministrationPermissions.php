<?php


namespace App\Http\Composer\Helper;


use App\Helpers\Core\Traits\InstanceCreator;

class AdministrationPermissions
{
    use InstanceCreator;

    public function permissions()
    {
        return [
            [
                'name' => __t('users_roles'),
                'url' => $this->userUrl(),
                'permission' => authorize_any(['view_roles'])
            ],
            [
                'name' => 'Centros de trabajo',
                'url' => $this->AgengyUrl(),
                'permission' => auth()->user()->can('view_departments') //TODO PERMISOS
            ],
            // [
            //     'name' => __t('work_shifts'),
            //     'url' => $this->workShiftUrl(),
            //     'permission' => authorize_any(['view_working_shifts'])
            // ],
            [
                'name' => __t('departments'),
                'url' => $this->departmentUrl(),
                'permission' => authorize_any(['view_departments'])
            ],
            // [
            //     'name' => __t('holiday'),
            //     'url' => $this->holidayUrl(),
            //     'permission' => authorize_any(['view_holidays'])
            // ],
            // [
            //     'name' => __t('org_structure'),
            //     'url' => $this->organizationUrl(),
            //     'permission' => auth()->user()->can('view_departments') &&
            //         auth()->user()->can('update_departments')
            // ],
            // [
            //     'name' => __t('announcements'),
            //     'url' => $this->announcementUrl(),
            //     'permission' => auth()->user()->can('view_announcements')
            // ],
            [
                'name' => 'Tipo documentos',
                'url' => $this->documentTypeUrl(),
                'permission' => auth()->user()->can('view_departments') //TODO PERMISOS
            ],
            [
                'name' => 'Régimen Pensionario',
                'url' => $this->pensionRegimeUrl(),
                'permission' => auth()->user()->can('view_departments') //TODO PERMISOS
            ],
            [
                'name' => 'Tipo Plaza',
                'url' => $this->workerTypeUrl(),
                'permission' => auth()->user()->can('view_departments') //TODO PERMISOS
            ],
            [
                'name' => 'Niveles Educativos',
                'url' => $this->educationLevelUrl(),
                'permission' => auth()->user()->can('view_departments') //TODO PERMISOS
            ],
            [
                'name' => 'Grupos Ocupacionales',
                'url' => $this->occupationalGroupUrl(),
                'permission' => auth()->user()->can('view_departments') //TODO PERMISOS
            ],
            [
                'name' => 'Tipos Empleado Público',
                'url' => $this->publicEmployeeTypeUrl(),
                'permission' => auth()->user()->can('view_departments') //TODO PERMISOS
            ],
            [
                'name' => 'Entidades Financieras',
                'url' => $this->FinancialEntityUrl(),
                'permission' => auth()->user()->can('view_departments') //TODO PERMISOS
            ],
            [
                'name' => 'Categorías de Trabajador',
                'url' => route('support.maintenance.worker-categories',optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]),
                'permission' => authorize_any(['view_departments'])
            ],
            [
                'name' => 'Ftes de Financiamiento',
                'url' => route('support.payroll.finance-sources',optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]),
                'permission' => authorize_any(['view_departments'])
            ],
            [
                'name' => __t('employment_status'),
                'url' => route('support.employee.employment-statuses',optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]),
                'permission' => authorize_any(['view_employment_statuses'])
            ],
            
        ];
    }

    public function canVisit()
    {
        return authorize_any(['view_users', 'view_roles', 'view_departments', 'view_working_shifts', 'view_announcements']);
    }

    public function departmentUrl()
    {
        return route(
            'support.employee.departments',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function organizationUrl()
    {
        return route(
            'support.organization.structure',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function workShiftUrl()
    {
        return route(
            'support.employee.work_shifts',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function userUrl()
    {
        return route(
            'support.tenant.users',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function holidayUrl()
    {
        return route(
            'support.employee.holidays',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function announcementUrl()
    {
        return route(
            'support.employee.announcements',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function documentTypeUrl()
    {
        return route(
            'support.administration.document-types',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function pensionRegimeUrl()
    {
        return route(
            'support.maintenance.pension-regimes',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function workerTypeUrl()
    {
        return route(
            'support.maintenance.worker-types',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function educationLevelUrl()
    {
        return route(
            'support.maintenance.education-levels',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function occupationalGroupUrl()
    {
        return route(
            'support.maintenance.occupational-groups',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function publicEmployeeTypeUrl()
    {
        return route(
            'support.maintenance.public-employee-types',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function FinancialEntityUrl()
    {
        return route(
            'support.maintenance.financial-entities',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

    public function AgengyUrl()
    {
        return route(
            'support.maintenance.agencies',
            optional(tenant())->is_single ? '' : ['tenant_parameter' => tenant()->short_name ]
        );
    }

}
