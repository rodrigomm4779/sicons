<?php

namespace App\Http\Controllers\Tenant\Employee;

use App\Filters\Tenant\EmployeeFilter;
use App\Helpers\Traits\AssignRelationshipToPaginate;
use App\Helpers\Traits\DepartmentAuthentications;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tenant\Employee\EmployeeRequest;
use App\Models\Core\Auth\User;
use App\Models\Tenant\Employee\EmploymentStatus;
use App\Models\Tenant\WorkingShift\WorkingShift;
use App\Services\Core\Auth\UserService;
use App\Services\Tenant\Employee\EmployeeService;
use App\Models\Tenant\Payroll\Beneficiary;
use App\Models\Tenant\Maintenance\PensionRegime;
use App\Services\Tenant\Setting\SettingService as TenantSettingService;

use Illuminate\Support\Facades\DB;

use App\Services\Tenant\Payroll\PayslipService;

class EmployeeController extends Controller
{
    use AssignRelationshipToPaginate, DepartmentAuthentications;

    public function __construct(EmployeeService $service, EmployeeFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        $workShift = WorkingShift::getDefault(['id', 'name']);

        if (!request()->get('employment_statuses')) {
            request()->merge(['employment_statuses' => implode(', ', EmploymentStatus::query()
                ->where('alias', '!=', 'terminated')
                ->pluck('id')
                ->toArray())
            ]);
        }

        $paginated = $this->service
            ->filters($this->filter)
            ->with([
                'department:id,name',
                'designation:id,name',
                'profile:id,user_id,joining_date,employee_id,document_type_id,document_number,ruc_number,entry_date',
                'profile.documentType',
                'profilePicture',
                'workingShift:id,name',
                'employmentStatus:id,name,class,alias',
                'roles:id,name',
                'status',
                'updatedSalary',
                'salary',
                'payrollInformation:id,user_id,pension_regime_id,cussp,commission,days_worked',
                // 'payrollInformation.pensionRegime',
                'payrollInformation.pensionRegime' => function ($query) {
                    $query->select('id', 'code', 'name', 'type');
                },
            ])->where('is_in_employee', 1)
            ->latest('id')
            ->paginate(request()->get('per_page', 10));

        return $this->paginated($paginated)
            ->setRelation(function (User $user) use ($workShift) {
                if (!$user->workingShift) {
                    $user->setRelation('workingShift', $workShift);
                }
            })->get();
    }

    public function show(User $employee)
    {
         $employee->load([
            'department:id,name',
            'designation:id,name',
            // 'profile:id,user_id,joining_date,employee_id,gender,date_of_birth,about_me,phone_number',
            'profile',
            'profilePicture',
            'workingShift:id,name',
            'employmentStatus:id,name,class,alias',
            'roles:id,name',
            'status',
            'updatedSalary',
            'salary',
            'payrollInformation',
            'addresses' => function ($query) {
                $query->where('key', 'present_address');
            },
        ]);

        if (!$employee->workingShift) {
            $workShift = WorkingShift::getDefault(['id', 'name', 'is_default']);
            $employee->setRelation('workingShift', $workShift);
        }
        return $employee;
    }

    //TODO Actualiza personal
    public function update(EmployeeRequest $request, User $employee)
    {
        $this->departmentAuthentications($employee->id);

        resolve(UserService::class)->validateIsNotDemoVersion();

        DB::transaction(function () use($employee, $request){

            //Obtener configuración del sistema
            $settings = (object)resolve(TenantSettingService::class)->getFormattedTenantSettings();

            $this->service
                ->setModel($employee)
                ->setAttributes($request->except('allowed_resource', 'tenant_id', 'tenant_short_name'))
                ->update();
            
            // Insertar conceptos
            // Ingresos afectos a ley

            $allowances = [];
            $allowanceValues = [];
            $allowancePercentages = [];
            $deductions = [];
            $deductionValues = [];
            $deductionPercentages = [];
            $contributions = [];
            $contributionValues = [];
            $contributionPercentages = [];

            // 1. HONORARIOS
            if($request->input('salary')){
                $benef_honorarios = Beneficiary::query()->where('affection_law', 'HONORARIOS')->first();

                if ($benef_honorarios) {
                    // Si se encuentra un concepto, se procede a agregar los valores correspondientes a los arrays
                    $allowances[] = $benef_honorarios->id;
                    $allowanceValues[] = floatval($request->input('salary'));
                    $allowancePercentages[] = false;
                }
            }

            // 2. REGIMEN LABORAL
            if($request->input('labor_regime_id')){
                $benef_ds311 = Beneficiary::query()->where('affection_law', 'DS311')->first();
                $amount_ds311 = floatval($settings->cant_incremento_ds311 ?? 0);

                if ($benef_ds311) {
                    $allowances[] = $benef_ds311->id;
                    $allowanceValues[] = $amount_ds311;
                    $allowancePercentages[] = false;
                }
            }

            // 3. CUARTA CAT
            if(!$request->input('have_suspension_4ta')){
                $benef_4tacat = Beneficiary::query()->where('affection_law', 'CUARTA CAT')->first();
                $percent_4tacat = floatval($settings->porcentaje_retencion_4TACAT ?? 0);

                if ($benef_4tacat) {
                    $deductions[] = $benef_4tacat->id;
                    $deductionValues[] = $percent_4tacat;
                    $deductionPercentages[] = true;
                }
            }

            // 4. AFP/ONP
            if($request->input('pension_regime_id')){

                $pension_regime = PensionRegime::findOrFail($request->input('pension_regime_id'));
                
                // Si es ONP
                if ($pension_regime->type == 'SNP') {
                    //Obtener ID concepto ONP
                    $benef_snp = Beneficiary::query()->where('affection_law', 'SNP')->first();

                    $deductions[] = $benef_snp->id;
                    $deductionValues[] = $pension_regime->aporte_obligatorio;
                    $deductionPercentages[] = true; //Es porcentaje
                }
                // Si es SPP
                if ($pension_regime->type == 'SPP') {
                    $benef_afp = Beneficiary::query()->where('affection_law', 'AFP')->first();
                    //Aporte obligatorio
                    $deductions[] = $benef_afp->id;
                    $deductionValues[] = $pension_regime->aporte_obligatorio;
                    $deductionPercentages[] = true; //Es porcentaje

                    $benef_afp_seguro = Beneficiary::query()->where('affection_law', 'AFP SEGURO')->first();
                    //Prima de seguro
                    $deductions[] = $benef_afp_seguro->id;
                    $deductionValues[] = $pension_regime->prima_seguro;
                    $deductionPercentages[] = true; //Es porcentaje

                    if($request->input('commission') == 'flujo'){
                        $benef_afp_com = Beneficiary::query()->where('affection_law', 'AFP COM VAR')->first();
                        //Comision sobre flujo
                        $deductions[] = $benef_afp_com->id;
                        $deductionValues[] = $pension_regime->comision_flujo;
                        $deductionPercentages[] = true; //Es porcentaje
                    }
                    //else Comisión mixta es anual se encarga la AFP
                }

            }

            // . DESCUENTO POR FALTAS
            // if($request->input('days_absent') > 0 ){
            //     $benef_dsct_faltas = Beneficiary::query()->where('affection_law', 'DSCT FALTAS')->first();
            //     $amount_xday = floatval($request->input('salary'))/floatval($request->input('days_worked'));
            //     $amount_dscto = round($amount_xday * floatval($request->input('days_absent')), 2);

            //     if ($benef_dsct_faltas) {
            //         $deductions[] = $benef_dsct_faltas->id;
            //         $deductionValues[] = $amount_dscto;
            //         $deductionPercentages[] = false; // Es monto
            //     }
            //     // dd($amount_dscto);
            // }

            // 5. REG SALUD
            if($request->input('have_health_insurance')){
                $benef_essalud = Beneficiary::query()->where('affection_law', 'ESSALUD')->first();
                $percent_essalud = floatval($settings->porcentaje_aporte_essalud ?? 0);

                if ($benef_essalud) {
                    $contributions[] = $benef_essalud->id;
                    $contributionValues[] = $percent_essalud;
                    $contributionPercentages[] = true; //Es porcentaje
                }
            }
            
            resolve(PayslipService::class)
            ->setModel($employee)
            ->setAttributes([
                'allowances' => $allowances,
                'allowanceValues' => $allowanceValues,
                'allowancePercentages' => $allowancePercentages,
                'deductions' => $deductions,
                'deductionValues' => $deductionValues,
                'deductionPercentages' => $deductionPercentages,
                'contributions' => $contributions,
                'contributionValues' => $contributionValues,
                'contributionPercentages' => $contributionPercentages,
            ])
            ->updateBeneficiaries('payrunBeneficiaries');

        });

        // // Insertar conceptos
        // resolve(PayslipService::class)
        // ->setModel($employee)
        // ->setAttributes(\request()->only([
        //     'allowances',
        //     'allowanceValues',
        //     'allowancePercentages',
        //     'deductions',
        //     'deductionValues',
        //     'deductionPercentages',
        //     'contributions',
        //     'contributionValues',
        //     'contributionPercentages', 
        // ]))
        // ->updateBeneficiaries('payrunBeneficiaries');

        return updated_responses('employee');
    }

    public function destroy(User $employee)
    {
        $this->service
            ->setModel($employee)
            ->delete();

        return deleted_responses('employee');
    }

}
