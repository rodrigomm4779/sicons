<?php


namespace App\Http\Controllers\Tenant\Employee;


use App\Exceptions\GeneralException;
use App\Hooks\User\AfterUserConfirmed;
use App\Hooks\User\AfterUserInvited;
use App\Hooks\User\BeforeUserInvited;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tenant\Employee\EmployeeRequest;
use App\Mail\Tenant\EmployeeInvitationCancelMail;
use App\Models\Core\Auth\Role;
use App\Models\Core\Auth\User;
use App\Notifications\Core\User\UserInvitationNotification;
use App\Services\Tenant\Employee\EmployeeInviteService;
use App\Services\Tenant\Employee\EmployeeContactService;
use App\Models\Tenant\Payroll\Beneficiary;
use App\Models\Tenant\Maintenance\PensionRegime;
use App\Services\Tenant\Setting\SettingService as TenantSettingService;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\Services\Tenant\Payroll\PayslipService;

class EmployeeInviteController extends Controller
{
    protected AfterUserInvited $afterUserInvited;

    protected BeforeUserInvited $beforeUserInvited;

    public function __construct(EmployeeInviteService $service, BeforeUserInvited $beforeUserInvited, AfterUserInvited $afterUserInvited, EmployeeContactService $contactService)
    {
        $this->service = $service;
        $this->afterUserInvited = $afterUserInvited;
        $this->beforeUserInvited = $beforeUserInvited;
        $this->contactService = $contactService;
    }

    public function invite(EmployeeRequest $request)
    {
        DB::transaction(function () use ($request) {
            $this->beforeUserInvited
                ->handle();

            $user = $this->service
                ->setAttributes($request->except('allowed_resource', 'tenant_id', 'tenant_short_name'))
                ->when(!auth()->user()->can('attach_users_to_roles'), function (EmployeeInviteService $service){
                    $employeeRoleId = Role::query()
                        ->where('alias', 'employee')
                        ->get()
                        ->pluck('id')
                        ->toArray();
                    $service->setAttr('roles', $employeeRoleId);
                })->validateRoles()
                ->validateMailSettings()
                ->invite();

            $this->afterUserInvited
                ->setModel($user)
                ->cacheQueueClear();

            notify()
                ->on('employee_invited')
                ->with($user)
                ->send(UserInvitationNotification::class);

            $this->afterUserInvited
                ->setModel($user)
                ->handle();
        });

        return response()->json([
            'status' => true,
            'message' => trans('default.invite_employee_response')
        ]);
    }

    public function create(EmployeeRequest $request)
    {

        // dd($request);
        // Iniciar una transacción de base de datos
        DB::transaction(function () use ($request) {
            
            //Obtener configuración del sistema
            $settings = (object)resolve(TenantSettingService::class)->getFormattedTenantSettings();

            // Lógica previa a la invitación del usuario
            $this->beforeUserInvited->handle();

            // Crear un usuario con los atributos dados, excluyendo algunos específicos
            $user = $this->service
                ->setAttributes($request->except('allowed_resource', 'tenant_id', 'tenant_short_name'))
                // Asignar rol de 'employee' si el usuario autenticado no puede adjuntar usuarios a roles
                ->when(!auth()->user()->can('attach_users_to_roles'), function (EmployeeInviteService $service){
                    $employeeRoleId = Role::query()
                        ->where('alias', 'employee')
                        ->get()
                        ->pluck('id')
                        ->toArray();
                    $service->setAttr('roles', $employeeRoleId);
                })
                // Validar roles y configuraciones de correo electrónico
                ->validateRoles()
                ->validateMailSettings()
                // Crear el usuario
                ->create();

            // Lógica posterior a la invitación del usuario
            $this->afterUserInvited
                ->setModel($user)
                ->cacheQueueClear();

            // Manejar acciones después de que el usuario haya sido confirmado
            AfterUserConfirmed::new()
                ->setModel($user)
                ->handle();
            
            // Insertar dirección
            $roadType = $request->input('address.road_type');
            $details = $request->input('address.details');
            $area = $request->input('address.area');
            $city = $request->input('address.city');
            $type = $request->input('address.type');
            $state = $request->input('address.state');
            $zipCode = $request->input('address.zip_code');
            $country = $request->input('address.country');
            $phoneNumber = $request->input('address.phone_number');
            $countryCode = $request->input('address.country_code');
            $this->contactService
                ->setAttributes([
                    'road_type' => $roadType,
                    'details' => $details,
                    'area' => $area,
                    'city' => $city,
                    'type' => $type,
                    'state' => $state,
                    'zip_code' => $zipCode,
                    'country' => $country,
                    'phone_number' => $phoneNumber,
                    'country_code' => $countryCode,
                ])
                // ->validateAddress()
                ->setModel($user)
                ->updateAddressJSON();

        });

        // Devolver una respuesta JSON indicando el éxito de la operación
        return response()->json([
            'status' => true,
            'message' => trans('default.create_employee_response')
        ]);
    }
    
    public function cancel(User $employee)
    {
        throw_if(
            !$employee->isInvited(),
            new GeneralException(__t('action_not_allowed'))
        );

        DB::transaction(function () use ($employee) {
             $this->service
                 ->setModel($employee)
                 ->cancel();

             Mail::to($employee->email)
                 ->send((new EmployeeInvitationCancelMail((object)$employee->toArray()))->delay(5));
        });

        return response()->json(['status' => true, 'message' => __t('employee_invitation_canceled_successfully')]);
    }
}