<?php

namespace App\Http\Controllers\Tenant\Employee;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\DocumentTypeFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Administration\DocumentType;
use App\Services\Tenant\Administration\DocumentTypeService;
use Illuminate\Http\Request;
use function PHPUnit\Framework\throwException;

class DocumentTypeController extends Controller
{
    public function __construct(DocumentTypeService $service, DocumentTypeFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return DocumentType::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('name', 'description', 'is_enabled', 'airhsp_code'))
            ->validate()
            ->save();

        return created_responses('leave_type');
    }


    public function show(DocumentType $documentType)
    {
        return $documentType;
    }

    public function update(Request $request, DocumentType $documentType)
    {
        $this->service
            ->setModel($documentType)
            ->setAttributes($request->only('name', 'description', 'is_enabled', 'airhsp_code'))
            ->validate()
            ->save();

        return updated_responses('leave_type');
    }


    public function destroy(DocumentType $documentType)
    {
        try {
            $documentType->delete();
        } catch (\Exception $e) {
            throw new GeneralException(__t('can_not_delete_used_leave_type'));
        }

        return deleted_responses('leave_type');
    }

}
