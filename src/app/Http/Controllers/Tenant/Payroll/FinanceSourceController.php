<?php

namespace App\Http\Controllers\Tenant\Payroll;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\FinanceSource;
use App\Services\Tenant\Maintenance\FinanceSourceService;
use Illuminate\Http\Request;

class FinanceSourceController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(FinanceSourceService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return FinanceSource::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name', 'description', 'is_active'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Fuente de Financiamiento created successfully'], 201);
    }

    public function show(FinanceSource $financeSource)
    {
        return response()->json($financeSource);
    }

    public function update(Request $request, FinanceSource $financeSource)
    {
        $this->service
            ->setModel($financeSource)
            ->setAttributes($request->only('code', 'name', 'description', 'is_active'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Fuente de Financiamiento updated successfully']);
    }

    public function destroy(FinanceSource $financeSource)
    {
        try {
            $financeSource->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete Fuente de Financiamiento');
        }

        return response()->json(['message' => 'Fuente de Financiamiento deleted successfully']);
    }
}