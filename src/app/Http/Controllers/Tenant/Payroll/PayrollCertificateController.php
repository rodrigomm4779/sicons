<?php

namespace App\Http\Controllers\Tenant\Payroll;

use App\Exceptions\GeneralException;
use App\Export\AllPayslipExport;
use App\Filters\Tenant\PayrollCertificateFilter;
use App\Helpers\Traits\ConflictedPayslipQueryHelpers;
use App\Helpers\Traits\DateRangeHelper;
use App\Helpers\Traits\SettingKeyHelper;
use App\Helpers\Traits\TenantAble;
use App\Http\Controllers\Controller;
use App\Jobs\Tenant\SendPayslipJob;
use App\Models\Tenant\Payroll\Payslip;
use App\Models\Tenant\Payroll\PayrollCertificate;
use App\Models\Core\Auth\User;
use App\Models\Tenant\Payroll\Beneficiary;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Repositories\Core\Setting\SettingRepository;
use App\Repositories\Core\Status\StatusRepository;
use App\Services\Tenant\Payroll\PayrollCertificateService;

use App\Services\Tenant\Setting\SettingService as TenantSettingService;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Requests\Tenant\Payroll\PayrollCertificateRequest;

class PayrollCertificateController extends Controller
{
    use DateRangeHelper, SettingKeyHelper, TenantAble, ConflictedPayslipQueryHelpers;

    public function __construct(PayrollCertificateService $service, PayrollCertificateFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
        // $this->employeeInviteService = $employeeInviteService;
    }

    public function index()
    {

        $within = request()->get('within');
        $month = $within ?: request('month_number') + 1;
        $ranges = $this->convertRangesToStringFormat($this->getStartAndEndOf($month, request()->get('year')));

        // return $ranges;

        if ($within == 'total' && PayrollCertificate::query()->exists()) {
            $min_date = PayrollCertificate::query()->oldest('start_date')->first()->start_date;
            $max_date = PayrollCertificate::query()->latest('end_date')->first()->end_date;
            $ranges = [$min_date, $max_date];
        }

        // return $ranges;

        $payslips = $this->service
            ->filters($this->filter)
            ->with($this->service->getRelations())
            // ->whereBetween(DB::raw('DATE(start_date)'), count($ranges) == 1 ? [$ranges[0], $ranges[0]] : $ranges)
            // ->whereBetween(DB::raw('DATE(end_date)'), count($ranges) == 1 ? [$ranges[0], $ranges[0]] : $ranges)
            ->latest()
            ->paginate(request()->get('per_page', 10));

        return $payslips;
    }


    public function view(Request $request){


        $user_id = 294;
        $ref_exp_number = '';
        $issue_date =  date("Y/m/d");
        $carbonFecha = Carbon::createFromFormat('Y/m/d', $issue_date);
        $issue_date_formated = $carbonFecha->isoFormat('D [de] MMMM [del] YYYY');
        $name = 'CONSTANCIA DE PAGOS, DESCUENTOS Y APORTACIÓN A LA DERRAMA ADMINISTRATIVA';
        $allowances = []; //$request->get('allowances'); // IDs de las allowances seleccionadas
        $deductions = [31]; //32 derrama $request->get('deductions'); // IDs de las deductions seleccionadas
        $token = uniqid(bin2hex(random_bytes(5)), true);
        $filename = $token . '.pdf';

        // $li_values = ['a)', 'b)', 'c)'];

        
        // $user = User::with('profile', 'payrollInformation.designation')->where('id', $user_id)->get();
        $remuneraciones = Beneficiary::whereIn('id', $allowances)->get();
        $descuentos = Beneficiary::whereIn('id', $deductions)->get();
        
        //Generar encabezado 
        $headRemuneraciones = [];
        $headDescuentos = [];

        $contador = 0;
        foreach ($remuneraciones as $remuneracion) {
            $letra = chr(97 + $contador); // Convertir el número en el código ASCII correspondiente a la letra
            $idRemuneracion = $remuneracion->id;
            $headRemuneraciones[$idRemuneracion] = strtoupper($letra) . ')';
            $contador++;
        }

        $contador = 0;
        foreach ($descuentos as $descuento) {
            $letra = chr(97 + $contador); // Convertir el número en el código ASCII correspondiente a la letra
            $idDescuento = $descuento->id;
            $headDescuentos[$idDescuento] = strtoupper($letra) . ')';
            $contador++;
        }

        // return $headDescuentos;

        $payslips = Payslip::with(['beneficiaries' => function ($query) use ($allowances, $deductions) {
            $query->with('beneficiary');
        }])
        ->where('user_id', $user_id)
        ->orderBy('start_date', 'asc')
        ->get();

        $user = $payslips->first();

        // Tratamiento de datos a payslips
        $payslips->map(function ($payslip) use ($allowances, $deductions) {
            // Dividir el atributo mes_periodo en mes y año
            [$mes, $anio] = explode(' - ', $payslip->mes_periodo);
            $payslip->mes = $mes;
            $payslip->anio = $anio;

            // Filtrar los allowances según los IDs de allowances
            $payslip->allowances_filtered = $payslip->beneficiaries->filter(function ($beneficiary) use ($allowances) {
                return in_array($beneficiary->beneficiary_id, $allowances);
            });

            $payslip->total_amount_filtered = $payslip->allowances_filtered->sum('amount');

            // Restar la diferencia entre total_amount_filtered y total_bruto
            $payslip->otras_remunerac = round($payslip->total_bruto - $payslip->total_amount_filtered, 2);
            
            // Filtrar los beneficiarios según los IDs de deductions
            $payslip->deductions_filtered = $payslip->beneficiaries->filter(function ($beneficiary) use ($deductions) {
                return in_array($beneficiary->beneficiary_id, $deductions);
            });

            
            return $payslip;
        });

        // return $payslips;

        // return view('tenant.payroll.pdf.payroll-certificate', compact(
        //     'payslips',
        //     'user',
        //     'name',
        //     'ref_exp_number',
        //     'issue_date',
        //     'issue_date_formated',
        //     'remuneraciones',
        //     'descuentos',
        //     'headRemuneraciones',
        //     'headDescuentos',
        // ));

        // PDF::setOptions(['isHtml5ParserEnabled' => true]);

        $pdf = PDF::loadView('tenant.payroll.pdf.payroll-certificate', compact(
            'payslips',
            'user',
            'name',
            'ref_exp_number',
            'issue_date',
            'issue_date_formated',
            'remuneraciones',
            'descuentos',
            'headRemuneraciones',
            'headDescuentos',
        ));

        
        return $pdf->stream();


        // $pdf->save(storage_path('app/public/payroll-certificate/' . $user_id .'_'.$token. '.pdf'));

        // return $payslips;
    }

    //TODO Generar pdf de la constancia
    public function generate(PayrollCertificateRequest $request)
    {   
        
        $name = $request->get('name');
        $ref_exp_number = $request->get('ref_exp_number');
       
        $issue_date =  $request->get('issue_date');
        $carbonFecha = Carbon::createFromFormat('Y-m-d', $issue_date);
        $issue_date_formated = $carbonFecha->isoFormat('D [de] MMMM [del] YYYY');
        $user_id = $request->get('user_id');
        $allowances = $request->get('allowances');
        $deductions = $request->get('deductions');
        $token = uniqid(bin2hex(random_bytes(5)), true);
        $filename = $token . '.pdf';
        //Generar encabezado 
        $headRemuneraciones = [];
        $headDescuentos = [];


        // $user = User::with('profile', 'payrollInformation.designation')->where('id', $user_id)->get();


        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        // Verifica si tanto $start_date como $end_date son fechas válidas
        if (strtotime($start_date) !== false && strtotime($end_date) !== false) {
            $payslips = Payslip::with(['beneficiaries' => function ($query) use ($allowances, $deductions) {
                $query->with('beneficiary');
            }])
            ->where('user_id', $user_id)
            ->whereBetween('start_date', [$start_date, $end_date])
            ->orderBy('start_date', 'asc')
            ->get();
        } else {
            $payslips = Payslip::with(['beneficiaries' => function ($query) use ($allowances, $deductions) {
                $query->with('beneficiary');
            }])
            ->where('user_id', $user_id)
            ->orderBy('start_date', 'asc')
            ->get();
        }

        $user = $payslips->first();
        if ($user === null) {
            return response()->json(['message' => 'No se encontraron boletas de pago para el periodo selecionado.'], 404);
        }

        $remuneraciones = Beneficiary::whereIn('id', $allowances)->get();
        $descuentos = Beneficiary::whereIn('id', $deductions)->get();

        $contador = 0;
        foreach ($remuneraciones as $remuneracion) {
            $letra = chr(97 + $contador); // Convertir el número en el código ASCII correspondiente a la letra
            $idRemuneracion = $remuneracion->id;
            $headRemuneraciones[$idRemuneracion] = strtoupper($letra) . ')';
            $contador++;
        }

        $contador = 0;
        foreach ($descuentos as $descuento) {
            $letra = chr(97 + $contador); // Convertir el número en el código ASCII correspondiente a la letra
            $idDescuento = $descuento->id;
            $headDescuentos[$idDescuento] = strtoupper($letra) . ')';
            $contador++;
        }

        // Tratamiento de datos a payslips
        $payslips->map(function ($payslip) use ($allowances, $deductions) {
            // Dividir el atributo mes_periodo en mes y año
            [$mes, $anio] = explode(' - ', $payslip->mes_periodo);
            $payslip->mes = $mes;
            $payslip->anio = $anio;

            // Filtrar los allowances según los IDs de allowances
            $payslip->allowances_filtered = $payslip->beneficiaries->filter(function ($beneficiary) use ($allowances) {
                return in_array($beneficiary->beneficiary_id, $allowances);
            });

            $payslip->total_amount_filtered = $payslip->allowances_filtered->sum('amount');

            // Restar la diferencia entre total_amount_filtered y total_bruto
            $payslip->otras_remunerac = round($payslip->total_bruto - $payslip->total_amount_filtered, 2);
            
            // Filtrar los beneficiarios según los IDs de deductions
            $payslip->deductions_filtered = $payslip->beneficiaries->filter(function ($beneficiary) use ($deductions) {
                return in_array($beneficiary->beneficiary_id, $deductions);
            });

            
            return $payslip;
        });
        
        
        $pdf = PDF::loadView('tenant.payroll.pdf.payroll-certificate', compact(
            'payslips',
            'user',
            'name',
            'ref_exp_number',
            'issue_date',
            'issue_date_formated',
            'remuneraciones',
            'descuentos',
            'headRemuneraciones',
            'headDescuentos',
        ));

        
        $pdf->save(storage_path('app/public/payroll-certificate/' . $user_id .'_'.$token. '.pdf'));
        
        // Crear una nueva instancia del modelo PayrollCertificate con los datos recibidos
        $data = $request->all();
        $payrollCertificate = new PayrollCertificate();
        $payrollCertificate->agency_id = $data['agency_id'];
        $payrollCertificate->allowances = json_encode($data['allowances']);
        $payrollCertificate->certificate_type = $data['certificate_type'];
        $payrollCertificate->deductions = json_encode($data['deductions']);
        $payrollCertificate->description = $data['description'];
        $payrollCertificate->designation_id = $data['designation_id'];
        $payrollCertificate->end_date = $data['end_date'];
        $payrollCertificate->filename = $user_id .'_'.$token. '.pdf';
        $payrollCertificate->generate_user_id = auth()->id();
        $payrollCertificate->legend = $data['legend'];
        $payrollCertificate->mag = $data['mag'];
        $payrollCertificate->name = $data['name'];
        $payrollCertificate->niv = $data['niv'];
        $payrollCertificate->start_date = $data['start_date'];
        $payrollCertificate->issue_date = $data['issue_date'];
        $payrollCertificate->ref_exp_number = $data['ref_exp_number'];
        $payrollCertificate->token = $token;
        $payrollCertificate->user_id = $data['user_id'];
        $payrollCertificate->save();

        return response()->json(['message' => 'Constancia generada con éxtio.'], 201);

    }

    public function destroy(PayrollCertificate $payrollCertificate)
    {
        $payrollCertificate->delete();

        return deleted_responses('payrollCertificate');
    }

    public function export(Request $request) {
        
        $id = $request->get('id');
    
        $certificate = PayrollCertificate::where('token', $id)->first();
    
        if (!$certificate) {
            return response()->json(['error' => 'La constancia no existe'], 404);
        }
    
        $filename = $certificate->filename;
    
        // $rutaArchivo = public_path('storage/payroll-certificate/' . $filename);
        $rutaArchivo = storage_path('app/public/payroll-certificate/'. $filename);

        // Verifica si el archivo existe y si tiene una extensión PDF
        if (File::exists($rutaArchivo)) {
            // Descarga el archivo PDF
            return response()->file($rutaArchivo, ['Content-Type' => 'application/pdf']);
        } else {
            // Si el archivo no es un PDF o no existe, devuelve una respuesta de error
            return response()->json(['error' => 'El archivo no existe o no es un PDF'], 404);
        }
    }
    
}
