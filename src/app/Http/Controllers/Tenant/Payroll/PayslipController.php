<?php

namespace App\Http\Controllers\Tenant\Payroll;

use App\Exceptions\GeneralException;
use App\Export\AllPayslipExport;
use App\Filters\Tenant\PayslipFilter;
use App\Helpers\Traits\ConflictedPayslipQueryHelpers;
use App\Helpers\Traits\DateRangeHelper;
use App\Helpers\Traits\SettingKeyHelper;
use App\Helpers\Traits\TenantAble;
use App\Http\Controllers\Controller;
use App\Jobs\Tenant\SendPayslipJob;
use App\Models\Tenant\Payroll\Payslip;
use App\Models\Core\Auth\User;
use App\Models\Tenant\Payroll\Beneficiary;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Repositories\Core\Setting\SettingRepository;
use App\Repositories\Core\Status\StatusRepository;
use App\Services\Tenant\Payroll\PayrunService;
use App\Services\Tenant\Payroll\ManualPayrollService;
use App\Services\Tenant\Payroll\PayslipService;
use App\Services\Tenant\Employee\EmployeeInviteService;


use App\Services\Tenant\Setting\SettingService as TenantSettingService;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
// use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\File;

class PayslipController extends Controller
{
    use DateRangeHelper, SettingKeyHelper, TenantAble, ConflictedPayslipQueryHelpers;

    public PayrunService $payrunService;

    public function __construct(PayslipService $service, PayslipFilter $filter, PayrunService $payrunService, ManualPayrollService $manualPayrollService, EmployeeInviteService $employeeInviteService)
    {
        $this->service = $service;
        $this->filter = $filter;
        $this->payrunService = $payrunService;
        $this->manualPayrollService = $manualPayrollService;
        $this->employeeInviteService = $employeeInviteService;
    }

    public function index()
    {
        $within = request()->get('within');
        $month = $within ?: request('month_number') + 1;
        $ranges = $this->convertRangesToStringFormat($this->getStartAndEndOf($month, request()->get('year')));

        if ($within == 'total' && Payslip::query()->exists()) {
            $min_date = Payslip::query()->oldest('start_date')->first()->start_date;
            $max_date = Payslip::query()->latest('end_date')->first()->end_date;
            $ranges = [$min_date, $max_date];
        }

        $payslips = $this->service
            ->filters($this->filter)
            ->with($this->service->getRelations())
            ->whereBetween(DB::raw('DATE(start_date)'), count($ranges) == 1 ? [$ranges[0], $ranges[0]] : $ranges)
            ->whereBetween(DB::raw('DATE(end_date)'), count($ranges) == 1 ? [$ranges[0], $ranges[0]] : $ranges)
            ->when(request()->has('conflicted') && request()->get('conflicted') == 'true', function (Builder $builder){
                $builder->whereIn('id', $this->getConflictedPayslip());
            })->latest()
            ->paginate(request()->get('per_page', 10));

        $payslips->map(function ($payslip){
            $conflictedData = $this->conflictedUserPayslip($payslip, $payslip->start_date, $payslip->end_date);
            $payslip->conflicted = $conflictedData->count();
        });

        return $payslips;
    }

    public function exportAllPayslip(){
        $payslips = $this->service
            ->filters($this->filter)
            ->with($this->service->getRelations())->latest()
            ->get();

        $payrunId= request()->has('payrun')?request()->get('payrun').'-':'';
        $file_name = 'payslip-'.$payrunId.date('Y-m-d').'.xlsx';

        return (new AllPayslipExport($payslips))->download($file_name, Excel::XLSX);
    }

    public function exportAllPayslipMCPPWeb(){

        //Obtenemos configuración de la entidad
        $settings = (object)resolve(TenantSettingService::class)
            ->getFormattedTenantSettings();
        
        $payslips = $this->service
            ->filters($this->filter)
            ->with($this->service->getRelations())->latest()
            ->get();
        
        // return $payslips;

        $firstPayslip = $payslips->first();

        if ($firstPayslip) {
            $payrun = $firstPayslip->payrun;

            // return $payrun;
            // Ahora puedes trabajar con $payrun

            // GENERAR TXT PLANILLA TIPO C - CAS
            // 1. Información para construir el nombre del archivo
            $prefijo = 'PLL';
            $codigoEjecutora = $settings->tenant_ue_code ?? '000000';
            $anio = $payrun->process_year ?? '-';
            $mes = $payrun->process_month ?? '-';
            $tipoPlanilla = $payrun->payrollType->code ?? '-';
            $clasePlanilla = $payrun->payrollClass->code ?? '-';
            $correlativo = '0001';
            $extension = 'txt';

            // Construye el nombre del archivo según las reglas
            $nombreArchivo = "{$prefijo}{$codigoEjecutora}{$anio}{$mes}{$tipoPlanilla}{$clasePlanilla}{$correlativo}.{$extension}";

            // Crea el archivo .TXT
            // Verificar si el archivo existe antes de eliminarlo
            if (File::exists(public_path('storage/mcpp-web/' . $nombreArchivo))) {
                File::delete(public_path('storage/mcpp-web/' . $nombreArchivo));
            }
            
            $rutaArchivo = public_path('storage/mcpp-web/' . $nombreArchivo); 

            // 2. Construye la primera línea de contenido del archivo
            $numeroRegistros = $payslips->count() ?? 0;
            // $montoIngresos = floatval($payrun->total_income_amount ?? 0);
            // $montoDescuentos = floatval($payrun->total_deductions_amount ?? 0);
            // $montoAportes = floatval($payrun->total_contributions_amount ?? 0);
            $montoIngresos = 0;
            $montoDescuentos = 0;
            $montoAportes = 0;

            foreach($payslips as $payslip) {
                //Realiza sumatoria de totales de boletas
                $montoIngresos += floatval($payslip->total_bruto ?? 0);
                $montoDescuentos += floatval($payslip->total_dscto ?? 0);
                $montoAportes += floatval($payslip->total_aporte ?? 0);
            }

            // 2.1. Establece la primera linea del txt
            $linea1Contenido = sprintf(
                "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
                $codigoEjecutora,
                $anio,
                $mes,
                $tipoPlanilla,
                $clasePlanilla,
                $correlativo,
                $numeroRegistros,
                $montoIngresos,
                $montoDescuentos,
                $montoAportes
            );

            File::prepend($rutaArchivo, $linea1Contenido);

            // 3. Contenido desde segunda linea
            foreach ($payslips as $payslip) {
                // Obtén los valores de cada propiedad del elemento actual
                $tipoDoc = $payslip->airhsp_tipo_doc_codigo;
                $numDoc = $payslip->nro_doc;
                $fuenteFinanciamiento = $payrun->financeSource->code ?? '-';

                foreach ($payslip->beneficiaries as $beneficiary) {

                    if($beneficiary->beneficiary->type == 'allowance'){
                        $tipoConceptoAIRHSP = 1;
                    }
                    if($beneficiary->beneficiary->type == 'deduction'){
                        $tipoConceptoAIRHSP = 2;
                    }
                    if($beneficiary->beneficiary->type == 'contribution'){
                        $tipoConceptoAIRHSP = 3;
                    }

                    // conceptAirshp

                    $codigoConceptoAIRHSP = $beneficiary->beneficiary->conceptAirshp->concept_code ?? '-';

                    // dd($payslip);

                    $descripConceptoUE = $beneficiary->beneficiary->conceptAirshp->name ?? '-';
                    $monto =$beneficiary->amount;
                    $codTipoRegAIRHSP = $payslip->airhsp_tipo_reg_codigo ?? '-'; // TODO desde payroll_information
                    $numRegistroAIRHSP = $payslip->airhsp_num_reg ?? '-'; // TODO desde payroll_information

                    // Formatea la línea con los valores obtenidos
                    $lineaContenido = sprintf(
                        "%s|%s|%s|%s|%s|%s|%s|%s|%s\n", // Añade un salto de línea al final
                        $tipoDoc,
                        $numDoc,
                        $fuenteFinanciamiento,
                        $tipoConceptoAIRHSP,
                        $codigoConceptoAIRHSP,
                        $descripConceptoUE,
                        $monto,
                        $codTipoRegAIRHSP,
                        $numRegistroAIRHSP
                    );
                
                    // Añade la línea al archivo
                    // Usa `File::append` para añadir cada línea en lugar de sobrescribir el archivo
                    File::append($rutaArchivo, $lineaContenido);

                }
                
            }



        } else {
            // Manejar la situación en la que no hay elementos en la colección
        }

        // return $payslips;
        // sleep(5);

        // Devuelve el archivo como respuesta
        return response()->download($rutaArchivo, $nombreArchivo, ['Content-Type' => 'text/plain']);


        // --------------------------------------------------

        // $payslips = $this->service
        //     ->filters($this->filter)
        //     ->with($this->service->getRelations())->latest()
        //     ->get();

        // $payrunId= request()->has('payrun')?request()->get('payrun').'-':'';
        // $file_name = 'payslip-'.$payrunId.date('Y-m-d').'.xlsx';

        // return $payslips;

        // return (new AllPayslipExport($payslips))->download($file_name, Excel::XLSX);
    }

    //TODO cron
    public function sendPayslip(Payslip $payslip)
    {
        SendPayslipJob::dispatch($payslip);

        $statusPending = resolve(StatusRepository::class)->payslipPending();
        $payslip->update(['status_id' => $statusPending]);

        return response()->json(['status' => true, 'message' => trans('default.payslip_has_started_sending')]);

    }

    public function sendMonthlyPayslip()
    {
        $within = request()->get('within');
        $month = $within ?: request('month_number') + 1;
        $ranges = $this->convertRangesToStringFormat($this->getStartAndEndOf($month, request()->get('year')));

        if ($within == 'total') {
            $min_date = Payslip::query()->oldest('start_date')->first()->start_date;
            $ranges = [$min_date->toDateString(), todayFromApp()->toDateString()];
        }

        $payslipsQuery = $this->service
            ->filters($this->filter)
            ->with($this->service->getRelations())
            ->whereBetween(DB::raw('DATE(start_date)'), count($ranges) == 1 ? [$ranges[0], $ranges[0]] : $ranges)
            ->whereBetween(DB::raw('DATE(end_date)'), count($ranges) == 1 ? [$ranges[0], $ranges[0]] : $ranges);

        $payslipsQuery->get()->each(fn(Payslip $payslip) => SendPayslipJob::dispatch($payslip));

        $statusPending = resolve(StatusRepository::class)->payslipPending();
        $payslipsQuery->update(['status_id' => $statusPending]);

        return response()->json(['status' => true, 'message' => trans('default.payslip_has_started_sending')]);

    }
    //TODO recibo pdf
    public function showPdf(Payslip $payslip)
    {
        $payslip->load($this->service->getRelations());

        if (auth()->id() != $payslip->user->id && auth()->user()->roles()->whereIn('alias',['admin', 'manager'])->doesntExist()){
            throw new GeneralException(__t('action_not_allowed'));
        }

        $beneficiaries = count($payslip->beneficiaries) ? $payslip->beneficiaries : ($payslip->without_beneficiary ? [] : $payslip->payrun->beneficiaries);
        $salaryAmount = $payslip->basic_salary;
        $totalAllowance = $this->service->getTotalBeneficiary($beneficiaries, $salaryAmount, 'allowance');
        $totalDeduction = $this->service->getTotalBeneficiary($beneficiaries, $salaryAmount, 'deduction');
        $totalContribution = $this->service->getTotalBeneficiary($beneficiaries, $salaryAmount, 'contribution');
        $payslipFor = $this->getDateDifferenceString($payslip->start_date, $payslip->end_date);
//        [$setting_able_id, $setting_able_type] = $this->tenantAble();
//        $settings = (object)resolve(SettingRepository::class)
//            ->getFormattedSettings('tenant', $setting_able_type, $setting_able_id);
        $settings = (object)resolve(TenantSettingService::class)
            ->getFormattedTenantSettings();
        $payslip_settings = json_decode($payslip->payrun->data);
        //if payslip pdf style not found
//        PDF::setOption(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
//            ->setHttpContext(stream_context_create([
//                'ssl' => [
//                    'verify_peer' => false,
//                    'verify_peer_name' => false,
//                    'allow_self_signed' => true,
//                ]
//            ]))->loadView();
        $pdf = PDF::loadView('tenant.payroll.pdf.payslip',
            compact(
                'payslip',
                'beneficiaries',
                'totalAllowance',
                'totalDeduction',
                'totalContribution',
                'settings',
                'salaryAmount',
                'payslipFor',
                'payslip_settings'
            )
        );
        $fileName = 'Payslip for ' . $payslip->user->full_name . ' (' . ($payslip->user->profile ? $payslip->user->profile->employee_id : 'uid') . ').pdf';
        if (request()->get('download') == true) {
            return $pdf->download($fileName);
        }
        return $pdf->stream($fileName);
    }

    public function update(Payslip $payslip)
    {

        // dd($payslip);

        DB::transaction(function () use ($payslip){
            $this->service
                ->setModel($payslip)
                ->setAttributes(\request()->only([
                    'allowances',
                    'allowanceValues',
                    'allowancePercentages',
                    'deductions',
                    'deductionValues',
                    'deductionPercentages',
                ]))
                ->beneficiariesValidation()
                ->updateBeneficiaries();

            //TODO no se registraron conceptos a la boleta
            if (count(\request()->get('allowances',[])) == 0 && count(\request()->get('deductions',[])) == 0){
                $payslip->without_beneficiary = true;
            }

            // $settings = [
            //     'consider_type' => $payslip->consider_type,
            //     'period' => $payslip->period,
            //     'consider_overtime' => $payslip->consider_overtime
            // ];
            // $ranges = [$payslip->start_date, $payslip->end_date];
            // $net_salary = $this->payrunService->countNetSalary($payslip->user, $settings, $payslip->beneficiaries, $ranges);
            //TODO calcula salario neto
            // $payslip->net_salary = $net_salary;
            // $payslip->note = request()->get('note');
            $payslip->total_bruto = request()->get('total_bruto');
            $payslip->total_dscto = request()->get('total_dscto');
            $payslip->total_aporte = request()->get('total_aporte');
            $payslip->base_imponible = request()->get('base_imponible');
            $payslip->total_liquido = request()->get('total_liquido');

            $payslip->update();
        });

        return updated_responses('payslip');
    }

    public function destroy(Payslip $payslip)
    {
        $payslip->beneficiaries()->delete();
        $payslip->delete();

        return deleted_responses('payslip');
    }

    // TODO Genera boleta de pago, guarda conceptos remunerativos
    public function manualStore(User $employee)
    {
        
        $employee->payrollInformation->agency_id = request()->get('agency_id');
        $employee->payrollInformation->designation_id = request()->get('designation_id');
        $employee->payrollInformation->save();
        // Asignar conceptos
        resolve(PayslipService::class)
            ->setModel($employee)
            ->setAttributes(\request()->only([
                'allowances',
                'allowanceValues',
                'allowancePercentages',
                'deductions',
                'deductionValues',
                'deductionPercentages',
                'contributions',
                'contributionValues',
                'contributionPercentages', 
            ]))
            ->beneficiariesValidation()
            ->updateBeneficiaries('payrunBeneficiaries');

        DB::transaction(function (){
            
            //Generar boleta
            $this->manualPayrollService
                ->setAttrs(\request()->only('consider_type', 'payrun_id', 'payrun_period', 'consider_overtime',
                    'departments', 'users', 'executable_month', 'executable_year', 'end_date', 'start_date',
                    'allowances', 'allowanceValues', 'allowancePercentages',
                    'deductions', 'deductionValues', 'deductionPercentages',
                    'note',
                    'finance_source_id','payroll_class_id','payroll_type_id','total_bruto','total_dscto','total_aporte','base_imponible','total_liquido', 'agency_id' ,'designation_id','note','days_worked'
                ))->payslipValidations()
                ->setPayrunRanges()
                // ->dateRangeValidations()
                ->setUsers()
                ->findAndSetManualPayrun() // Buscar planilla y establcer
                // ->saveAndSetBeneficiaries() 
                ->generateManualPayrunPayslips();
                // ->saveBatchId();

        });

        return created_responses('payrun');
                    
    }

    public function masiveStore(){

        // Verificar si hay un archivo cargado request()->get('within');
        if (request()->hasFile('file')) {
            $file = request()->file('file');

            // 0. READ XLXS
            // Cargar el archivo Excel
            $spreadsheet = IOFactory::load($file);

            // Seleccionar la hoja 'PLANILLA'
            $sheet = $spreadsheet->getSheetByName('PLANILLA');

            // Obtener el valor de la celda G10 -> nro DNI
            // $user_doc_num = $sheet->getCell('G10')->getValue();
            
            $dataArray = [];

            // 1. CREATE USER
            // 1.1. Verificar si el DNI fue registrado
            

            // return $user;
            // 2. CREATE BENEFICIARIES FOR USER
            $allowances = [];
            $allowanceValues = [];
            $allowancePercentages = [];
            $deductions = [];
            $deductionValues = [];
            $deductionPercentages = [];
            $contributions = [];
            $contributionValues = [];
            $contributionPercentages = [];

            // $user_beneficiary = $sheet->getCell('I10')->getValue();

            $matriz = [
                ['L', 'M', 'N'],
                ['O', 'P', 'Q'],
                ['R', 'S', 'T'],
                ['U', 'V', 'W'],
                ['X', 'Y', 'Z'],
                ['AA', 'AB', 'AC'],
                ['AD', 'AE', 'AF'],
                ['AG', 'AH', 'AI'],
                ['AJ', 'AK', 'AL'],
                ['AM', 'AN', 'AO'],
                ['AP', 'AQ', 'AR'],
                ['AS', 'AT', 'AU']
            ];
            

            $row = 10;
            $flag = $sheet->getCell($matriz[0][0].$row)->getValue(); // Inicializar tipo de celda

            $data = [];
            
            while (strlen($sheet->getCell('D'.$row)->getValue()) > 1) { // Mientras haya un valor en la primera celda de la fila y $row sea <= 50

                $user_doc_num = $sheet->getCell('D'.$row)->getValue();
                $user_ruc_num = $sheet->getCell('E'.$row)->getValue();
                $user_first_name = $sheet->getCell('A'.$row)->getValue();
                $user_last_name = $sheet->getCell('B'.$row)->getValue();
                $user_middle_name = $sheet->getCell('C'.$row)->getValue();
                $user_cargo = $sheet->getCell('F'.$row)->getValue();
                $user_cta = $sheet->getCell('G'.$row)->getValue();

                $user_total_bruto = $sheet->getCell('H'.$row)->getValue();
                $user_bi = $sheet->getCell('I'.$row)->getValue();
                $user_total_dscto = $sheet->getCell('J'.$row)->getValue();
                $user_neto = $sheet->getCell('K'.$row)->getValue();


                // Usuario
                $dataArray = [
                    'roles' => [4],
                    'document_number' => $user_doc_num,
                    'ruc_number' => $user_ruc_num,
                    'payslip' => [
                        'cargo' => $user_cargo,
                        'cuenta' => $user_cta,
                        'total_bruto' => $user_total_bruto,
                        'base_imponible' => $user_bi,
                        'total_dscto' => $user_total_dscto,
                        'neto_pagar' => $user_neto,
                    ],
                    'account_number' => '',
                    'agency_id' => '1',
                    'basic_remuneration' => '0',
                    'basic_remuneration_start_at' => null,
                    'days_worked' => '0',
                    'worker_type_id' => 2,
                    'payment_currency_id' => 1,
                    'is_contract' => false,
                    'disability' => false,
                    'unionized' => false,
                    'resident' => true,
                    'document_type_id' => 1,
                    'email' => $user_doc_num.'@DREPUNO.NET.PE',
                    'employee_id' => '',
                    'employment_status_id' => 2,
                    'first_name' => $user_first_name !== '' ? strtoupper($user_first_name) : '',
                    'gender' => null,
                    'have_health_insurance' => true,
                    'have_suspension_4ta' => true,
                    'health_regime_id' => 1,
                    'is_in_employee' => true,
                    'last_name' => $user_last_name !== '' ? strtoupper($user_last_name) : '',
                    'middle_name' => $user_middle_name !== '' ? strtoupper($user_middle_name) : '',
                ];
                  

                // Recorrer cada columna de la matriz
                foreach ($matriz as $key => $fila) {

                    $type = $sheet->getCell($fila[0].$row)->getValue();
                    $id_concepto = $sheet->getCell($fila[1].$row)->getValue();
                    $value = $sheet->getCell($fila[2].$row)->getValue();

                    if ($type == 1 && $value) { // Es INGRESO y tiene valor
                        // echo "INGRESO ".$sheet->getCell($fila[2].$row)->getValue();
                        // $concepto = Beneficiary::query()->where('id', $id_concepto)->first();

                        if($id_concepto){
                            $allowances[] = $id_concepto;
                            $allowanceValues[] = floatval($value);
                            $allowancePercentages[] = false;
                        }
                    }
                    if ($type == 2 && $value) { // Es INGRESO
                        // echo "DESCUENTO ".$sheet->getCell($fila[2].$row)->getValue();
                        // $concepto = Beneficiary::query()->where('id', $id_concepto)->first();

                        if($id_concepto){
                            $deductions[] = $id_concepto;
                            $deductionValues[] = floatval($value);
                            $deductionPercentages[] = false;
                        }
                    }
                    if ($type == 3 && $value) { // Es INGRESO
                        // echo "APORTE ".$sheet->getCell($fila[2].$row)->getValue();
                        // $concepto = Beneficiary::query()->where('id', $id_concepto)->first();

                        if($id_concepto){
                            $contributions[] = $id_concepto;
                            $contributionValues[] = floatval($value);
                            $contributionPercentages[] = false;
                        }
                    }
                    
                }

                $dataArray['allowances'] = $allowances;
                $dataArray['allowanceValues'] = $allowanceValues;
                $dataArray['allowancePercentages'] = $allowancePercentages;

                $dataArray['deductions'] = $deductions;
                $dataArray['deductionValues'] = $deductionValues;
                $dataArray['deductionPercentages'] = $deductionPercentages;

                $dataArray['contributions'] = $contributions;
                $dataArray['contributionValues'] = $contributionValues;
                $dataArray['contributionPercentages'] = $contributionPercentages;

                $data[] = $dataArray;

                $allowances = [];
                $allowanceValues = [];
                $allowancePercentages = [];
                $deductions = [];
                $deductionValues = [];
                $deductionPercentages = [];
                $contributions = [];
                $contributionValues = [];
                $contributionPercentages = [];

                $row++; // Incrementar el valor de $row para pasar a la siguiente fila
            }

            $users = [];
            

            DB::transaction(function () use ($data, &$users) {
                foreach ($data as $key => $value) {

                    $user[$key] = User::whereHas('profile', function ($query) use ($value) {
                        $query->where('document_number', $value['document_number']);
                    })->first();

                    if ($user[$key] == null){
                        // User::insert($usersData);
                        $user = new User();
                        $user->first_name = $value['first_name'];
                        $user->last_name = $value['last_name'];
                        $user->middle_name = $value['middle_name'];
                        $user->email = $value['email'];
                        $user->status_id = 1;
                        $user->invitation_token = base64_encode($value['email'].'-invitation-from-us');
                        $user->is_in_employee = 1;
                        $user->password = Hash::make(Str::random(8));
                        $user->save();

                        //ACTUALIZAR PROFILE
                        $user->profile()->updateOrCreate(
                            ['user_id' => $user->id],
                            [
                                // 'date_of_birth' => $value['date_of_birth'],
                                // 'marital_status_id' => $value['marital_status_id'],
                                'document_type_id' => $value['document_type_id'],
                                'document_number' => $value['document_number'],
                                'ruc_number' => $value['ruc_number'],
                                // 'personal_situation_id' => $value['personal_situation_id'],
                                // 'worker_category_id' => $value['worker_category_id'],
                                // 'education_level_id' => $value['education_level_id'],
                                // 'entry_date' => $value['entry_date'],
                                // 'disability' => $value['disability'],
                                // 'unionized' => $value['unionized'],
                                // 'resident' => $value['resident'],
                                // 'phone_number' => $value['phone_number'],
                                // 'entry_date' => $value['entry_date'],
                                'gender' => $value['gender'],
                            ]
                        );

                        $users[] = $user->id; 

                        //ACTUALIZAR CONCEPTOS
                        $payslipService = app(PayslipService::class);
                        $payslipService->setModel($user)
                            ->setAttributes([
                                'allowances' => $value['allowances'],
                                'allowanceValues' => $value['allowanceValues'],
                                'allowancePercentages' => $value['allowancePercentages'],
                                'deductions' => $value['deductions'],
                                'deductionValues' => $value['deductionValues'],
                                'deductionPercentages' => $value['deductionPercentages'],
                                'contributions' => $value['contributions'],
                                'contributionValues' => $value['contributionValues'],
                                'contributionPercentages' => $value['contributionPercentages'],
                            ])
                            ->updateBeneficiaries('payrunBeneficiaries');
                    }else{
                        $users[] = $user[$key]->id; 
                    }

                }

            });

            // 3. GENERATE PAYSLIP

            // Fusionar datos adicionales en la solicitud
            request()->merge(['users' => $users]);
            //Generar boleta
            $this->manualPayrollService
                ->setAttrs(\request()->only('consider_type', 'payrun_period', 'consider_overtime',
                    'departments', 'users', 'executable_month', 'executable_year', 'end_date', 'start_date',
                    'note',
                    'finance_source_id','payroll_class_id','payroll_type_id','total_bruto','total_dscto','total_aporte','base_imponible','total_liquido','designation_id','note','days_worked', 'payslip'
                ))->masiveValidations()
                ->setRanges()
                // ->dateRangeValidations()
                ->setUsers() 
                ->saveAndSetManualPayrun() // Genera la planilla
                // ->saveAndSetBeneficiaries() 
                ->generateManualPayrunPayslips();
                // ->saveBatchId();
            
            // return $users;


            // Aquí puedes realizar el procesamiento del archivo, como almacenarlo o realizar alguna acción con los datos
            return response()->json(['message' => 'Archivo recibido correctamente'], 200);
        } else {
            return response()->json(['message' => 'No se recibió ningún archivo'], 400);
        }

    }
    
}
