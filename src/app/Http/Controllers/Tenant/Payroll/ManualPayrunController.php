<?php

namespace App\Http\Controllers\Tenant\Payroll;

use App\Http\Controllers\Controller;
use App\Models\Tenant\Payroll\Payrun;
use App\Services\Tenant\Payroll\ManualPayrollService;
use Illuminate\Support\Facades\DB;

class ManualPayrunController extends Controller
{

    public function __construct(ManualPayrollService $service)
    {
        $this->service = $service;
    }

    public function index(Payrun $payrun)
    {
        return $payrun->load([
            'beneficiaries',
            'beneficiaries.beneficiary:id,type'
        ]);
    }
    //TODO REGISTRAR PLANILLA
    public function store() 
    {
        DB::transaction(function () {
            $this->service
                ->setAttrs(\request()->only('consider_type', 'payrun_period', 'consider_overtime',
                    'departments', 'users', 'executable_month', 'executable_year', 'end_date', 'start_date',
                    'allowances', 'allowanceValues', 'allowancePercentages',
                    'deductions', 'deductionValues', 'deductionPercentages',
                    'note',
                    'finance_source_id','payroll_class_id','payroll_type_id'
                ))->validations()
                ->setRanges()
//                ->dateRangeValidations()
                ->setEmptyUsers()
                ->saveAndSetManualPayrun();
                // ->saveAndSetBeneficiaries() // Se tomarásolo del usu
                // ->generateManualPayrunPayslips();
                // ->saveBatchId();
        });

        //TODO verificar si la planilla es NORMAL o ADICIONAL y generar un codigo identificador

        return created_responses('payrun');
    }


    /**
     * @param Payrun $payrun parámetro una ejecucuón de pago (payrun).
     * 
     * 
     */
    public function update(Payrun $payrun)
    {
        DB::transaction(function () use ($payrun) {
            $this->service
                ->validateForUpdate($payrun)
                ->setAttrs(\request()->only('consider_type', 'payrun_period', 'consider_overtime',
                    'departments', 'users', 'executable_month', 'executable_year', 'end_date', 'start_date',
                    'allowances', 'allowanceValues', 'allowancePercentages',
                    'deductions', 'deductionValues', 'deductionPercentages',
                    'note',
                    'finance_source_id','payroll_class_id','payroll_type_id'
                ))->validations() // Validadción general, Validad si existe fecha ini-fin, Validad si existe Haber/Descuento genral 
                ->setRanges() // Parse to date fechas
//                ->dateRangeValidations()
                ->setUsers()
                ->updatePayrun($payrun) // Actualiza información de planilla
                ->setPayrun($payrun) // Establece información de planilla actualizada
                ->deleteAllUnderPayrun() // Borra conceptos anteriores 
                ->saveAndSetBeneficiaries() // Guarda nuevos conceptos
                ->generateManualPayrunPayslips() // Genera encabezado de recibos
                ->setModel($payrun)
                ->saveBatchId();
        });

        // return 'HOLI';
        return updated_responses('payrun');
    }

}
