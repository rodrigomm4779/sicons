<?php

namespace App\Http\Controllers\Tenant\Auth;

use App\Filters\Common\Auth\UserFilter as AppUserFilter;
use App\Filters\Core\UserFilter;
use App\Filters\Tenant\Helper\UserSelectableAccessFilter;
use App\Http\Controllers\Controller;
use App\Models\Core\Auth\User;
use App\Repositories\Core\Status\StatusRepository;
use Illuminate\Database\Eloquent\Builder;

class TenantUserAPIController extends Controller
{
    /**
     * @var UserSelectableAccessFilter
     */
    private UserSelectableAccessFilter $userAccessSelectableFilter;

    public function __construct(UserFilter $filter, UserSelectableAccessFilter $userAccessSelectableFilter)
    {
        $this->filter = $filter;
        $this->userAccessSelectableFilter = $userAccessSelectableFilter;
    }

    public function index()
    {
        $statuses_id = resolve(StatusRepository::class)->userInvitedInactive();
        return (new AppUserFilter(
            User::query()
                ->select(['id', 'first_name', 'last_name', 'email', 'status_id', 'middle_name'])
                ->when(request()->has('with') && request()->has('with') == 'status-profile-picture', function (Builder $builder){
                    $builder->with(['status:id,name,class', 'profilePicture']);
                    $builder->with(['profile:id,user_id,ruc_number,document_number']);
                })->when(request()->has('except-invited-only'), function (Builder $builder) use ($statuses_id){
                    $builder->where('status_id','!=',  resolve(StatusRepository::class)->userInvited());
                }, function (Builder $builder) use ($statuses_id){
                    $builder->whereNotIn('status_id',  $statuses_id);
                })
                ->when(request()->has('existing'), function(Builder $builder) {
                    $builder->whereNotIn('id', explode(',', request('existing')));
                })
                ->when(optional(tenant())->is_single, function (Builder $builder) {
                    $builder->whereHas('roles', function (Builder $builder) {
                        $builder->where('tenant_id', tenant()->id);
                    });
                }, function (Builder $builder) {
                    $builder->whereHas('tenants', function (Builder $builder) {
                        $builder->where('id', tenant()->id);
                    });
                })->when(request()->include_profile_picture, function (Builder $builder) {
                    $builder->with('profilePicture');
                })
        ))->filter()
            ->filters($this->filter)
            ->filters($this->userAccessSelectableFilter)
            ->get();
    }

    public function users()
    {
        $statuses_id = resolve(StatusRepository::class)->userInvitedInactive();
        
        $query = User::query();

        if (request()->has('existing')) {
            $query->whereNotIn('users.id', explode(',', request('existing')));
        }

        if (request()->has('with') && request('with') == 'profile-information') {
            $query->select(['users.id', 'first_name', 'last_name', 'middle_name'])
                ->selectRaw('profiles.document_number')
                ->selectRaw('CONCAT(profiles.document_number, " - ", users.first_name, " ", users.last_name, " ",users.middle_name) as value')
                ->leftJoin('profiles', 'profiles.user_id', '=', 'users.id');
        }

        return (new AppUserFilter($query))->filter()
            ->filters($this->filter)
            ->filters($this->userAccessSelectableFilter)
            ->get();
    }

    public function getUserInfo($user)
    {
        return User::with('profile', 'payrollInformation')->findOrFail($user);
    }

    public function nextUser($user, $type)
    {
        $userInvited = resolve(StatusRepository::class)->userInvited();

        return User::where('id', '!=', $user)
            ->with('profilePicture', 'status:id,name,class')
            ->when(
                $type == 'next',
                fn(Builder $builder) => $builder->where('id', '>', $user)->orderBy('id', 'ASC'),
                fn(Builder $builder) => $builder->where('id', '<', $user)->orderBy('id', 'DESC')
            )->where('status_id', '!=', $userInvited)
            ->firstOrFail();
    }
}
