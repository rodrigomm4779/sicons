<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\PublicEmployee;
use App\Services\Tenant\Maintenance\PublicEmployeeService;
use Illuminate\Http\Request;

class PublicEmployeeController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(PublicEmployeeService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return PublicEmployee::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name', 'description'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Tipo de empleado created successfully'], 201);
    }

    public function show(PublicEmployee $publicEmployeeType)
    {
        return response()->json($publicEmployeeType);
    }

    public function update(Request $request, PublicEmployee $publicEmployeeType)
    {
        $this->service
            ->setModel($publicEmployeeType)
            ->setAttributes($request->only('code', 'name', 'description'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Tipo de empleado updated successfully']);
    }

    public function destroy(PublicEmployee $publicEmployeeType)
    {
        try {
            $publicEmployeeType->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete Tipo de empleado');
        }

        return response()->json(['message' => 'Tipo de empleado deleted successfully']);
    }
}