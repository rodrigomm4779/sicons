<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\PaymentCurrencyFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\PaymentCurrency;

class PaymentCurrencyAPIController extends Controller
{
    protected $filter;

    public function __construct(PaymentCurrencyFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return PaymentCurrency::filters($this->filter)
            ->get(['id', 'name', 'code']);
    }
}
