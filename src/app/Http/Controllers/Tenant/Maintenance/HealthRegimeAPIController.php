<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\HealthRegime;

class HealthRegimeAPIController extends Controller
{
    protected $filter;

    public function __construct(GeneralMaintenanceFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return HealthRegime::filters($this->filter)
                ->get()
                ->map(function ($item) {
                    return [
                        'id' => $item->id,
                        'name' => $item->name,
                        'option' => $item->option,
                    ];
                });
    }
}
