<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\EducationLevelFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\EducationLevel;

class EducationLevelAPIController extends Controller
{
    protected $filter;

    public function __construct(EducationLevelFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return EducationLevel::isActive(true)->filters($this->filter)
            ->get(['id', 'name', 'code']);
    }
}
