<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\ContractTypeFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\ContractType;

class ContractTypeAPIController extends Controller
{
    protected $filter;

    public function __construct(ContractTypeFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return ContractType::filters($this->filter)
            ->get(['id', 'name', 'code']);
    }
}
