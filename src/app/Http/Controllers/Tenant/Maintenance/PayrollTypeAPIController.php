<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\PayrollType;

class PayrollTypeAPIController extends Controller
{
    protected $filter;

    public function __construct(GeneralMaintenanceFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return PayrollType::isActive(true)->filters($this->filter)
                ->get()
                ->map(function ($paayrollType) {
                    return [
                        'id' => $paayrollType->id,
                        'name' => $paayrollType->name,
                        'option' => $paayrollType->option, // Incluir el accesor 'option'
                    ];
                });
    }
}
