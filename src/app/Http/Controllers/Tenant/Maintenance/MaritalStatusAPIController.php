<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\MaritalStatusFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\MaritalStatus;

class MaritalStatusAPIController extends Controller
{
    public function __construct(MaritalStatusFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return MaritalStatus::filters($this->filter)
            ->get(['id', 'name']);
    }
}
