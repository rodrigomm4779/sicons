<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\CostCenterFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\CostCenter;

class CostCenterAPIController extends Controller
{
    protected $filter;

    public function __construct(CostCenterFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return CostCenter::filters($this->filter)
            ->get(['id', 'code', 'name', 'options']);
    }
}
