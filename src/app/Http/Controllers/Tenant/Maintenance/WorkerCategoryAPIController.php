<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\WorkerCategoryFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\WorkerCategory;

class WorkerCategoryAPIController extends Controller
{
    protected $filter;

    public function __construct(WorkerCategoryFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return WorkerCategory::isActive(true)->filters($this->filter)
            ->get(['id', 'name', 'code']);
    }
}
