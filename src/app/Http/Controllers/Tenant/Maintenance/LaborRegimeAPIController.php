<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\LaborRegimeFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\LaborRegime;

class LaborRegimeAPIController extends Controller
{
    protected $filter;

    public function __construct(LaborRegimeFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return LaborRegime::filters($this->filter)
                ->get()
                ->map(function ($item) {
                    return [
                        'id' => $item->id,
                        'name' => $item->name,
                        'option' => $item->option,
                    ];
                });
    }
}
