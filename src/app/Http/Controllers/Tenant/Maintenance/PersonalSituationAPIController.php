<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\PersonalSituationFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\PersonalSituation;

class PersonalSituationAPIController extends Controller
{
    protected $filter;

    public function __construct(PersonalSituationFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return PersonalSituation::isActive(true)->filters($this->filter)
            ->get(['id', 'name', 'code']);
    }
}
