<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\ConceptAirshpFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\ConceptAirshp;

class ConceptAirshpAPIController extends Controller
{
    protected $filter;

    public function __construct(ConceptAirshpFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return ConceptAirshp::filters($this->filter)
            ->get(['id','type_code', 'concept_type', 'concept_code', 'name', 'description', 'is_taxable', 'modality']);
    }

}
