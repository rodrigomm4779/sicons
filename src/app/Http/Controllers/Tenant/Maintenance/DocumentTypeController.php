<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\DocumentType;
use App\Services\Tenant\Maintenance\DocumentTypeService;
use Illuminate\Http\Request;

class DocumentTypeController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(DocumentTypeService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return DocumentType::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name', 'description', 'is_enabled', 'airhsp_code'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Tipo documento created successfully'], 201);
    }

    public function show(DocumentType $documentType)
    {
        return response()->json($documentType);
    }

    public function update(Request $request, DocumentType $documentType)
    {
        $this->service
            ->setModel($documentType)
            ->setAttributes($request->only('code', 'name', 'description', 'is_enabled', 'airhsp_code'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Tipo documento updated successfully']);
    }

    public function destroy(DocumentType $documentType)
    {
        try {
            $documentType->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete Tipo documento');
        }

        return response()->json(['message' => 'Tipo documento deleted successfully']);
    }
}