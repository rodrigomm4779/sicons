<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\AgencyFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\Agency;

class AgencyAPIController extends Controller
{
    protected $filter;

    public function __construct(AgencyFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return Agency::filters($this->filter)
            ->get(['id', 'code', 'name']);
    }
}
