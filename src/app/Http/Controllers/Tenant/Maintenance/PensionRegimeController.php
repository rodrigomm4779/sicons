<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\PensionRegime;
use App\Services\Tenant\Maintenance\PensionRegimeService;
use Illuminate\Http\Request;

class PensionRegimeController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(PensionRegimeService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return PensionRegime::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name', 'description', 'is_active', 'type', 'options', 'created_at', 'updated_at', 'prefijo', 'comision_flujo', 'comision_mixta', 'comision_anual_saldo', 'prima_seguro', 'aporte_obligatorio', 'maxima_asegurable'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Pension regime created successfully'], 201);
    }

    public function show(PensionRegime $pensionRegime)
    {
        return response()->json($pensionRegime);
    }

    public function update(Request $request, PensionRegime $pensionRegime)
    {
        $this->service
            ->setModel($pensionRegime)
            ->setAttributes($request->only('prefix', 'commission_on_flow', 'commission_mixed', 'commission_annual_balance', 'insurance_premium', 'mandatory_contribution', 'max_insurable_salary'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Pension regime updated successfully']);
    }

    public function destroy(PensionRegime $pensionRegime)
    {
        try {
            $pensionRegime->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete pension regime');
        }

        return response()->json(['message' => 'Pension regime deleted successfully']);
    }
}