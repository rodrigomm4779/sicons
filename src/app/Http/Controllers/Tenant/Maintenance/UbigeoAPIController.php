<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\Department;
use App\Models\Tenant\Maintenance\Province;
use App\Models\Tenant\Maintenance\District;

class UbigeoAPIController extends Controller
{
    public function __construct(GeneralMaintenanceFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        
    }

    // Método para obtener todos los departamentos
    public function getAllDepartments()
    {
        return Department::filters($this->filter)
                            ->get()
                            ->map(function ($department) {
                                return [
                                    'id' => $department->code,
                                    'name' => $department->name,
                                    'option' => $department->option,
                                ];
                            });

    }

    // Método para obtener todas las provincias de un departamento específico
    public function getProvincesByDepartment($departmentCode)
    {
        return Province::where('department_code', $departmentCode)->filters($this->filter)
                            ->get()
                            ->map(function ($province) {
                                return [
                                    'id' => $province->code,
                                    'name' => $province->name,
                                    'option' => $province->option,
                                ];
                            });
    }

    // Método para obtener todos los distritos de una provincia específica
    public function getDistrictsByProvince($provinceCode)
    {
        return District::where('province_code', $provinceCode)->filters($this->filter)
                            ->get()
                            ->map(function ($province) {
                                return [
                                    'id' => $province->code,
                                    'name' => $province->name,
                                    'option' => $province->option,
                                ];
                            });

    }

}
