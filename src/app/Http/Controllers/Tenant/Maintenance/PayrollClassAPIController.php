<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\PayrollClass;

class PayrollClassAPIController extends Controller
{
    protected $filter;

    public function __construct(GeneralMaintenanceFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return PayrollClass::isActive(true)->filters($this->filter)
                ->get()
                ->map(function ($payrollClass) {
                    return [
                        'id' => $payrollClass->id,
                        'name' => $payrollClass->name,
                        'option' => $payrollClass->option
                    ];
                });
    }
}
