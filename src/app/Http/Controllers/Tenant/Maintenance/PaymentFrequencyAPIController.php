<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\PaymentFrequencyFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\PaymentFrequency;

class PaymentFrequencyAPIController extends Controller
{
    protected $filter;

    public function __construct(PaymentFrequencyFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return PaymentFrequency::filters($this->filter)
            ->get(['id', 'name', 'code']);
    }
}
