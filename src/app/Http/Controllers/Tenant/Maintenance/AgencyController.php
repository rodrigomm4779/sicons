<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\Agency;
use App\Services\Tenant\Maintenance\AgencyService;
use Illuminate\Http\Request;

class AgencyController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(AgencyService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return Agency::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Agencia created successfully'], 201);
    }

    public function show(Agency $agency)
    {
        return response()->json($agency);
    }

    public function update(Request $request, Agency $agency)
    {
        $this->service
            ->setModel($agency)
            ->setAttributes($request->only('code', 'name'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Agencia updated successfully']);
    }

    public function destroy(Agency $agency)
    {
        try {
            $agency->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete Agencia');
        }

        return response()->json(['message' => 'Agencia deleted successfully']);
    }
}