<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\SunatPET22Filter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\SunatPET22;

class SunatPET22APIController extends Controller
{
    protected $filter;

    public function __construct(SunatPET22Filter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return SunatPET22::filters($this->filter)
            ->get(['codigo', 'descripcion']);
    }
}
