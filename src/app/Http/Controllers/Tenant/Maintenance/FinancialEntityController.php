<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\FinancialEntity;
use App\Services\Tenant\Maintenance\FinancialEntityService;
use Illuminate\Http\Request;

class FinancialEntityController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(FinancialEntityService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return FinancialEntity::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name', 'description', 'is_active'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Entidad Financiera created successfully'], 201);
    }

    public function show(FinancialEntity $financialEntity)
    {
        return response()->json($financialEntity);
    }

    public function update(Request $request, FinancialEntity $financialEntity)
    {
        $this->service
            ->setModel($financialEntity)
            ->setAttributes($request->only('code', 'name', 'description', 'is_active'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Entidad Financiera updated successfully']);
    }

    public function destroy(FinancialEntity $financialEntity)
    {
        try {
            $financialEntity->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete Entidad Financiera');
        }

        return response()->json(['message' => 'Entidad Financiera deleted successfully']);
    }
}