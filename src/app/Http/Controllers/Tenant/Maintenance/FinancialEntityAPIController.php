<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\FinancialEntity;

class FinancialEntityAPIController extends Controller
{
    protected $filter;

    public function __construct(GeneralMaintenanceFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return FinancialEntity::filters($this->filter)
                ->get()
                ->map(function ($financialEntity) {
                    return [
                        'id' => $financialEntity->id,
                        'code' => $financialEntity->code,
                        'name' => $financialEntity->name,
                        'option' => $financialEntity->option,
                    ];
                });
    }
}
