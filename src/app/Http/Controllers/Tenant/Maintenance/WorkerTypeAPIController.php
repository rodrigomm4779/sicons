<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\WorkerTypeFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\WorkerType;

class WorkerTypeAPIController extends Controller
{
    public function __construct(WorkerTypeFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return WorkerType::isEnabled(true)->filters($this->filter)
            ->get(['id', 'name', 'code']);
    }
}
