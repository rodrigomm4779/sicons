<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\WorkerType;
use App\Services\Tenant\Maintenance\WorkerTypeService;
use Illuminate\Http\Request;

class WorkerTypeController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(WorkerTypeService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return WorkerType::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name', 'description', 'is_enabled'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Tipo plaza created successfully'], 201);
    }

    public function show(WorkerType $workerType)
    {
        return response()->json($workerType);
    }

    public function update(Request $request, WorkerType $workerType)
    {
        $this->service
            ->setModel($workerType)
            ->setAttributes($request->only('code', 'name', 'description', 'is_enabled'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Tipo plaza updated successfully']);
    }

    public function destroy(WorkerType $workerType)
    {
        try {
            $workerType->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete Tipo plaza');
        }

        return response()->json(['message' => 'Tipo plaza deleted successfully']);
    }
}