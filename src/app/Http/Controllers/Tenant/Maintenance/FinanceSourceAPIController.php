<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\FinanceSource;

class FinanceSourceAPIController extends Controller
{
    protected $filter;

    public function __construct(GeneralMaintenanceFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return FinanceSource::filters($this->filter)
                ->get()
                ->map(function ($finance_source) {
                    return [
                        'id' => $finance_source->id,
                        'name' => $finance_source->name,
                        'option' => $finance_source->option,
                    ];
                });
    }
}
