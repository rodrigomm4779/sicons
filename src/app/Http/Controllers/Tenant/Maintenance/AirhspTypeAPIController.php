<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\AirhspType;

class AirhspTypeAPIController extends Controller
{
    protected $filter;

    public function __construct(GeneralMaintenanceFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return AirhspType::filters($this->filter)
                ->get()
                ->map(function ($airhspType) {
                    return [
                        'id' => $airhspType->id,
                        'name' => $airhspType->name,
                        'option' => $airhspType->option, // Incluir el accesor 'option'
                    ];
                });
    }
}
