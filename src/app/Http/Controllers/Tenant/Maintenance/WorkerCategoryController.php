<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\WorkerCategory;
use App\Services\Tenant\Maintenance\WorkerCategoryService;
use Illuminate\Http\Request;

class WorkerCategoryController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(WorkerCategoryService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return WorkerCategory::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name', 'decription', 'is_active'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Categoría de trabajador created successfully'], 201);
    }

    public function show(WorkerCategory $workerCategory)
    {
        return response()->json($workerCategory);
    }

    public function update(Request $request, WorkerCategory $workerCategory)
    {
        $this->service
            ->setModel($workerCategory)
            ->setAttributes($request->only('code', 'name', 'decription', 'is_active'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Categoría de trabajador updated successfully']);
    }

    public function destroy(WorkerCategory $workerCategory)
    {
        try {
            $workerCategory->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete Categoría de trabajador');
        }

        return response()->json(['message' => 'Categoría de trabajador deleted successfully']);
    }
}