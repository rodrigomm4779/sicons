<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\OccupationalGroup;
use App\Services\Tenant\Maintenance\OccupationalGroupService;
use Illuminate\Http\Request;

class OccupationalGroupController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(OccupationalGroupService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return OccupationalGroup::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name', 'description'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Grupo ocupacional created successfully'], 201);
    }

    public function show(OccupationalGroup $occupationalGroup)
    {
        return response()->json($occupationalGroup);
    }

    public function update(Request $request, OccupationalGroup $occupationalGroup)
    {
        $this->service
            ->setModel($occupationalGroup)
            ->setAttributes($request->only('code', 'name', 'description'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Grupo ocupacional updated successfully']);
    }

    public function destroy(OccupationalGroup $occupationalGroup)
    {
        try {
            $occupationalGroup->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete Grupo ocupacional');
        }

        return response()->json(['message' => 'Grupo ocupacional deleted successfully']);
    }
}