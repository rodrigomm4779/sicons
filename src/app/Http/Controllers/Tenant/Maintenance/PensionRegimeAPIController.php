<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\PensionRegimeFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\PensionRegime;

class PensionRegimeAPIController extends Controller
{
    protected $filter;

    public function __construct(PensionRegimeFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return PensionRegime::isActive(true)->filters($this->filter)
                ->get()
                ->map(function ($item) {
                    return [
                        'id' => $item->id,
                        'name' => $item->name,
                        'type' => $item->type,
                        'option' => $item->option,
                    ];
                });
    }
}
