<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Exceptions\GeneralException;
use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\EducationLevel;
use App\Services\Tenant\Maintenance\EducationLevelService;
use Illuminate\Http\Request;

class EducationLevelController extends Controller
{
    protected $service;
    protected $filter;

    public function __construct(EducationLevelService $service, GeneralMaintenanceFilter $filter)
    {
        $this->service = $service;
        $this->filter = $filter;
    }

    public function index()
    {
        return EducationLevel::filters($this->filter)
            ->latest()
            ->paginate(request()->get('per_page', 10));
    }

    public function store(Request $request)
    {
        $this->service
            ->setAttributes($request->only('code', 'name', 'description', 'is_active'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Nivel educativo created successfully'], 201);
    }

    public function show(EducationLevel $educationLevel)
    {
        return response()->json($educationLevel);
    }

    public function update(Request $request, EducationLevel $educationLevel)
    {
        $this->service
            ->setModel($educationLevel)
            ->setAttributes($request->only('code', 'name', 'description', 'is_active'))
            ->validate()
            ->save();

        return response()->json(['message' => 'Nivel educativo updated successfully']);
    }

    public function destroy(EducationLevel $educationLevel)
    {
        try {
            $educationLevel->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Unable to delete Nivel educativo');
        }

        return response()->json(['message' => 'Nivel educativo deleted successfully']);
    }
}