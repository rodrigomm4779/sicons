<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\DocumentTypeFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\DocumentType;

class DocumentTypeAPIController extends Controller
{
    public function __construct(DocumentTypeFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return DocumentType::active()->filters($this->filter)
        ->get()
        ->map(function ($documentType) {
            return [
                'id' => $documentType->id,
                'name' => $documentType->name,
                'description' => $documentType->description,
                'option' => $documentType->option, // Incluir el accesor 'option'
            ];
        });
    }
}
