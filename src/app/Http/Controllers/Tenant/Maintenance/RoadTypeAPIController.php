<?php

namespace App\Http\Controllers\Tenant\Maintenance;

use App\Filters\Tenant\GeneralMaintenanceFilter;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Maintenance\RoadType;

class RoadTypeAPIController extends Controller
{
    public function __construct(GeneralMaintenanceFilter $filter)
    {
        $this->filter = $filter;
    }

    public function index()
    {
        return RoadType::active()->filters($this->filter)
        ->get()
        ->map(function ($documentType) {
            return [
                'id' => $documentType->id,
                'name' => $documentType->name,
                'description' => $documentType->description,
                'option' => $documentType->option, // Incluir el accesor 'option'
            ];
        });
    }
}
