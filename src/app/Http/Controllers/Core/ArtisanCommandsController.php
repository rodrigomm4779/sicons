<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;

/**
 * Class ArtisanCommandsController.
 */
class ArtisanCommandsController extends Controller
{
    public function clearView()
    {
        Artisan::call('view:clear');
        return response()->json(['message' => 'Vistas limpiadas correctamente.']);
    }

    public function storageLink()
    {
        Artisan::call('storage:link');
        return response()->json(['message' => 'Enlace simbólico para almacenamiento creado correctamente.']);
    }

    public function cacheConfig()
    {
        Artisan::call('config:cache');
        return response()->json(['message' => 'Configuración cacheada correctamente.']);
    }
}
