<?php

namespace App\Http\Requests\Tenant\Payroll;

use App\Http\Requests\BaseRequest;

class PayrollCertificateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'user_id' => 'required',
            'allowances' => 'required',
            'deductions' => 'required',
            'ref_exp_number' => 'nullable',
            'issue_date' => 'required',

            // 'department_id' => 'nullable|exists:departments,id',
        ];
    }
}
