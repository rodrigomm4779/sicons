<?php

namespace App\Http\Requests\Tenant\Employee;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class EmployeeRequest extends BaseRequest
{

    public function rules()
    {
        $employee = $this->route()->parameter('employee');

        return [
            'email' => [
                'sometimes',
                function ($attribute, $value, $fail) {
                    if ($value) {
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $fail('The email must be a valid email address.');
                        }
                    }
                },
                Rule::unique('users', 'email')->ignore(optional($employee)->id)
            ],
            // 'employee_id' => 'required|min:2|unique:profiles,employee_id,'.optional($employee)->id.',user_id',
            'employee_id' => 'nullable',
            'department_id' => 'required|integer',
            'designation_id' => 'required|integer',
            'employment_status_id' => 'required|integer',
            'work_shift_id' => 'nullable|integer',
            'gender' => 'required',

            // Nuevos atributos
            'document_type_id' => 'required|integer',
            'document_number' => 'required|string|max:50',
            'ruc_number' => 'nullable|string|max:11', // Asumiendo que es opcional
            'personal_situation_id' => 'nullable|integer',
            'worker_category_id' => 'nullable|integer',
            'education_level_id' => 'nullable|integer',
            'entry_date' => 'nullable|date',
            'disability' => 'required|boolean',
            'unionized' => 'required|boolean',
            'resident' => 'required|boolean',
        ];
    }
}
