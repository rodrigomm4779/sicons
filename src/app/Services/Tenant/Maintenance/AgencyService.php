<?php


namespace App\Services\Tenant\Maintenance;


use App\Exceptions\GeneralException;
use App\Models\Tenant\Maintenance\Agency;
use App\Services\Tenant\TenantService;
use Illuminate\Validation\ValidationException;

class AgencyService extends TenantService
{
    public function __construct(Agency $agency)
    {
        $this->model = $agency;
    }

    public function validate(): self
    {
        validator($this->getAttributes(), [
            'name'  => 'min:2',
            'code'  => 'min:1',
        ])->validate();

        return $this;
    }

}
