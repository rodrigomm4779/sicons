<?php


namespace App\Services\Tenant\Maintenance;


use App\Exceptions\GeneralException;
use App\Models\Tenant\Maintenance\EducationLevel;
use App\Services\Tenant\TenantService;
use Illuminate\Validation\ValidationException;

class EducationLevelService extends TenantService
{
    public function __construct(EducationLevel $educationLevel)
    {
        $this->model = $educationLevel;
    }

    public function validate(): self
    {
        validator($this->getAttributes(), [
            'name'  => 'min:2',
            'description'   => 'nullable|min:2',
            'code'  => 'nullable|min:1',
            'is_active'    => 'boolean',
        ])->validate();

        return $this;
    }

}
