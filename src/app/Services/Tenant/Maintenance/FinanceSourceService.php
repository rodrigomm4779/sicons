<?php


namespace App\Services\Tenant\Maintenance;


use App\Exceptions\GeneralException;
use App\Models\Tenant\Maintenance\FinanceSource;
use App\Services\Tenant\TenantService;
use Illuminate\Validation\ValidationException;

class FinanceSourceService extends TenantService
{
    public function __construct(FinanceSource $financeSource)
    {
        $this->model = $financeSource;
    }

    public function validate(): self
    {
        validator($this->getAttributes(), [
            'name'  => 'min:2',
            'description'   => 'nullable|min:2',
            'code'  => 'min:1',
            'is_active'    => 'boolean',
        ])->validate();

        return $this;
    }

}
