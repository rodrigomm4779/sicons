<?php


namespace App\Services\Tenant\Maintenance;


use App\Exceptions\GeneralException;
use App\Models\Tenant\Maintenance\PensionRegime;
use App\Services\Tenant\TenantService;
use Illuminate\Validation\ValidationException;

class PensionRegimeService extends TenantService
{
    public function __construct(PensionRegime $pensionRegime)
    {
        $this->model = $pensionRegime;
    }

    public function validate(): self
    {
        validator($this->getAttributes(), [
            'code' => 'min:2',
            'name' => 'min:2',
            'description' => 'min:2',
            'is_active' => 'boolean',
            'type' => 'min:2',
            'prefix' => 'nullable|min:1|max:10',
            'comision_flujo' => 'numeric|nullable',
            'comision_mixta' => 'numeric|nullable',
            'comision_anual_saldo' => 'numeric|nullable',
            'prima_seguro' => 'numeric|nullable',
            'aporte_obligatorio' => 'numeric|nullable',
            'maxima_asegurable' => 'numeric|nullable',
        ])->validate();

        return $this;
    }

}
