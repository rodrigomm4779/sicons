<?php


namespace App\Services\Tenant\Maintenance;


use App\Exceptions\GeneralException;
use App\Models\Tenant\Maintenance\DocumentType;
use App\Services\Tenant\TenantService;
use Illuminate\Validation\ValidationException;

class DocumentTypeService extends TenantService
{
    public function __construct(DocumentType $documentType)
    {
        $this->model = $documentType;
    }

    public function validate(): self
    {
        validator($this->getAttributes(), [
            'code' => 'min:2',
            'name' => 'min:2',
            'description' => 'min:2',
            'is_enabled' => 'boolean',
            'airhsp_code' => 'min:1',
        ])->validate();

        return $this;
    }

}
