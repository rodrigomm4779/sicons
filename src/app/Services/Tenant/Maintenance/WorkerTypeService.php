<?php


namespace App\Services\Tenant\Maintenance;


use App\Exceptions\GeneralException;
use App\Models\Tenant\Maintenance\WorkerType;
use App\Services\Tenant\TenantService;
use Illuminate\Validation\ValidationException;

class WorkerTypeService extends TenantService
{
    public function __construct(WorkerType $workerType)
    {
        $this->model = $workerType;
    }

    public function validate(): self
    {
        validator($this->getAttributes(), [
            'name'  => 'min:2',
            'description'   => 'nullable|min:2',
            'code'  => 'min:1',
            'is_enabled'    => 'boolean',
        ])->validate();

        return $this;
    }

}
