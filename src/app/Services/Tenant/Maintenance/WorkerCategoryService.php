<?php


namespace App\Services\Tenant\Maintenance;


use App\Exceptions\GeneralException;
use App\Models\Tenant\Maintenance\WorkerCategory;
use App\Services\Tenant\TenantService;
use Illuminate\Validation\ValidationException;

class WorkerCategoryService extends TenantService
{
    public function __construct(WorkerCategory $workerCategory)
    {
        $this->model = $workerCategory;
    }

    public function validate(): self
    {
        validator($this->getAttributes(), [
            'name'  => 'min:2',
            'code'  => 'min:1',
        ])->validate();

        return $this;
    }

}
