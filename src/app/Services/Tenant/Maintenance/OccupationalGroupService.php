<?php


namespace App\Services\Tenant\Maintenance;


use App\Exceptions\GeneralException;
use App\Models\Tenant\Maintenance\OccupationalGroup;
use App\Services\Tenant\TenantService;
use Illuminate\Validation\ValidationException;

class OccupationalGroupService extends TenantService
{
    public function __construct(OccupationalGroup $occupationalGroup)
    {
        $this->model = $occupationalGroup;
    }

    public function validate(): self
    {
        validator($this->getAttributes(), [
            'name'  => 'min:2',
            'description'   => 'nullable|min:2',
            'code'  => 'nullable|min:1',
        ])->validate();

        return $this;
    }

}
