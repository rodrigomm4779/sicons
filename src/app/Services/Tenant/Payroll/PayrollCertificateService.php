<?php

namespace App\Services\Tenant\Payroll;

use App\Helpers\Core\Traits\FileHandler;
use App\Helpers\Traits\DateRangeHelper;
use App\Helpers\Traits\SettingKeyHelper;
use App\Helpers\Traits\TenantAble;
use App\Mail\Tenant\EmployeePayslipMail;
use App\Models\Tenant\Payroll\PayrollCertificate;
use App\Repositories\Core\Setting\SettingRepository;
use App\Repositories\Core\Status\StatusRepository;
use App\Services\Tenant\Setting\SettingService as TenantSettingService;
use App\Services\Tenant\TenantService;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class PayrollCertificateService extends TenantService
{
    use SettingKeyHelper, TenantAble, DateRangeHelper, FileHandler;

    public function __construct(PayrollCertificate $payrollCertificate)
    {
        $this->model = $payrollCertificate;
    }

    public function validate(): self
    {
        validator($this->getAttributes(), [
            'name' => 'min:2',
            'user_id' => 'required',
        ])->validate();

        return $this;
    }

    public function sendPayslipToUser(): PayrollCertificateService
    {
        $storagePath = Storage::disk('public')->path($this->getAttribute('file_path'));
        try {
            Mail::to($this->model->user->email)
                ->send(new EmployeePayslipMail($this->model->user, $storagePath));
        } catch (\Exception $exception) {
            info($exception);
        }

        //TODO DELETE BOLETA 
        // Storage::delete($this->getAttribute('file_path'));

        return $this;
    }

    public function getRelations()
    {   
        return [
            'user:id,first_name,last_name,email,status_id,middle_name',
            'user.profile:id,user_id,gender,ruc_number,document_type_id,document_number',
            'user.profile.documentType:id,code,name,description',
        ]; 
    }

}
