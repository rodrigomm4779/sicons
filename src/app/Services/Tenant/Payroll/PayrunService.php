<?php


namespace App\Services\Tenant\Payroll;


use App\Helpers\Traits\DateRangeHelper;
use App\Helpers\Traits\DateTimeHelper;
use App\Helpers\Traits\TenantAble;
use App\Jobs\Tenant\RunDefaultPayrunJob;
use App\Models\Core\Auth\User;
use App\Models\Tenant\Attendance\Attendance;
use App\Models\Tenant\Attendance\AttendanceDetails;
use App\Models\Tenant\Holiday\Holiday;
use App\Models\Tenant\Payroll\Payrun;
use App\Models\Tenant\Payroll\Payslip;
use App\Models\Tenant\Maintenance\Agency;
use App\Models\Tenant\Employee\Designation;
use App\Repositories\Core\Setting\SettingRepository;
use App\Repositories\Core\Status\StatusRepository;
use App\Services\Tenant\Attendance\AttendanceSummaryService;
use App\Services\Tenant\Leave\LeaveCalendarService;
use App\Services\Tenant\Setting\SettingService as TenantSettingService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class PayrunService extends DefaultPayrunService
{
    use DateRangeHelper, DateTimeHelper, TenantAble;

    private AttendanceSummaryService $attendanceSummaryService;
    private LeaveCalendarService $leaveCalendarService;
    public $batch;
    private $totalSchedule = 0;

    public function __construct(
        LeaveCalendarService $leaveCalendarService,
        AttendanceSummaryService $attendanceSummaryService
    ){
        $this->leaveCalendarService = $leaveCalendarService;
        $this->attendanceSummaryService = $attendanceSummaryService;
    }

    public function runDefaultPayrun(): self
    {
        $this
            ->makePayrunFollowedBySettings($this->followedBySettings()->get())
            ->saveBatchId()
            ->makePayrunFollowedByEmployee($this->followedByEmployee()->get())
            ->saveBatchId();

        return $this;
    }

    public function saveBatchId():self
    {
        if ($this->batch) $this->model->update(['batch_id' => $this->batch->id]);

        return $this;
    }

    public function defaultSettingsInArray(): array
    {
        $settings = $this->defaultSettings();

        return [
            'consider_type' => $settings->setting->consider_type,
            'period' => $settings->setting->payrun_period,
            'consider_overtime' => $settings->setting->consider_overtime
        ];
    }

    public function defaultBeneficiariesInArray()
    {
        $settings = $this->defaultSettings();

        return $settings
            ->load('beneficiaries', 'beneficiaries.beneficiary:id,type')
            ->beneficiaries
            ->toArray();
    }

    public function makePayrunFollowedBySettings($users): self
    {
        if (!$this->countFollowedBySettings()){
            return $this;
        }

        $settings = $this->defaultSettings();

        $attributes = $this->defaultPayrunAttributes($settings);

        $beneficiaries = $this->defaultBeneficiariesInArray();

        $settings = $this->defaultSettingsInArray();

        $this->generateEmployeesPayslip(
            $this->savePayrun($attributes, $beneficiaries),
            $users,
            $settings,
            $beneficiaries
        );

        return $this;
    }

    public function defaultPayrunAttributes($settings): array
    {
        return [
            'name' => 'payrun_'.nowFromApp(),
            'status_id' => resolve(StatusRepository::class)->payrunGenerated(),
            'data' => json_encode(array_merge([
                'time_range' => $this->getDefaultPayrunTimeRange(),
                'employees' => $this->followedBySettings()->pluck('id')->toArray(),
                'type' => 'default',
                'consider_type' => $settings->setting->consider_type,
                'period' => $settings->setting->payrun_period,
                'consider_overtime' => $settings->setting->consider_overtime,
            ], $this->getCommonPayslipSettingData())),
            'followed' => 'settings',
        ];
    }

    public function savePayrun($attributes = [], $beneficiaries = []): Payrun
    {
        $payrun = new Payrun($attributes);
        $payrun->save();

        if (count($beneficiaries)){
            $payrun->beneficiaries()->createMany($beneficiaries);
        }

        $this->model = $payrun;

        return $payrun;
    }

    public function makePayrunFollowedByEmployee($users): self
    {

        // dd($users);

        if (!$this->countFollowedByEmployee()){
            return $this;
        }

        $settings = [
            'consider_type' => $this->employeesConsiderType(),
            'period' => $this->employeesPayrunPeriod(),
            'consider_overtime' => $this->employeesPayrunConsiderOvertime(),
        ];

        $attributes = $this->employeePayrunAttributes($settings);

        $this->employeePayrunPayslipGenerated($this->savePayrun($attributes), $users);

        return $this;
    }
 

    /**
     * 
     *
     * @param  Payrun  $payrun recibe la planilla, usuarios,
     * @return self  Itera sobre cada usuario para procesar boleta
     *
     */
    public function employeePayrunPayslipGenerated($payrun, $users, $manualSettings = [], $manualBeneficiaries = [], $ranges = [], $isManual = false): self
    {
        // Carga la configuración de planilla y los beneficios para cada usuario
        // TODO payrollInformation debe traer solo lo necesario
        // $users->load('payrunSetting', 'payrunBeneficiaries');
        $users->load('payrunSetting', 'payrunBeneficiaries', 'payrollInformation.pensionRegime', 'payrollInformation.healthRegime');

        // dd($users);
        // Inicia un nuevo lote de trabajo
        $this->batch = Bus::batch([])->dispatch();

        // Itera sobre cada usuario para procesar boleta
        foreach ($users as $user) {
            // Determina la configuración de nómina para el usuario
            if (count($manualSettings) > 0) {
                $settings = $manualSettings; // <--
            } else {
                $defaultSettings = $this->defaultSettingsInArray();
                $settings = [
                    'consider_type' => $user->payrunSetting ? $user->payrunSetting->consider_type : $defaultSettings['consider_type'],
                    'period' => $user->payrunSetting ? $user->payrunSetting->payrun_period : $defaultSettings['period'],
                    'consider_overtime' => $user->payrunSetting ? $user->payrunSetting->consider_overtime : $defaultSettings['consider_overtime'],
                ];
            }

            // Determina los beneficios para el usuario
            if ($isManual) {
                $beneficiaries = $manualBeneficiaries; // <--
            } elseif ($user->payrunBeneficiaries) {
                $beneficiaries = $user->payrunBeneficiaries->load('beneficiary')->toArray();
            } elseif (in_array($user->id, $this->restrictedUserForBadge())) {
                $beneficiaries = [];
            } else {
                $beneficiaries = $this->defaultBeneficiariesInArray();
            }

            // Agrega un trabajo al lote para generar el recibo del personal MANUAL
            $this->generatePayslip($payrun, $user, $settings, $beneficiaries, $ranges);
            // CRON
            // $this->batch->add(new RunDefaultPayrunJob($payrun, $user, $settings, $beneficiaries, $ranges));
        }

        return $this;
    }

    public function generateEmployeesPayslip($payrun, Collection $users, $settings, $beneficiaries): self
    {
        $this->batch = Bus::batch([])->dispatch();
        $users->map(function ($user) use ($payrun, $settings, $beneficiaries){
            $this->batch->add(new RunDefaultPayrunJob($payrun, $user, $settings, $beneficiaries));
//            RunDefaultPayrunJob::dispatch($payrun, $user, $settings, $beneficiaries);
            //$this->generatePayslip($payrun, $user, $settings, $beneficiaries);
        });

        return $this;
    }


    

    /**
     * El cron realiza llamada a la función. EXECUTE
     *
     * @param  recibe parametros para generar el boleta por cada usuario
     *
     */
    public function generatePayslip($payrun, User $user, $settings, $beneficiaries, $ranges = []): self
    {
        // dd($settings['total_bruto']);
        $ranges = count($ranges) ? $ranges : $this->getPreviousTimeRange($settings['period']);

        // 1. PARAMETROS GENERALES
        // $userRegPem = $user->payrollInformation->pensionRegime;
        // $userRegPenCom = $user->payrollInformation->commission;
        $userPayInf = $user->payrollInformation;
        // 1.1. Cargo from front designation_id
        // dd($settings);

        $userAgency = Agency::find($settings['agency_id'])->name ?? '';
        $userDesignation = Designation::find($settings['designation_id'])->name ?? '';

        // dd($settings['designation_id']);

        // 2. PROCESAR LOS CONCEPTOS DE LA BOLETA DESDE CONCEPTOS FIJOS REGISTRADOS PARA EL TRABAJADOR
        $payslipBeneficiaries = [];

        foreach ($beneficiaries as $beneficiary){
            $tmpBeneficiary = $beneficiary;
            // 2.2. DESPUES DE PROCESAR AGREGAR A NUEVO ARRAY DE CONCEPTOS DE BOLETA PROCESADOS
            $payslipBeneficiaries[] = $tmpBeneficiary;
        }

        // 3. GENERAR BOLETA DE PAGO (PAYSLIP) (ENCABEZADO)

        $payslip = new Payslip([
            'user_id' => $user->id,
            'status_id' => resolve(StatusRepository::class)->payslipGenerated(),
            'start_date' => $ranges[0],
            'end_date' => $ranges[1],
            'period' => $settings['period'],
            'consider_type' => $settings['consider_type'],
            'consider_overtime' => filter_var($settings['consider_overtime'], FILTER_VALIDATE_BOOLEAN),
            'basic_salary' => optional($user->salary($this->carbon($ranges[0])->parse())->first())->amount ?: 0,
            'net_salary' => $this->countNetSalary($user, $settings, $beneficiaries, $ranges), //REALIZA EL CÁLCULO
            'mes_periodo' => $this->formatMesPeriodo($ranges[0]),
            'mes_periodo_codigo' => $this->generateMonthYearCode($ranges[0]),
            'period_id' => null,
            'apellidos' => $user->last_name.' ' .$user->middle_name,
            'nombres' => $user->first_name,
            'fecha_nac' => $user->profile->date_of_birth,
            'airhsp_tipo_doc_codigo' => $user->profile->documentType->airhsp_code ?? '',
            'airhsp_tipo_reg_codigo' => $userPayInf->airhspType->code ?? '',
            'airhsp_num_reg' => $userPayInf->airhsp_code ?? '',
            'tipo_doc' => $user->profile->documentType->name ?? '',
            'nro_doc' => $user->profile->document_number ?? '',
            'num_ruc' => $user->profile->ruc_number ?? '',
            // 'establecimiento' => $userPayInf->department->name ?? '',
            'establecimiento' => $userAgency,
            'cargo' => $userDesignation,
            'tipo_servidor' => $userPayInf->publicEmployeType->name ?? '',
            'regimen_laboral' => $userPayInf->laborRegime->name ?? '',
            'grupo_ocupacional' => $userPayInf->occupationalGroup->name ?? '',
            'jornada_laboral' => $userPayInf->work_day ?? '',
            'num_cta' => $userPayInf->account_number ?? '',
            'inicio_contrato' => $userPayInf->contract_start_date ?? null,
            'fin_contrato' => $userPayInf->contract_end_date ?? null,
            'tipo_regimen' => $userPayInf->commission ?? '',
            'regimen_pension' => $userPayInf->pensionRegime->name ?? '',
            'fecha_afil' => null,
            'CUSPP' => $userPayInf->cussp ?? '',
            'dias_mes' => null,
            'dias_lab' => $settings['days_worked'],
            'leyenda_mensual' => $settings['note'],
            'total_bruto' => $settings['total_bruto'],
            'total_dscto' => $settings['total_dscto'],
            'total_aporte' => $settings['total_aporte'],
            'base_imponible' => $settings['base_imponible'],
            'total_liquido' => $settings['total_liquido'],
        ]);


        // dd($payslip);

        $payrun->payslips()->save($payslip);

        if ($payrun->followed == 'employee'){
            $payslip->beneficiaries()->createMany($payslipBeneficiaries);
        }
        //TODO OJO Realizar tratamiento antes de inserión como BOLETA
        if ($payrun->followed == 'customized'){
            $payslip->beneficiaries()->createMany($payslipBeneficiaries);
        }

        return $this;
    }

    /**
     * Calcula el salario neto de un usuario en función de la configuración y los conceptos proporcionados.
     *
     * @param  User  $user  El usuario para el cual se calcula el salario neto.
     * @param  array  $settings  La configuración de planilla para el usuario.
     * @param  array  $beneficiaries  Los conceptos de planilla del usuario.
     * @param  array  $ranges  (Opcional) Rangos de tiempo para calcular el salario (por defecto, se obtienen del período de planilla).
     * @return float  El salario neto calculado.
     */
    public function countNetSalary($user, $settings, $beneficiaries, $ranges = [])
    {

        // Si no se proporcionaron rangos de tiempo, se obtienen del período de nómina
        // TODO implementar descuento por días
        $ranges = count($ranges) ? $ranges : $this->getPreviousTimeRange($settings['period']);
        // TODO si no tiene regimen pensionario
        
        // Se parsean los rangos de tiempo para asegurar que sean objetos Carbon
        $ranges = [
            $this->carbon($ranges[0])->parse(),
            $this->carbon($ranges[1])->parse()
        ];

        // Se obtiene el salario del usuario, si está disponible, de lo contrario, se establece en 0
        $salary = optional($user->salary($ranges[0])->first())->amount ?: 0;

        // Se calcula la contribución de los conceptos al salario
        $beneficiaries = $this->countBeneficiaries($beneficiaries, $salary);

        // Según el tipo de consideración en la configuración, se calcula el salario neto //TODO FALTAS
        if ($settings['consider_type'] == 'daily_log') {
            // Se calcula el salario neto basado en registros diarios
            $earnSalary = $this->netSalaryDailyLogBased($user, $ranges, $salary);
            // Se calcula el salario final teniendo en cuenta la configuración
            $finalSalary = $this->finalSalaryCount($user, $ranges, $settings, $earnSalary, $salary);

            // Se retorna el salario final más la contribución de los beneficiarios
            return $finalSalary + $beneficiaries;
        }

        if ($settings['consider_type'] == 'hour') {
            // Se calcula el salario neto basado en horas trabajadas
            $earnSalary = $this->netSalaryHourBased($user, $ranges, $salary);
            // Se calcula el salario final teniendo en cuenta la configuración
            $finalSalary = $this->finalSalaryCount($user, $ranges, $settings, $earnSalary, $salary);

            // Se retorna el salario final más la contribución de los beneficiarios
            return $finalSalary + $beneficiaries;
        }

        // Si no se especifica un tipo de consideración, se calcula el salario neto sin considerarlo
        return $this->netSalaryWithoutConsiderType($user, $ranges, $salary) + $beneficiaries;
    }

    public function finalSalaryCount($user, $ranges, $settings, $earnSalary, $salary)
    {
        $rangeSchedule = $settings['consider_type'] == 'daily_log' ? $this->getTotalScheduledInDay($user, $ranges) :
            $this->getTotalScheduledInSec($user, $ranges);

        $totalSchedule = $settings['consider_type'] == 'daily_log' ? $this->countTotalScheduledDays($user, $ranges) :
            $this->countTotalScheduled($user, $ranges);

        $expectedSalary = $this->calculateSalary($salary, $totalSchedule, $rangeSchedule);

        return $settings['consider_overtime'] ? $earnSalary :
            ($earnSalary > $expectedSalary ? $expectedSalary : $earnSalary);
    }

    public function calculateSalary($salary, $totalSchedule, $work, $paidLeave = 0)
    {
        return (($salary / $totalSchedule)
            * ($work + $paidLeave));
    }

    public function netSalaryWithoutConsiderType($user, $ranges, $salary): float
    {
        return $salary;
    }

    public function calcNetSalaryWithoutConsiderType($user, $ranges, $beneficiaries): float
    {
        return $salary;
    }

    public function netSalaryHourBased($user, $ranges, $salary)
    {
        $paidLeave = $this->countPaidLeave($user, $ranges);

        $totalScheduled = $this->countTotalScheduled($user, $ranges);

        $worked = $this->countTotalWorked($user, $ranges);

        return $this->calculateSalary($salary, $totalScheduled, $worked, $paidLeave);
    }

    public function netSalaryDailyLogBased($user, $ranges, $salary)
    {
        $paidLeaveDate = $this->paidLeaveDays($user, $ranges);

        $totalScheduled = $this->countTotalScheduledDays($user, $ranges);

        $workedDate = $this->totalWorkedDate($user, $ranges);

        $worked = count(array_unique(array_merge($paidLeaveDate, $workedDate)));

        return $this->calculateSalary($salary, $totalScheduled, $worked);
    }

    public function totalWorkedDate($user, $ranges): array
    {
        return array_unique($user->load([
            'attendances' => fn(HasMany $builder) => $builder
                ->where('status_id', resolve(StatusRepository::class)->attendanceApprove())
                ->whereBetween(DB::raw('DATE(in_date)'), $this->convertRangesToStringFormat($ranges))
        ])->attendances->pluck('in_date')->toArray());
    }

    public function paidLeaveDays($user, $ranges)
    {
        return $this->leaveCalendarService
            ->setRanges($ranges)
            ->setEmployeeIds([$user->id])
            ->buildWorkshiftService()
            ->getTotalLeaveDurationInDays($user->load([
                'leaves' => function (HasMany $leave) use($ranges) {
                    $leave->where('status_id', resolve(StatusRepository::class)->leaveApproved())
                        ->whereHas('type', fn (Builder $leaveType) => $leaveType->where('type', 'paid'))
                        ->where(function (Builder $builder) use ($ranges){
                            $builder->whereBetween(DB::raw('DATE(start_at)'), $ranges)
                                ->orWhereBetween(DB::raw('DATE(end_at)'), $ranges);
                        });
                }])->leaves);
    }

    public function countTotalScheduledDays($user, $ranges): int
    {
        return $this->memoize('count-total-schedule-days-'.$user->id, function () use ($user, $ranges){
            $getRanges = $this->getDateRangesByMonthYear(
                $this->carbon($ranges[0])->parse()->monthName,
                $this->carbon($ranges[0])->parse()->year
            );

            $ranges = [
                $this->carbon($getRanges[0])->parse(),
                $this->carbon($getRanges[1])->parse(),
            ];

            return $this->getTotalScheduledInDay($user, $ranges);
        });
    }

    public function getTotalScheduledInDay($user, $ranges)
    {
        return $this->attendanceSummaryService
            ->setModel($user)
            ->setRanges($ranges)
            ->setHolidays(
                $this->attendanceSummaryService
                    ->generateEmployeeHolidaysFromDepartments($user->load('departments')->departments)
                    ->merge(Holiday::generalHolidays($ranges))
            )->getTotalScheduledDays();
    }

    public function countTotalWorkedDays($user, $ranges): int
    {
        return Attendance::query()->where('user_id', $user->id)
            ->where('status_id', resolve(StatusRepository::class)->attendanceApprove())
            ->whereBetween(DB::raw('DATE(in_date)'), $this->convertRangesToStringFormat($ranges))
            ->count();
    }

    public function countTotalWorked($user, $ranges)
    {
        $attendanceApprove = resolve(StatusRepository::class)->attendanceApprove();

        return Attendance::addSelect([
            'id',
            'user_id',
            'worked' => AttendanceDetails::whereColumn('attendance_id', 'attendances.id')
                ->where('status_id', $attendanceApprove)
                ->selectRaw(DB::raw('CAST(SUM(TIME_TO_SEC(TIMEDIFF(out_time, in_time))) AS SIGNED) AS worked')),
        ])->where('user_id', $user->id)
            ->where('status_id', $attendanceApprove)
            ->whereBetween(DB::raw('DATE(in_date)'), $this->convertRangesToStringFormat($ranges))
            ->get()->sum('worked');
    }

    public function countTotalScheduled($user, $ranges): int
    {
        return $this->memoize('count-total-schedule-hour-'.$user->id, function () use ($user, $ranges){
            $getRanges = $this->getDateRangesByMonthYear(
                $this->carbon($ranges[0])->parse()->monthName,
                $this->carbon($ranges[0])->parse()->year
            );

            $ranges = [
                $this->carbon($getRanges[0])->parse(),
                $this->carbon($getRanges[1])->parse(),
            ];

            return $this->getTotalScheduledInSec($user, $ranges);
        });

    }

    public function getTotalScheduledInSec($user, $ranges)
    {
        return $this->attendanceSummaryService
            ->setModel($user)
            ->setRanges($ranges)
            ->setHolidays(
                $this->attendanceSummaryService
                    ->generateEmployeeHolidaysFromDepartments($user->load('departments')->departments)
                    ->merge(Holiday::generalHolidays($ranges))
            )->getTotalScheduled();
    }

    public function countPaidLeave($user, $ranges): int
    {
        return $this->leaveCalendarService
            ->setRanges($ranges)
            ->setEmployeeIds([$user->id])
            ->buildWorkshiftService()
            ->getTotalLeaveDurationInSeconds($user->load([
                'leaves' => function (HasMany $leave) use($ranges) {
                    $leave->where('status_id', resolve(StatusRepository::class)->leaveApproved())
                        ->whereHas('type', fn (Builder $leaveType) => $leaveType->where('type', 'paid'))
                        ->where(function (Builder $builder) use ($ranges){
                            $builder->whereBetween(DB::raw('DATE(start_at)'), $ranges)
                                ->orWhereBetween(DB::raw('DATE(end_at)'), $ranges);
                        });
                }])->leaves);
    }

    //TODO calcula CORE tipo de  beneficio
    public function countBeneficiaries($beneficiaries, $salary)
    {
        $allowance = 0;
        $deduction = 0;
        $contribution = 0;

        foreach ($beneficiaries as $beneficiary){
            if ($beneficiary['beneficiary']['type'] == 'allowance'){
                $allowance +=  $beneficiary['is_percentage'] ?
                    (($beneficiary['amount'] / 100) * $salary) :
                    $beneficiary['amount'];
            }if ($beneficiary['beneficiary']['type'] == 'deduction'){
                $deduction +=  $beneficiary['is_percentage'] ?
                    (($beneficiary['amount'] / 100) * $salary) :
                    $beneficiary['amount'];
            }if ($beneficiary['beneficiary']['type'] == 'contribution'){
                $contribution +=  $beneficiary['is_percentage'] ?
                    (($beneficiary['amount'] / 100) * $salary) :
                    $beneficiary['amount'];
            }else{
                //TODO sin regimen
            }
        }
    }

    //Calcula total de descuentos despues de haber sido convertidos los porcentajes
    public function calcTotalDeductions($beneficiaries)
    {
        $total = 0;
        foreach ($beneficiaries as $beneficiary){
            if ($beneficiary['beneficiary']['type'] == 'deduction'){
                // Parsea el valor como float y luego redondea a dos decimales
                $amount = round(floatval($beneficiary['amount']), 2);
                $total += $amount;
            }
        }

        return $total;
    }

    //Calcula total de descuentos despues de haber sido convertidos los porcentajes
    public function calcTotalContributions($beneficiaries)
    {
        $total = 0;
        foreach ($beneficiaries as $beneficiary){
            if ($beneficiary['beneficiary']['type'] == 'contribution'){
                // Parsea el valor como float y luego redondea a dos decimales
                $amount = round(floatval($beneficiary['amount']), 2);
                $total += $amount;
            }
        }

        return $total;
    }

    /**
     * Calcula la remuneración sumando los montos de los conceptos que son asignaciones (allowance) y afectos a la ley HONORARIOS.
     *
     * @param  array  $beneficiaries  Los conceptos del usuario.
     * @return float  La remuneración calculada.
     */
    public function countRemuneracion($beneficiaries, $diasTotales, $diasLaborados)
    {
        $remuneracion = 0;

        foreach ($beneficiaries as $beneficiary){
            if ($beneficiary['beneficiary']['type'] == 'allowance'){
                 // Verificamos si el concepto es afecto a la ley HONORARIOS
                if ($beneficiary['beneficiary']['affection_law'] == 'HONORARIOS') {
                    // Si es afecto a la ley HONORARIOS, sumamos el monto a la remuneración
                    $remuneracion += $beneficiary['amount'];
                }
            }
        }

        $rem = round((floatval($remuneracion)/floatval($diasTotales))*floatval($diasLaborados) , 2);        

        return $rem;
    }

    /**
     * Calcula la base imponible sumando los habebes con modalidad de pago (modality) es Permanente y el concepto es Imponible (is_taxable)
     *
     * @param  array  $beneficiaries  Los conceptos del usuario.
     * @return float  La base imponible calculada.
     * TODO si un monto es parte de la base imponible y es porcentaje
     */
    public function countBaseImponible($beneficiaries, $remuneracion_basica, $incremento_DS311)
    {
        $baseImponible = 0;

        foreach ($beneficiaries as $beneficiary){
            // Verificamos si es un haber
            if ($beneficiary['beneficiary']['type'] == 'allowance'){
                 // Verificamos si la modalidad de pago es Permanente y el concepto es Imponible //TODO
                if ($beneficiary['beneficiary']['modality'] == 'Permanente' && $beneficiary['beneficiary']['is_taxable'] == 'SI') {
                    if ($beneficiary['beneficiary']['affection_law'] != 'HONORARIOS' && $beneficiary['beneficiary']['affection_law'] != 'DS311') {
                        //  Sumamos el monto a la base imponible
                        $baseImponible += $beneficiary['amount'];
                    }
                    
                }
            }
        }

        return $baseImponible + $remuneracion_basica + $incremento_DS311;
    }

    /**
     * Calcula el monto bruto del mes sumando los montos de los conceptos que son haberes (allowance).
     *
     * @param  array  $beneficiaries  Los conceptos del usuario.
     * @return float  El monto bruto del mes calculado.
     * TODO un haber puede ser porcentaje, se debe implementar a que concepto se saca el porcentaje
     */
    public function countMontoBrutoMes($beneficiaries)
    {
        $montoBrutoMes = 0;

        foreach ($beneficiaries as $beneficiary){
            // Verificamos si es un haber
            if ($beneficiary['beneficiary']['type'] == 'allowance'){
                //  Sumamos el monto
                $montoBrutoMes += $beneficiary['amount'];
            }
        }

        return $montoBrutoMes;
    }

    /**
     * Calcula el monto de otros conceptos porcentuales.
     *
     * @param  array  $baseImponible.
     * @return float  $porcentanje.
     */
    public function calcOtrosConceptos($baseImponible, $porcentanje)
    {
        // Convertir a float y calcular el porcentaje como fracción
        $porcentaje = floatval($porcentaje) / 100;
        $baseImponible = floatval($baseImponible);

        // Realizar el cálculo
        $response = round($baseImponible * $porcentaje, 2);

        return $response;
    }

    /**
     * Calcula el monto de aporte ESSALUD 
     * Para el cálculo de los aportes al EsSalud de los trabajadores CAS es 45% de la UIT.
     *
     * @param  array  $baseImponible.
     * @return float  $porcentanje.
     */
    
    public function calcRegimenSalud($baseImponible)
    {
        $settings = (object)resolve(TenantSettingService::class)
            ->getFormattedTenantSettings();

        $UIT = $settings->valor_UIT ?? 0; // Valor actual de la UIT, ajustar según corresponda
        $RMV = $settings->valor_RMV ?? 0; // Valor actual de la RMV, ajustar según corresponda
        $limiteMaximo = $UIT * (floatval($settings->porcentaje_max_uit_essalud ?? 0)/100); // Límite máximo basado en la UIT
        $tasaAporte = floatval($settings->porcentaje_aporte_essalud ?? 0)/100; // Tasa de aporte del 9%

        // Convertir a float la base imponible
        $baseImponible = floatval($baseImponible);

        // Asegurar que la base imponible no sea menor que la RMV ni exceda el límite máximo
        $baseCalculo = max(min($baseImponible, $limiteMaximo), $RMV);

        // Calcular el aporte a EsSalud
        $aporteEsSalud = round($baseCalculo * $tasaAporte, 2);

        return $aporteEsSalud;
    }

    // calcCuartaCat
    public function calcRetencionCuartaCategoria($baseImponible, $tieneSuspension)
    {
        $settings = (object)resolve(TenantSettingService::class)
            ->getFormattedTenantSettings();

        $UIT = $settings->valor_UIT ?? 0; // Valor de la UIT para el año en curso
        $limiteExento = floatval($settings->cant_UIT_exento_retencion_4TACAT ?? 7) * $UIT; // Límite para estar exento de retención (7 UIT)
        $tasaRetencion = floatval($settings->porcentaje_retencion_4TACAT)/100; // Tasa de retención de 8%

        // Convertir a float la base imponible
        $baseImponible = floatval($baseImponible);
        
        // Calcular la base imponible anualizada
        $baseImponibleAnual = $baseImponible * 12;

        // Verificar si se aplica la retención
        // TODO baseImponibleAnual
        if ($tieneSuspension && ($baseImponibleAnual < $limiteExento)) {
            $retencion = 0;
        } else {
            $retencion = round($baseImponible * $tasaRetencion, 2);
        }

        return $retencion;
    }


    public function calcAFP($concepto, $baseImponible, $UserRegimen, $tipoComision = 'flujo')
    {
        $baseImponible = floatval($baseImponible);
        $baseCalculo = min($baseImponible, floatval($UserRegimen->maxima_asegurable));

        if($concepto == 'APORTE'){
            return round($baseCalculo * (floatval($UserRegimen->aporte_obligatorio)/100), 2);
        }

        if($concepto == 'PRIMA'){
            return round($baseCalculo * (floatval($UserRegimen->prima_seguro)/100), 2);
        }

        if($concepto == 'COMISION'){
            if($tipoComision == 'flujo'){
                return round($baseCalculo * (floatval($UserRegimen->comision_flujo)/100), 2);
            }else if($tipoComision == 'mixta'){
                return 0;
            }
        }
    }

    public function calAFPComision($baseImponible, $UserRegimen, $tipoComision)
    {
        $baseImponible = floatval($baseImponible);
        $baseCalculo = min($baseImponible, floatval($UserRegimen->maxima_asegurable));

        return round($baseCalculo * (floatval($UserRegimen->aporte_obligatorio)/100), 2);
    }

    public function calRegimenPensionSPP($baseImponible, $UserRegimen, $tipoComision)
    {
        $baseImponible = floatval($baseImponible);
        $baseCalculo = min($baseImponible, floatval($UserRegimen->maxima_asegurable));

        $descuentoAporte = round($baseCalculo * (floatval($UserRegimen->aporte_obligatorio)/100), 2);
        $descuentoPrima = round($baseCalculo * (floatval($UserRegimen->prima_seguro)/100), 2);
        $descuentoComision = 0;
        
        if($tipoComision == 'flujo'){
            $descuentoComision = round($baseCalculo * (floatval($UserRegimen->comision_flujo)/100), 2);
        }

        $totalDescuentos = $descuentoAporte + $descuentoPrima + $descuentoComision;

        return [
            'base_calculo' => $baseCalculo,
            'descuento_aporte' => $descuentoAporte,
            'descuento_prima' => $descuentoPrima,
            'descuento_comision' => $descuentoComision,
            'total_descuentos' => $totalDescuentos,
        ];
    }

    public function calRegimenPensionSNP($baseImponible, $UserRegimen)
    {
        $tasaAporteONP = floatval($UserRegimen->aporte_obligatorio) / 100; // Asegurarse de que es un valor flotante

        // Cálculo del descuento
        $descuentoONP = round($baseImponible * $tasaAporteONP, 2); // Redondear a dos decimales

        return $descuentoONP;
    }

    public function calIncrementoDS311($beneficiaries, $diasTotales, $diasLaborados)
    {   
        $montoDS311 = 0;
        
        foreach ($beneficiaries as $beneficiary){
            // Verificamos si es un haber
            if ($beneficiary['beneficiary']['type'] == 'allowance'){
                if ($beneficiary['beneficiary']['affection_law'] == 'DS311') {
                    $montoDS311 = $beneficiary['amount'];
                }
            }
        }

        // mDS311=ROUND(DS311/30*DIASLAB,2) 
        $mDS311 = round((floatval($montoDS311)/floatval($diasTotales))*floatval($diasLaborados) , 2);

        return $mDS311;
    }


    public function employeePayrunAttributes($settings): array
    {
        return [
            'name' => 'payrun_'.nowFromApp(),
            'status_id' => resolve(StatusRepository::class)->payrunGenerated(),
            'data' => json_encode(array_merge([
                'time_range' => $this->employeePayrunTimeRange(),
                'employees' => $this->followedByEmployee()->pluck('id')->toArray(),
                'type' => 'default',
                'consider_type' => $settings['consider_type'],
                'period' => $settings['period'],
                'consider_overtime' => $settings['consider_overtime'],
            ], $this->getCommonPayslipSettingData())),
            'followed' => 'employee'
        ];
    }

    public function getCommonPayslipSettingData(): array
    {
        return [
            'note' => $this->getAttr('note'),
            'address' => $this->payslipSettings()['address'],
            'title' => $this->payslipSettings()['title'],
            'logo' => $this->payslipSettings()['logo'],
        ];
    }

    public function payslipSettings(): array
    {
        [$setting_able_id, $setting_able_type] = $this->tenantAble();

        $payslip_settings = (object)resolve(SettingRepository::class)
            ->getFormattedSettings('payslip', $setting_able_type, $setting_able_id);

        return [
            'address' => property_exists($payslip_settings, 'address') ? $payslip_settings->address : '',
            'title' => property_exists($payslip_settings, 'title') ? $payslip_settings->title : '',
            'logo' => property_exists($payslip_settings, 'logo') ? Storage::disk(config('filesystems.default'))->url($payslip_settings->logo) : '',
        ];
    }

    public function runManualPayrun(): self
    {
        return $this;
    }

    public function getDateRangesByMonthYear($month, $year): array
    {
        $date = $this->carbon($month.' '.$year)->parse();

        return [
            $date->startOfMonth()->toDateString(),
            $date->endOfMonth()->toDateString()
        ];
    }

    public function formatMesPeriodo($date, $locale = 'es')
    {
        $carbonDate = Carbon::parse($date)->locale($locale);
        $nombreMesEnEspanol = $carbonDate->translatedFormat('F');
        $anio = $carbonDate->year;

        return strtoupper($nombreMesEnEspanol) . ' - ' . $anio;
    }

    public function generateMonthYearCode($date)
    {
        $carbonDate = Carbon::parse($date);
        $codigoMesAnio = $carbonDate->format('Ym'); // Obtiene el año y el número de mes

        return $codigoMesAnio;
    }
}