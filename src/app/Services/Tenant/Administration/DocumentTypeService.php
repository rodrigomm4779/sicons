<?php


namespace App\Services\Tenant\Administration;


use App\Exceptions\GeneralException;
use App\Models\Tenant\Administration\DocumentType;
use App\Services\Tenant\TenantService;

class DocumentTypeService extends TenantService
{
    public function __construct(DocumentType $documentType)
    {
        $this->model = $documentType;
    }

    public function validate()
    {
        validator($this->getAttributes(), [
            'description' => 'required|min:2',
            'is_enabled' => 'required',
        ])->validate();

        return $this;
    }

}
