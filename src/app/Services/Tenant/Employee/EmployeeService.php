<?php

namespace App\Services\Tenant\Employee;

use App\Exceptions\GeneralException;
use App\Helpers\Core\Traits\HasWhen;
use App\Helpers\Traits\DepartmentAuthentications;
use App\Manager\Employee\EmployeeManager;
use App\Models\Core\Auth\Role;
use App\Models\Core\Auth\User;
use App\Models\Tenant\WorkingShift\DepartmentWorkingShift;
use App\Models\Tenant\WorkingShift\WorkingShift;
use App\Notifications\Core\User\UserNotification;
use App\Services\Core\Auth\UserService;
use App\Services\Tenant\TenantService;
use App\Services\Tenant\WorkingShift\WorkingShiftService;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Types\Boolean;

class EmployeeService extends TenantService
{
    use HasWhen, DepartmentAuthentications;

    private EmployeeManager $manager;

    private $isInvite = false;

    public function __construct(User $user, EmployeeManager $manager)
    {
        $this->model = $user;

        $this->manager = $manager;
    }


    public function update()
    {
        DB::transaction(function () {
            $this->model->fill($this->getAttributes('first_name', 'last_name', 'middle_name', 'email'));

            $this->when($this->model->isDirty(), fn (EmployeeService $service) =>  $service->notify('employee_updated'));

            $this->model->save();

            $this->saveEmployeeId();
            // $this->saveSalary(); // Guarda la información salarial;
            $this->saveAdditionalEmployeeAttributes(); // Guarda los atributos adicionales del empleado
            $this->savePayrollInformationId(); // Guarda atributos de informacion de planillas.

            $this->assignToDepartment()
                ->assignToDesignation()
                ->assignToWorkShift()
                ->when(auth()->user()->can('attach_users_to_roles'),fn(EmployeeService $service) => $service
                    ->validateRoles()
                    ->checkAndSetIfDepartmentManager()
                    ->assignToRoles()
                )->assignEmploymentStatus();
                
        });

        return $this;
    }

    public function checkAndSetIfDepartmentManager(): self
    {
        $isDepartmentManager = $this->model->roles()
            ->where('alias', 'department_manager')->select('id')->first();

        if ($isDepartmentManager){
            $this->setAttr('roles', array_merge($this->getAttr('roles'), [$isDepartmentManager->id]));
        }

        return $this;
    }

    public function validateRoles(): self
    {
        validator($this->getAttributes(),[
            'roles' => [
                'required',
                Rule::exists('roles', 'id')->where(function ($query) {
                    $query->whereIn('id', $this->getAttr('roles'));
                })
            ],
        ])->validate();

        return $this;
    }

    public function saveEmployeeId()
    {
        $this->when($this->getAttribute('employee_id'), function () {
            $this->model->profile()->updateOrCreate([
                'user_id' => $this->model->id
            ], [
                'user_id' => $this->model->id,
                'employee_id' => $this->getAttribute('employee_id'),
                'gender' => strtolower($this->getAttribute('gender'))
            ]);
        });

        return $this;
    }

    public function saveJoiningDate(): self
    {
        $this->when($this->getAttribute('joining_date'), function () {
            $this->model->profile()->updateOrCreate([
                'user_id' => $this->model->id
            ], [
                'user_id' => $this->model->id,
                'joining_date' => $this->getAttribute('joining_date')
            ]);
        });

        return $this;
    }

    public function saveSalary(): self
    {
        $this->when($this->getAttribute('salary'), function () {
            $this->model->salaries()->create([
                'user_id' => $this->model->id,
                'added_by' => auth()->id(),
                'amount' => $this->getAttribute('salary')
            ]);
        });

        return $this;
    }

    public function assignToRoles()
    {
        $this->when($this->getAttr('roles'), function (EmployeeService $service) {
            resolve(UserService::class)
                ->setModel($service->model)
                ->beforeAttachingRole()
                ->attachRole();
        });

        return $this;
    }

    public function assignRolesFromAttribute()
    {
        $this->model->roles()->sync($this->getAttr('roles'));

        return $this;
    }

    public function assignEmploymentStatus()
    {
        $this->when($this->getAttr('employment_status_id'), function (EmployeeService $service) {
            $service->manager
                ->walkInto('employmentStatus', $this->getAttr('employment_status_id'))
                ->setAttributes($this->getAttrs())
                ->assignEmployees($this->model->id);
        });

        return $this;
    }

    public function assignToWorkShift()
    {
        $this->when($this->getAttr('work_shift_id'), function (EmployeeService $service) {
            $service->manager
                ->walkInto('workShift', $this->getAttr('work_shift_id'))
                ->assignEmployees($this->model->id);
        });

        return $this;
    }

    public function assignToDesignation()
    {
        $this->when($this->getAttr('designation_id'), function (EmployeeService $service) {
            $service->manager
                ->walkInto('designation', $this->getAttr('designation_id'))
                ->assignEmployees($this->model->id);

        });

        return $this;
    }

    public function assignToDepartment()
    {
        $this->departmentAuthentications($this->getAttr('department_id'), false, 'department');

        $this->when($this->getAttr('department_id'), function(EmployeeService $service) {
            $service->manager
                ->walkInto('department', $this->getAttribute('department_id'))
                ->assignEmployees($this->model->id);
            
            $workingShiftId = DepartmentWorkingShift::getDepartmentWorkingShiftId($this->getAttr('department_id')) ?:
                WorkingShift::getDefault()->id;

            $this->when($workingShiftId && !$this->isInvite, function ($service) use ($workingShiftId) {
                resolve(WorkingShiftService::class)
                    ->setWorkShiftId((int) $workingShiftId)
                    ->departmentMoveChangeUpcomingWorkingShift([$this->model->id]);
                //->assignToUsers($this->model->id);
            });

            $this->when($workingShiftId && $this->isInvite, function ($service) use ($workingShiftId){
                resolve(WorkingShiftService::class)
                    ->setWorkShiftId((int) $workingShiftId)
                    ->assignToUsers($this->model->id);
            });

        });

        return $this;
    }

    public function notify($event, $model = null)
    {
        $model = $model ? $model : $this->model;

        notify()
            ->on($event)
            ->with($model)
            ->send(UserNotification::class);

        return $this;
    }

    public function assignToUpcomingWorkingShift()
    {
        $this->when($this->getAttr('work_shift_id'), function () {
            resolve(WorkingShiftService::class)
                ->setWorkShiftId((int) $this->getAttr('work_shift_id'))
                ->departmentMoveChangeUpcomingWorkingShift([$this->model->id]);
            //->assignToUsers($this->model->id);
        });
    }

    public function setIsInvite(bool $bool): self
    {
        $this->isInvite = $bool;

        return $this;
    }

    /**
     * Guarda o actualiza información adicional del empleado.
     */
    public function saveAdditionalEmployeeAttributes(): self
    {
        $this->model->profile()->updateOrCreate(
            ['user_id' => $this->model->id],
            [
                'date_of_birth' => $this->getAttribute('date_of_birth'),
                'marital_status_id' => $this->getAttribute('marital_status_id'),
                'document_type_id' => $this->getAttribute('document_type_id'),
                'document_number' => $this->getAttribute('document_number'),
                'ruc_number' => $this->getAttribute('ruc_number'),
                'personal_situation_id' => $this->getAttribute('personal_situation_id'),
                'worker_category_id' => $this->getAttribute('worker_category_id'),
                'education_level_id' => $this->getAttribute('education_level_id'),
                'entry_date' => $this->getAttribute('entry_date'),
                'disability' => $this->getAttribute('disability'),
                'unionized' => $this->getAttribute('unionized'),
                'resident' => $this->getAttribute('resident'),
                'phone_number' => $this->getAttribute('phone_number'),
                'entry_date' => $this->getAttribute('entry_date'),
                'gender' => $this->getAttribute('gender'),
            ]
        );

        return $this;
    }

    public function saveAdditionalEmployeeMasiveAttributes(): self
    {
        $this->model->profile()->updateOrCreate(
            ['user_id' => $this->model->id],
            [
                // 'date_of_birth' => $this->getAttribute('date_of_birth'),
                // 'marital_status_id' => $this->getAttribute('marital_status_id'),
                'document_type_id' => $this->getAttribute('document_type_id'),
                'document_number' => $this->getAttribute('document_number'),
                'ruc_number' => $this->getAttribute('ruc_number'),
                // 'personal_situation_id' => $this->getAttribute('personal_situation_id'),
                // 'worker_category_id' => $this->getAttribute('worker_category_id'),
                // 'education_level_id' => $this->getAttribute('education_level_id'),
                // 'entry_date' => $this->getAttribute('entry_date'),
                // 'disability' => $this->getAttribute('disability'),
                // 'unionized' => $this->getAttribute('unionized'),
                // 'resident' => $this->getAttribute('resident'),
                // 'phone_number' => $this->getAttribute('phone_number'),
                // 'entry_date' => $this->getAttribute('entry_date'),
                'gender' => $this->getAttribute('gender'),
            ]
        );

        return $this;
    }

    public function savePayrollInformationId(): self //TODO payrollInformation
    {
        // Define los atributos que se esperan recibir.
        $attributes = [
            'agency_id',
            'department_id',
            'designation_id',
            'worker_type_id',
            'cost_center_id',
            'pension_regime_id',
            'cussp',
            'commission',
            'contract_type_id',
            'labor_regime_id',
            'payment_frequency_id',
            'life_insurance_id',
            'payment_currency_id',
            'biweekly_percentage',
            'variable_commission_percentage',
            'job_rating_id',
            'job_rating_date',
            'is_contract',
            'contract_start_date',
            'contract_end_date',
            'airhsp_type_id',
            'modular_code',
            'nexus_code',
            'airhsp_code',
            'occupational_group_id',
            'public_employee_type_id',
            // 'work_day',
            'contract_number',
            'have_suspension_4ta',
            'nro_suspension',
            'date_suspension',
            'means_suspension',
            'have_health_insurance',
            'health_regime_id',
            'nro_health_insurance',
            'payment_type_id',
            'financial_entity_id',
            'account_number',
            'inter_account_number',
            'days_worked',
            'days_subsidized',
            'days_absent',
            'tardiness_hours',
            'tardiness_minutes',
            'total_days', 
            'total_days', 
            'basic_remuneration',
            'basic_remuneration_start_at',
        ];

        // Construye el array de datos usando los atributos esperados.
        $data = ['user_id' => $this->model->id];
        foreach ($attributes as $attribute) {
            $data[$attribute] = $this->getAttribute($attribute);
        }

        // Ejecuta updateOrCreate en el modelo de PayrollInformation.
        $this->model->payrollInformation()->updateOrCreate(
            ['user_id' => $this->model->id], // La clave por la que buscar.
            $data // Los datos para insertar o actualizar.
        );

        return $this;

    }
    
}
