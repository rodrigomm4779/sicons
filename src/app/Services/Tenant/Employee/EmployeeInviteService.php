<?php


namespace App\Services\Tenant\Employee;


use App\Helpers\Core\Traits\HasWhen;
use App\Mail\Tenant\EmployeeCreateMail;
use App\Mail\Tenant\EmployeeInvitationMail;
use App\Models\Core\Auth\User;
use App\Repositories\Core\Status\StatusRepository;
use App\Services\Core\Auth\UserInvitationService;
use App\Services\Settings\SettingService;
use App\Services\Tenant\TenantService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class EmployeeInviteService extends TenantService
{
    use HasWhen;

    protected EmployeeService $service;

    protected UserInvitationService $userService;

    public function __construct(EmployeeService $service, UserInvitationService $userService)
    {
        $this->service = $service;
        $this->userService = $userService;
    }

    public function validateMailSettings()
    {
        throw_if(
            !count(resolve(SettingService::class)->getCachedMailSettings()),
            ValidationException::withMessages([
                'email' => [__t('no_delivery_settings_found')]
            ])
        );

        return $this;
    }

    public function invite()
    {
        /**@var User $user*/
        $user = $this->userService
            ->create($this->getAttribute('email'), ['is_in_employee' => isset($this->attributes['is_in_employee']) ? $this->attributes['is_in_employee'] : 1])
            ->assignRoles($this->getAttributes('roles'))
            ->getModel();

        $this->service
            ->setModel($user)
            ->setAttributes($this->getAttributes())
            ->saveEmployeeId()
            ->saveJoiningDate()
            ->saveSalary()
            ->setIsInvite(true)
            ->assignToDepartment()
            ->assignToDesignation()
            ->assignEmploymentStatus();

        $user->load([
            'department:id,name',
            'designation:id,name',
            'profile:id,user_id,joining_date,employee_id',
            'workingShift:id,name',
            'employmentStatus:id,name,class',
            'roles:id,name'
        ]);

        $this->inviteEmployee($user);

        return $user;

    }

    public function create()
    {
        // Genera una contraseña aleatoria de 8 caracteres.
        $password = Str::random(8);
        $email = $this->getAttribute('email') == '' ? $this->getAttribute('dni').'@DREPUNO.NET.PE' : $this->getAttribute('email');

        // Crea un nuevo usuario (empleado) utilizando el servicio de invitación de usuarios.
        /**@var User $user */
        $user = $this->userService
            ->create(
                // Obtiene el correo electrónico del atributo y lo pasa al servicio de creación.
                // $this->getAttribute('email'),
                $email,
                // Fusiona otros atributos necesarios para la creación del usuario.
                array_merge(
                    $this->getAttributes(['first_name', 'last_name', 'middle_name']), // Obtiene el nombre y apellido.
                    [
                        // Establece 'is_in_employee' a un valor predeterminado si no se proporciona.
                        'is_in_employee' => isset($this->attributes['is_in_employee']) ? $this->attributes['is_in_employee'] : 1,
                        // Obtiene el ID de estado de usuario activo.
                        'status_id' => resolve(StatusRepository::class)->userActive(),
                        // Establece el token de invitación a una cadena vacía.
                        'invitation_token' => '',
                        // Encripta la contraseña generada y la asigna al usuario.
                        'password' => Hash::make($password)
                    ]
                ),
                true
            )
            // Asigna roles al usuario basándose en los atributos proporcionados.
            ->assignRoles($this->getAttributes('roles'))
            ->getModel();

        // Configura el usuario en el servicio EmployeeService y guarda varios atributos.
        $this->service
            ->setModel($user)
            ->setAttributes($this->getAttributes())
            ->saveEmployeeId() // Guarda el ID del empleado.
            ->saveJoiningDate() // Guarda la fecha de incorporación.
            ->saveSalary() // Guarda la información salarial.
            ->setIsInvite(false) // Establece que no es una invitación.
            ->assignToDepartment() // Asigna al departamento.
            ->assignToDesignation() // Asigna el cargo.
            ->saveAdditionalEmployeeAttributes() // Guarda atributos adicionales del empleado.
            ->savePayrollInformationId() // Guarda atributos de informacion de planillas.
            ->assignEmploymentStatus(); // Asigna el estado de empleo.

        // Carga relaciones adicionales del usuario para incluirlas en el objeto.
        $user->load([
            'department:id,name',
            'designation:id,name',
            'profile:id,user_id,joining_date,employee_id',
            'workingShift:id,name',
            'employmentStatus:id,name,class',
            'roles:id,name'
        ]);

        // Guarda la contraseña en una propiedad temporal para uso posterior.
        // $user->tempPass = $password; 

        // Envía un correo electrónico al empleado creado con la información relevante. TODO CORREO USUARIO
        // $this->createEmployeeMail($user);

        // Devuelve el objeto del usuario creado.
        return $user;
    }

    public function createMasive()
    {
        // dd($this->getAttribute('email'));
        // Genera una contraseña aleatoria de 8 caracteres.
        $password = Str::random(8);

        // Crea un nuevo usuario (empleado) utilizando el servicio de invitación de usuarios.
        /**@var User $user */
        $user = $this->userService
            ->create(
                // Obtiene el correo electrónico del atributo y lo pasa al servicio de creación.
                $this->getAttribute('email'),
                // Fusiona otros atributos necesarios para la creación del usuario.
                array_merge(
                    $this->getAttributes(['first_name', 'last_name', 'middle_name']), // Obtiene el nombre y apellido.
                    [
                        // Establece 'is_in_employee' a un valor predeterminado si no se proporciona.
                        'is_in_employee' => isset($this->attributes['is_in_employee']) ? $this->attributes['is_in_employee'] : 1,
                        // Obtiene el ID de estado de usuario activo.
                        'status_id' => resolve(StatusRepository::class)->userActive(),
                        // Establece el token de invitación a una cadena vacía.
                        'invitation_token' => '',
                        // Encripta la contraseña generada y la asigna al usuario.
                        'password' => Hash::make($password)
                    ]
                ),
                true
            )
            // Asigna roles al usuario basándose en los atributos proporcionados.
            // ->assignRoles($this->getAttributes('roles'))
            ->getModel();

        // Configura el usuario en el servicio EmployeeService y guarda varios atributos.
        $this->service
            ->setModel($user)
            // ->setAttributes($this->getAttributes())
            // ->saveEmployeeId() // Guarda el ID del empleado.
            // ->saveJoiningDate() // Guarda la fecha de incorporación.
            // ->saveSalary() // Guarda la información salarial.
            // ->setIsInvite(false) // Establece que no es una invitación.
            // ->assignToDepartment() // Asigna al departamento.
            // ->assignToDesignation() // Asigna el cargo.
            ->saveAdditionalEmployeeMasiveAttributes(); // Guarda atributos adicionales del empleado.
            // ->savePayrollInformationId() // Guarda atributos de informacion de planillas.
            // ->assignEmploymentStatus(); // Asigna el estado de empleo.

        // Carga relaciones adicionales del usuario para incluirlas en el objeto.
        // $user->load([
        //     'department:id,name',
        //     'designation:id,name',
        //     'profile:id,user_id,joining_date,employee_id',
        //     'workingShift:id,name',
        //     'employmentStatus:id,name,class',
        //     'roles:id,name'
        // ]);

        // Guarda la contraseña en una propiedad temporal para uso posterior.
        // $user->tempPass = $password; 

        // Envía un correo electrónico al empleado creado con la información relevante. TODO CORREO USUARIO
        // $this->createEmployeeMail($user);

        // Devuelve el objeto del usuario creado.
        return $user;
    }

    public function inviteEmployee(User $user)
    {
        Mail::to($user)
            ->locale(session()->get('locale') ?: settings('language') ?: "en")
            ->send((new EmployeeInvitationMail($user))->onQueue('high')->delay(5));

        return $this;
    }

    public function createEmployeeMail(User $user)
    {
        Mail::to($user)
            ->locale(session()->get('locale') ?: settings('language') ?: "en")
            ->send((new EmployeeCreateMail($user))->onQueue('high')->delay(5));

        return $this;
    }

    public function cancel()
    {
        /**@var User $user*/
        $user = $this->model;

        $user->departments()->sync([]);

        $user->upcomingWorkingShift()->delete();

        $user->designations()->sync([]);

        $user->employmentStatuses()->sync([]);

        $user->workingShifts()->sync([]);

        $user->salaries()->delete();

        $user->profile()->delete();

        $user->assignedLeaves()->delete();

        $this->userService
            ->setModel($user)
            ->detachRoles()
            ->delete();

        return $user;
    }

    public function validateRoles(): self
    {
        validator($this->getAttributes(),[
            'roles' => [
                'required',
                Rule::exists('roles', 'id')->where(function ($query) {
                    $query->whereIn('id', $this->getAttr('roles'));
                })
            ],
        ])->validate();

        return $this;
    }

}