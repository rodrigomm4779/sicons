<?php

namespace App\Filters\Tenant;

use App\Filters\Core\traits\NameFilter;
use App\Filters\Core\traits\SearchFilterTrait;
use App\Filters\FilterBuilder;
use App\Helpers\Traits\UserAccessQueryHelper;
use Illuminate\Database\Eloquent\Builder;

class GeneralMaintenanceFilter extends FilterBuilder
{
    use NameFilter, SearchFilterTrait, UserAccessQueryHelper;

    public function search($search = null)
    {
        $this->builder->when($search, function (Builder $builder) use ($search) {
            $builder->where('name', 'LIKE', "%$search%")
                    ->orWhere('code', 'LIKE', "%{$search}%");
        });
    }

}