<?php

namespace App\Filters\Tenant;

use App\Filters\Core\traits\SearchFilterTrait;
use App\Filters\FilterBuilder;
use Illuminate\Database\Eloquent\Builder;

class ConceptAirshpFilter extends FilterBuilder
{
    use SearchFilterTrait;

    public function search($search = null)
    {
        $this->builder->when($search, function (Builder $builder) use ($search) {
            $builder->where('concept_code', 'LIKE', "%{$search}%")
                    ->orWhere('concept_type', 'LIKE', "%{$search}%")
                    ->orWhere('description', 'LIKE', "%{$search}%");
        });
    }

    public function type($value = null)
    {
        $this->whereClause('type_code', $value);
    }

    // Aquí puedes agregar otros métodos de filtro específicos para este modelo
}
