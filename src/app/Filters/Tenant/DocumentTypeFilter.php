<?php


namespace App\Filters\Tenant;


use App\Filters\Core\traits\DescriptionFilter;
use App\Filters\Core\traits\SearchFilterTrait;
use App\Filters\FilterBuilder;
use App\Filters\Traits\DateRangeFilter;
use Illuminate\Database\Eloquent\Builder;

class DocumentTypeFilter extends FilterBuilder
{
    // use DateRangeFilter, DescriptionFilter, SearchFilterTrait;

    // public function type($type = null)
    // {
    //     $this->builder->when($type, function (Builder $builder) use ($type) {
    //         $builder->whereIn('type', explode(',', $type));
    //     });
    // }

    public function search($search = null)
    {
        $this->builder->when($search, function (Builder $builder) use ($search) {
            $builder->where('description', 'LIKE', "%$search%");
                // ->orWhere('code', 'LIKE', "%{$search}%")
                // ->orWhere('serial_number', 'LIKE', "%$search%");
        });
    }
}
