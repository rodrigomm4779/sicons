<?php


namespace App\Filters\Tenant;


use App\Filters\FilterBuilder;
use App\Filters\Traits\DateRangeFilter;
use App\Filters\Traits\FilterThroughDepartment;
use App\Filters\Traits\SearchThroughUserFilter;
use App\Filters\Traits\StatusFilterTrait;
use App\Helpers\Traits\MakeArrayFromString;
use Illuminate\Database\Eloquent\Builder;

class PayrollCertificateFilter extends FilterBuilder
{
    use DateRangeFilter,
        MakeArrayFromString,
        SearchThroughUserFilter,
        FilterThroughDepartment,
        StatusFilterTrait;

    public function search($search = null)
    {
        return $this->builder->when($search, function (Builder $builder) use ($search) {
            $builder->where(function (Builder $builder) use ($search) {
                $builder->where('name', 'LIKE', "%$search%")
                    ->orWhere('ref_exp_number', 'LIKE', "%{$search}%");
            });
        });
    }

    public function users($value = null)
    {
        $this->whereClause('user_id', $value);
    }

    public function type($type = null)
    {
//        $types = $this->makeArray($type);
        $this->builder->when($type, function (Builder $builder) use ($type) {
            $builder->whereHas(
                'payrun',
                fn(Builder $b) => $b->whereJsonContains('data->type', $type)
            );
        });
    }
    public function payrunPeriod($period = null)
    {
//        $periods = $this->makeArray($period);
        $this->builder->when($period, function (Builder $builder) use ($period) {
            $builder->whereHas(
                'payrun',
                fn(Builder $b) => $b->whereJsonContains('data->period', $period)
            );
        });
    }

    public function payrun($payrun = null)
    {
        $this->builder->when($payrun, function (Builder $builder) use ($payrun) {
            $builder->whereHas(
                'payrun',
                fn(Builder $b) => $b->where('uid', $payrun)
            );
        });
    }

}