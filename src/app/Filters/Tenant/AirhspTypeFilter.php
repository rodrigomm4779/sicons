<?php

namespace App\Filters\Tenant;

use App\Filters\Core\traits\SearchFilterTrait;
use App\Filters\FilterBuilder;
use Illuminate\Database\Eloquent\Builder;

class AirhspTypeFilter extends FilterBuilder
{
    use SearchFilterTrait;

    public function search($search = null)
    {
        $this->builder->when($search, function (Builder $builder) use ($search) {
            $builder->where('name', 'LIKE', "%$search%")
                    ->orWhere('code', 'LIKE', "%{$search}%");
        });
    }
}
