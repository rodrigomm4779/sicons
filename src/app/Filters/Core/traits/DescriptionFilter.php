<?php


namespace App\Filters\Core\traits;


trait DescriptionFilter
{
    public function description($description = null)
    {
        $this->whereClause('description', "%{$description}%", "LIKE");
    }
}
