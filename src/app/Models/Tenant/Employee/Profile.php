<?php


namespace App\Models\Tenant\Employee;

use App\Models\Tenant\Maintenance\DocumentType;

use App\Models\Core\Auth\Profile as BaseProfile;
use App\Models\Tenant\Traits\ProfileRelationship;

class Profile extends BaseProfile
{
    use ProfileRelationship;

    protected $fillable = [
        'gender', 'date_of_birth', 'address', 'contact', 'joining_date', 'employee_id', 'user_id', 'phone_number',
        'marital_status', 'fathers_name', 'mothers_name', 'social_security_number', 'department_id', 'designation_id', 'about_me', 'document_type_id', 'document_number', 'ruc_number', 'personal_situation_id', 'worker_category_id', 'education_level_id', 'entry_date', 'disability', 'unionized', 'resident'
    ];

    protected $dates = [
        'date_of_birth', 'joining_date', 'entry_date'
    ];

    public function documentType()
    {
        return $this->belongsTo(DocumentType::class, 'document_type_id');
    }

    
}
