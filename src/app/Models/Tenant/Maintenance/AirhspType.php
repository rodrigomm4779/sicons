<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class AirhspType extends TenantModel
{
    protected $table = 'airhsp_types';
    
    protected $fillable = [
        'code', 'name', 'description'
    ];

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }
}
