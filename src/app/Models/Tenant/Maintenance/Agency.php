<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class Agency extends TenantModel
{
    protected $table = 'agencies';
    
    protected $fillable = [
        'tenant_id', 'code', 'name'
    ];
}
