<?php


namespace App\Models\Tenant\Maintenance;


use App\Models\Tenant\TenantModel;
use App\Repositories\Core\Status\StatusRepository;
use Illuminate\Database\Eloquent\Builder;

class Department extends TenantModel
{
    protected $table = 'man_departments';
    protected $primaryKey = 'code';
    public $incrementing = false;

    protected $fillable = [
        'code', 
        'name',
    ];

    public function provinces()
    {
        return $this->hasMany(Province::class, 'department_code', 'code');
    }

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }

}
