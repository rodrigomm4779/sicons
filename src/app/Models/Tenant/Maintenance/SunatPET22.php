<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class SunatPET22 extends TenantModel
{
    protected $table = 'sunat_pe_t22';
    
    protected $fillable = [
        'codigo', 'descripcion', 'essalud_seguro_regular_trabajador', 'essalud_cbs_sp_seg_trabajador_pesquero', 'essalud_seguro_agrario_acuicultor', 'essalud_sctr', 'impuesto_extraord_de_solidaridad', 'fondo_derechos_sociales_del_artista', 'senati', 'sistema_nacional_de_pensiones_19990', 'sistema_privado_de_pensiones', 'renta_5ta_categoria_retenciones', 'essalud_seguro_regular_pensionista', 'contrib_solidaria_asistencia_prev'
    ];
}
