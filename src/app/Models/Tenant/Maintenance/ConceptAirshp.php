<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class ConceptAirshp extends TenantModel
{
    protected $table = 'concept_airshp';
    
    protected $fillable = [
        'ue_code', 'executing_unit', 'type_code', 'concept_type', 'concept_code', 'name', 'description', 'is_taxable', 'modality'
    ];
}
