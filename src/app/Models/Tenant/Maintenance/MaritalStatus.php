<?php


namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use App\Repositories\Core\Status\StatusRepository;
use Illuminate\Database\Eloquent\Builder;

class MaritalStatus extends TenantModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'marital_statuses';
    
    protected $fillable = [
        'code', 'name'
    ];


}
