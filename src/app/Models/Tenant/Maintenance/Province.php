<?php


namespace App\Models\Tenant\Maintenance;


use App\Models\Tenant\TenantModel;
use App\Repositories\Core\Status\StatusRepository;
use Illuminate\Database\Eloquent\Builder;

class Province  extends TenantModel
{
    protected $table = 'man_provinces';
    protected $primaryKey = 'code';
    public $incrementing = false;
    // protected $keyType = 'string';
    public $timestamps = true;

    protected $fillable = [
        'code', // Asumiendo que quieres que este campo sea asignable
        'name',
        'department_code', // Asegúrate de que este campo esté correctamente asignado
    ];

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_code', 'code');
    }

    public function districts()
    {
        return $this->hasMany(District::class, 'province_code', 'code');
    }

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }

}
