<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class PayrollClass extends TenantModel
{
    protected $table = 'payroll_classes';
    
    protected $fillable = [
        'code', 'name', 'description', 'is_active'
    ];

    public function scopeIsActive(Builder $query, $value)
    {
        return $query->where('is_active', $value);
    }
    
    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }
}
