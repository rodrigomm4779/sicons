<?php


namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use App\Repositories\Core\Status\StatusRepository;
use Illuminate\Database\Eloquent\Builder;

class WorkerType extends TenantModel
{
    // Especificar el nombre de la tabla
    protected $table = 'worker_types';
    
    protected $fillable = [
        'code', 'name', 'description', 'is_enabled', 'is_public_sector', 'is_private_sector'
    ];

    // Scope para filtrar por is_enabled
    public function scopeIsEnabled(Builder $query, $value)
    {
        return $query->where('is_enabled', $value);
    }

    // Scope para filtrar por is_public_sector
    public function scopeIsPublicSector(Builder $query, $value)
    {
        return $query->where('is_public_sector', $value);
    }

    // Scope para filtrar por is_private_sector
    public function scopeIsPrivateSector(Builder $query, $value)
    {
        return $query->where('is_private_sector', $value);
    }

}
