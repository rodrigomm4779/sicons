<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class CostCenter extends TenantModel
{
    protected $table = 'cost_centers';
    
    protected $fillable = [
        'code', 'name', 'options'
    ];
}
