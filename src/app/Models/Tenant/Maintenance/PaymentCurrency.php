<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class PaymentCurrency extends TenantModel
{
    protected $table = 'payment_currencies';
    
    protected $fillable = [
        'code', 'name'
    ];
}
