<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class ContractType extends TenantModel
{
    protected $table = 'contract_types';
    
    protected $fillable = [
        'code', 'name', 'description', 'options'
    ];
}
