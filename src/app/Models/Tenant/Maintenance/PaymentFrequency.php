<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class PaymentFrequency extends TenantModel
{
    protected $table = 'payment_frequencies';
    
    protected $fillable = [
        'code', 'name', 'description', 'options'
    ];
}