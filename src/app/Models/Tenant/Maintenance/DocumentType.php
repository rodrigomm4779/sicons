<?php


namespace App\Models\Tenant\Maintenance;


// use App\Models\Tenant\Leave\Boot\LeaveCategoryBoot;
use App\Models\Tenant\TenantModel;
use App\Repositories\Core\Status\StatusRepository;
// use App\Services\Tenant\Administration\DocumentTypeService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DocumentType extends TenantModel
{
    // use LeaveCategoryBoot {
    //     boot as public traitBoot;
    // }

    protected $fillable = [
        'is_enabled', 'description', 'name', 'airhsp_code'
    ];


    public function scopeActive($query)
    {
        return $query->where('is_enabled', true);
    }

    // Accesor para code-name
    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }

}
