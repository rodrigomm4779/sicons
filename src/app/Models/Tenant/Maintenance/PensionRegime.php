<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class PensionRegime extends TenantModel
{
    protected $table = 'pension_regimes';
    
    protected $fillable = [
        'code', 'name', 'description', 'is_active', 'options',
        'prefijo','comision_flujo','comision_mixta','comision_anual_saldo','prima_seguro','aporte_obligatorio','maxima_asegurable'
    ];

    public function scopeIsActive(Builder $query, $value)
    {
        return $query->where('is_active', $value);
    }

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }
}