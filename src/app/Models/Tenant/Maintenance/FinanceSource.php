<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class FinanceSource extends TenantModel
{
    protected $table = 'finance_sources';
    
    protected $fillable = [
        'code', 'name', 'description', 'is_active'
    ];

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }
}
