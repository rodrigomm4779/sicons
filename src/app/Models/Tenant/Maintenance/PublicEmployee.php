<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class PublicEmployee extends TenantModel
{
    protected $table = 'public_employees';
    
    protected $fillable = [
        'code', 'name', 'description'
    ];

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }
}
