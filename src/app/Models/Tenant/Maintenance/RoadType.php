<?php


namespace App\Models\Tenant\Maintenance;


use App\Models\Tenant\TenantModel;
use App\Repositories\Core\Status\StatusRepository;
use Illuminate\Database\Eloquent\Builder;

class RoadType extends TenantModel
{
    protected $table = 'road_types';

    protected $fillable = [
        'is_enabled', 'description', 'name', 'code'
    ];

    public static array $types = ['paid', 'unpaid', 'special'];

    public function scopeActive($query)
    {
        return $query->where('is_enabled', true);
    }

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }

}
