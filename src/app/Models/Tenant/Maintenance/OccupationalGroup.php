<?php


namespace App\Models\Tenant\Maintenance;


use App\Models\Tenant\TenantModel;
use App\Repositories\Core\Status\StatusRepository;
use Illuminate\Database\Eloquent\Builder;

class OccupationalGroup extends TenantModel
{
    protected $table = 'occupational_groups';

    protected $fillable = [
        'description', 'name', 'code'
    ];

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }

}
