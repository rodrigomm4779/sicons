<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class EducationLevel extends TenantModel
{
    protected $table = 'education_levels';
    
    protected $fillable = [
        'name','description','code','is_active'
    ];

    // Scope para filtrar por is_active
    public function scopeIsActive(Builder $query, $value)
    {
        return $query->where('is_active', $value);
    }
    
}
