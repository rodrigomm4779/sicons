<?php


namespace App\Models\Tenant\Maintenance;


use App\Models\Tenant\TenantModel;
use App\Repositories\Core\Status\StatusRepository;
use Illuminate\Database\Eloquent\Builder;

class District  extends TenantModel
{
    protected $table = 'man_districts';
    protected $primaryKey = 'code';
    public $incrementing = false;
    // protected $keyType = 'string';
    // public $timestamps = true;

    protected $fillable = [
        'code', // Asumiendo que quieres que este campo sea asignable
        'name',
        'province_code', // Asegúrate de que este campo esté correctamente asignado
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_code', 'code');
    }

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }

}
