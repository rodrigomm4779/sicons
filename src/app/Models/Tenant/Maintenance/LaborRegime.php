<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class LaborRegime extends TenantModel
{
    protected $table = 'labor_regimes';
    
    protected $fillable = [
        'code', 'name', 'description', 'options'
    ];

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }
}
