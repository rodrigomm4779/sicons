<?php

namespace App\Models\Tenant\Maintenance;

use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Builder;

class HealthRegime extends TenantModel
{
    protected $table = 'health_regimes';

    protected $fillable = [
        'code', 'name', 'description', 'is_active'
    ];

    public function getOptionAttribute()
    {
        return "{$this->code} - {$this->name}";
    }
}
