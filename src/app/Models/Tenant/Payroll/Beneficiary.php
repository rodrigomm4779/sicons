<?php

namespace App\Models\Tenant\Payroll;

use App\Models\Core\BaseModel;
use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tenant\Maintenance\ConceptAirshp;
use App\Models\Tenant\Maintenance\SunatPeTable22;

class Beneficiary extends TenantModel
{
    use HasFactory;

    protected $fillable = ['name', 'type', 'description', 'is_active', 'modality', 'is_taxable', 'concept_pdt_public_id', 'concept_pdt_private_id', 'concept_airshp_id', 'sunat_pe_t22_codigo'];

    public function values()
    {
        return $this->hasMany(BeneficiaryValue::class);
    }

    // Método para la relación con ConceptAirshp
    public function conceptAirshp()
    {
        return $this->belongsTo(ConceptAirshp::class, 'concept_airshp_id');
    }

    public function sunatPeTable22()
    {
        return $this->belongsTo(SunatPeTable22::class, 'sunat_pe_t22_codigo', 'codigo');
    }
}
