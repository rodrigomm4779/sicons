<?php

namespace App\Models\Tenant\Payroll;

use App\Models\Tenant\Maintenance\Agency;
use App\Models\Tenant\Employee\Department;
use App\Models\Tenant\Employee\Designation;
use App\Models\Tenant\Maintenance\WorkerType;
use App\Models\Tenant\Maintenance\CostCenter;
use App\Models\Tenant\Maintenance\PensionRegime;
use App\Models\Tenant\Maintenance\HealthRegime;
use App\Models\Tenant\Maintenance\ContractType;
use App\Models\Tenant\Maintenance\LaborRegime;
use App\Models\Tenant\Maintenance\PaymentFrequency;
use App\Models\Tenant\Maintenance\PublicEmployee;
use App\Models\Tenant\Maintenance\OccupationalGroup;
// use App\Models\Tenant\Maintenance\LifeInsurance; // TODO
use App\Models\Tenant\Maintenance\PaymentCurrency;
use App\Models\Tenant\Maintenance\AirhspType;

use App\Models\Core\BaseModel;
use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayrollInformation extends TenantModel
{
    protected $table = 'payroll_information';

    protected $fillable = [
        'agency_id', 'department_id', 'designation_id', 'worker_type_id',
        'cost_center_id', 'pension_regime_id', 'cussp', 'commission',
        'contract_type_id', 'labor_regime_id', 'payment_frequency_id',
        'life_insurance_id', 'payment_currency_id', 'biweekly_percentage',
        'variable_commission_percentage', 'job_rating_id', 'job_rating_date',
        'integral_salary', 'exempt_fifth_category_income', 'is_contract',
        'contract_start_date', 'contract_end_date', 'airhsp_type_id',

        'modular_code',        'nexus_code',        'airhsp_code',        'occupational_group_id',        'public_employee_type_id',        'work_day',        'contract_number',        'have_suspension_4ta',        'nro_suspension',        'date_suspension',        'means_suspension',        'have_health_insurance',        'health_regime_id',        'nro_health_insurance',        'payment_type_id',        'financial_entity_id',        'account_number',        'inter_account_number',        'days_worked',        'days_subsidized',        'days_absent',        'tardiness_hours',        'tardiness_minutes',        'total_days', 'basic_remuneration', 'basic_remuneration_start_at',
    ];

    public function agency()
    {
        return $this->belongsTo(Agency::class, 'agency_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class, 'designation_id');
    }

    public function workerType()
    {
        return $this->belongsTo(WorkerType::class, 'worker_type_id');
    }

    public function costCenter()
    {
        return $this->belongsTo(CostCenter::class, 'cost_center_id');
    }

    public function pensionRegime()
    {
        return $this->belongsTo(PensionRegime::class, 'pension_regime_id');
    }

    public function healthRegime()
    {
        return $this->belongsTo(HealthRegime::class, 'health_regime_id');
    }

    public function contractType()
    {
        return $this->belongsTo(ContractType::class, 'contract_type_id');
    }

    public function laborRegime()
    {
        return $this->belongsTo(LaborRegime::class, 'labor_regime_id');
    }

    public function paymentFrequency()
    {
        return $this->belongsTo(PaymentFrequency::class, 'payment_frequency_id');
    }

    public function paymentCurrency()
    {
        return $this->belongsTo(PaymentCurrency::class, 'payment_currency_id');
    }

    public function airhspType()
    {
        return $this->belongsTo(AirhspType::class, 'airhsp_type_id');
    }

    public function publicEmployeType()
    {
        return $this->belongsTo(PublicEmployee::class, 'public_employee_type_id');
    }

    public function occupationalGroup()
    {
        return $this->belongsTo(OccupationalGroup::class, 'occupational_group_id');
    }

    
    // relaciones, scopes o métodos adicionales
}