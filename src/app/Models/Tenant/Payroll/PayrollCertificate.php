<?php

namespace App\Models\Tenant\Payroll;

use App\Models\Core\Auth\User;
use App\Models\Tenant\Maintenance\Agency;
use App\Models\Tenant\Employee\Designation;
use App\Models\Core\Traits\StatusRelationship;
use App\Models\Tenant\TenantModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\This;

class PayrollCertificate extends TenantModel
{
    use HasFactory, StatusRelationship;

    protected $fillable = [
        'generate_user_id',
        'user_id',
        'name',
        'description',
        'token',
        'filename',
        'certificate_type',
        'start_date',
        'end_date',
        'allowances',
        'deductions',
        'legend',
        'agency_id',
        'designation_id',
        'niv',
        'mag',
        'ref_exp_number',
        'issue_date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function createBy()
    {
        return $this->belongsTo(User::class, 'generate_user_id');
    }

    public function agency()
    {
        return $this->belongsTo(Agency::class);
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }

}
