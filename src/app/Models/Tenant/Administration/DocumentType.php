<?php


namespace App\Models\Tenant\Administration;


// use App\Models\Tenant\Leave\Boot\LeaveCategoryBoot;
use App\Models\Tenant\TenantModel;
use App\Repositories\Core\Status\StatusRepository;
// use App\Services\Tenant\Administration\DocumentTypeService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DocumentType extends TenantModel
{
    // use LeaveCategoryBoot {
    //     boot as public traitBoot;
    // }

    protected $fillable = [
        'is_enabled', 'description', 'name', 'airhsp_code'
    ];


    // public static function boot()
    // {
    //     self::traitBoot();
    // }

}
