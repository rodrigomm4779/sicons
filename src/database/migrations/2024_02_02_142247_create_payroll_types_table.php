<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_types', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('name');
            $table->text('description')->nullable();
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });

        // Inserción de datos
        DB::table('payroll_types')->insert([
            ['code' => '01', 'name' => 'ACTIVO', 'description' => '', 'is_active' => true],
            ['code' => '02', 'name' => 'PENSIONISTA', 'description' => '', 'is_active' => false],
            ['code' => '03', 'name' => 'BENEFICIARIO', 'description' => '', 'is_active' => false],
            ['code' => '04', 'name' => 'DESCUENTO JUDICIAL', 'description' => '', 'is_active' => false],
            ['code' => '05', 'name' => 'JUDICIAL', 'description' => '', 'is_active' => false],
            ['code' => '06', 'name' => 'PAGO ÚNICO', 'description' => '', 'is_active' => false],
            ['code' => '07', 'name' => 'PROVISIONAL', 'description' => '', 'is_active' => false],
        ]);        

        DB::statement("ALTER TABLE payroll_types COMMENT = 'Tipos de planilla'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_types');
    }
};
