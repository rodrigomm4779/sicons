<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('payroll_information', function (Blueprint $table) {
            $table->string('modular_code', 50)->nullable();
            $table->string('nexus_code', 50)->nullable();
            $table->string('airhsp_code', 50)->nullable();
            $table->unsignedBigInteger('occupational_group_id')->nullable();
            $table->unsignedBigInteger('public_employee_type_id')->nullable();
            $table->integer('work_day')->nullable()->default(0);
            $table->string('contract_number', 50)->nullable();
            $table->boolean('have_suspension_4ta')->default(false);
            $table->string('nro_suspension', 20)->nullable();
            $table->date('date_suspension')->nullable();
            $table->string('means_suspension', 20)->nullable();
            $table->boolean('have_health_insurance')->default(true);
            $table->unsignedBigInteger('health_regime_id')->nullable();
            $table->string('nro_health_insurance', 20)->nullable();
            $table->unsignedBigInteger('payment_type_id')->nullable();
            $table->unsignedBigInteger('financial_entity_id')->nullable();
            $table->string('account_number', 50)->nullable();
            $table->string('inter_account_number', 50)->nullable();
            $table->integer('days_worked')->nullable()->default(0);
            $table->integer('days_subsidized')->nullable()->default(0);
            $table->integer('days_absent')->nullable()->default(0);
            $table->integer('tardiness_hours')->nullable()->default(0);
            $table->integer('tardiness_minutes')->nullable()->default(0);
            $table->integer('total_days')->nullable()->default(0);
            $table->double('basic_remuneration')->default(0);
            $table->date('basic_remuneration_start_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('payroll_information', function (Blueprint $table) {
            // Revertir los cambios en caso de un rollback
            $table->dropColumn([
                'modular_code',
                'nexus_code',
                'airhsp_code',
                'occupational_group_id',
                'public_employee_type_id',
                'work_day',
                'contract_number',
                'have_suspension_4ta',
                'nro_suspension',
                'date_suspension',
                'means_suspension',
                'have_health_insurance',
                'health_regime_id',
                'nro_health_insurance',
                'payment_type_id',
                'financial_entity_id',
                'account_number',
                'inter_account_number',
                'days_worked',
                'days_subsidized',
                'days_absent',
                'tardiness_hours',
                'tardiness_minutes',
                'total_days',
            ]);
        });
    }
};
