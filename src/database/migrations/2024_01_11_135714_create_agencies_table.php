<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tenant_id')->default(1);
            $table->string('code');
            $table->string('name');
            $table->timestamps();
        });

        DB::table('agencies')->insert([
            ['tenant_id' => 1, 'code' => '01', 'name' => 'AGENCIA PRINCIPAL'],
        ]);

        DB::statement("ALTER TABLE agencies COMMENT = 'Agencias'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
};
