<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payslips', function (Blueprint $table) {
            // Aquí puedes definir las mismas columnas sin ninguna modificación
            $table->string('mes_periodo')->nullable();
            $table->string('mes_periodo_codigo')->nullable();
            $table->unsignedBigInteger('period_id')->nullable();
            $table->string('apellidos')->nullable();
            $table->string('nombres')->nullable();
            $table->date('fecha_nac')->nullable();
            $table->char('airhsp_tipo_doc_codigo', 2)->nullable();
            $table->char('airhsp_tipo_reg_codigo', 2)->nullable();
            $table->string('airhsp_num_reg')->nullable();
            $table->string('tipo_doc')->nullable();
            $table->string('nro_doc')->nullable();
            $table->string('establecimiento')->nullable();
            $table->string('cargo')->nullable();
            $table->string('tipo_servidor')->nullable();
            $table->string('regimen_laboral')->nullable();
            $table->string('grupo_ocupacional')->nullable();
            $table->string('jornada_laboral')->nullable();
            $table->string('num_ruc')->nullable();
            $table->string('num_cta')->nullable();
            $table->date('inicio_contrato')->nullable();
            $table->date('fin_contrato')->nullable();
            $table->string('tipo_regimen')->nullable();
            $table->string('regimen_pension')->nullable();
            $table->date('fecha_afil')->nullable();
            $table->string('CUSPP')->nullable();
            $table->integer('dias_mes')->nullable();
            $table->integer('dias_lab')->nullable();
            $table->string('leyenda_mensual')->nullable();
            $table->decimal('total_bruto', 10, 2)->nullable();
            $table->decimal('total_dscto', 10, 2)->nullable();
            $table->decimal('total_aporte', 10, 2)->nullable();
            $table->decimal('base_imponible', 10, 2)->nullable();
            $table->decimal('total_liquido', 10, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payslips', function (Blueprint $table) {
            $table->string('mes_periodo');
            $table->string('mes_periodo_codigo');
            $table->unsignedBigInteger('period_id');
            $table->string('apellidos');
            $table->string('nombres');
            $table->date('fecha_nac');
            $table->string('tipo_doc');
            $table->string('nro_doc');
            $table->string('establecimiento');
            $table->string('cargo');
            $table->string('tipo_servidor');
            $table->string('regimen_laboral');
            $table->string('grupo_ocupacional');
            $table->string('jornada_laboral');
            $table->string('num_ruc');
            $table->string('num_cta');
            $table->date('inicio_contrato');
            $table->date('fin_contrato');
            $table->string('tipo_regimen');
            $table->string('regimen_pension');
            $table->date('fecha_afil');
            $table->string('CUSPP');
            $table->integer('dias_mes');
            $table->integer('dias_lab');
            $table->string('leyenda_mensual');
            $table->decimal('total_bruto', 10, 2);
            $table->decimal('total_dscto', 10, 2);
            $table->decimal('base_imponible', 10, 2);
            $table->decimal('total_liquido', 10, 2);
        });
    }
};
