<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labor_regimes', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->string('description');
            $table->text('options')->nullable();
            $table->timestamps();
        });

        DB::table('labor_regimes')->insert([
            ['code' => '01', 'name' => 'OTRO NO PREVISTOS', 'description' => 'OTRO NO PREVISTOS', 'options' => null],
            // ... otros registros
        ]);

        DB::statement("ALTER TABLE labor_regimes COMMENT = 'Regímenes Laborales'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labor_regimes');
    }
};
