<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concept_airshp', function (Blueprint $table) {
            $table->id();
            $table->string('ue_code')->nullable()->comment('Código de unidad ejecutora');
            $table->string('executing_unit')->nullable()->comment('Unidad ejecutora');
            $table->string('type_code', 10)->comment('Código de tipo concepto 1/2/3');
            $table->string('concept_type')->comment('Tipo de concepto');
            $table->string('concept_code')->nullable()->comment('Código del concepto');
            $table->text('name')->comment('Descripción');
            $table->text('description')->nullable()->comment('Detalle');
            $table->enum('is_taxable', ['SI', 'NO'])->nullable()->comment('Imponible');
            $table->enum('modality', ['Permanente', 'Ocacional'])->nullable()->comment('Modalidad de entrega');
        });

        // Insertar datos
        DB::table('concept_airshp')->insert([
            [
                'ue_code' => '000910',
                'executing_unit' => '300 REGION PUNO-EDUCACIÓN PUNO',
                'type_code' => '1',
                'concept_type' => 'Ingresos',
                'concept_code' => '0131',
                'name' => 'Honorarios por servicios profesionales/técnicos',
                'description' => 'Honorarios por servicios profesionales/técnicos',
                'is_taxable' => true,
                'modality' => 'Permanente'
            ],
            [
                'ue_code' => '000910',
                'executing_unit' => '300 REGION PUNO-EDUCACIÓN PUNO',
                'type_code' => '1',
                'concept_type' => 'Ingresos',
                'concept_code' => '0897',
                'name' => 'DS 311-2022-EF',
                'description' => 'Incremento Mensual de Servidores, directivos y funcionarios del DL 728, 1057, Leyes 30057, 29709, 28091',
                'is_taxable' => true,
                'modality' => 'Permanente'
            ]
        ]);

        DB::statement("ALTER TABLE concept_airshp COMMENT = 'Catalogo de conceptos AIRHSP'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concept_airshp');
    }
};
