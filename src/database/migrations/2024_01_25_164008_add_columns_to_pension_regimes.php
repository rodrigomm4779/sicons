<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pension_regimes', function (Blueprint $table) {
            $table->char('prefijo', 10)->nullable();
            $table->double('comision_flujo')->default(0);
            $table->double('comision_mixta')->nullable();
            $table->double('comision_anual_saldo')->nullable();
            $table->double('prima_seguro')->default(0);
            $table->double('aporte_obligatorio')->default(0);
            $table->double('maxima_asegurable')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pension_regimes', function (Blueprint $table) {
            $table->dropColumn([
                'prefijo',
                'comision_flujo',
                'comision_mixta',
                'comision_anual_saldo',
                'prima_seguro',
                'aporte_obligatorio',
                'maxima_asegurable',
            ]);
        });
    }
};
