<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     * Niveles educativos
     * @return void
     */
    public function up()
    {
        Schema::create('education_levels', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('code')->nullable();
            $table->boolean('is_active')->default(1);
            $table->string('options')->nullable();
            $table->timestamps();
        });

        DB::table('education_levels')->insert([
            ['name' => 'SIN EDU. FORMAL'],
            ['name' => 'EDU. ESPECIAL INCOMPLETA'],
            ['name' => 'EDU. ESPECIAL COMPLETA'], 
            ['name' => 'EDU. PRIMARIA INCOMPLETA'], 
            ['name' => 'EDU. PRIMARIA COMPLETA'], 
            ['name' => 'EDU. SECUNDARIA INCOMPLETA'], 
            ['name' => 'EDU. SECUNDARIA COMPLETA'], 
            ['name' => 'EDU. TECNICA INCOMPLETA'], 
            ['name' => 'EDU. TECNICA COMPLETA'], 
            ['name' => 'EDU. SUPERIOR INCOMPLETA'], 
            ['name' => 'EDU. SUPERIOR COMPLETA'], 
            // ... otros niveles educativos
        ]);

        DB::statement("ALTER TABLE education_levels COMMENT = 'Niveles educativos'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_levels');
    }
};
