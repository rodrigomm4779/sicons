<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('code')->nullable();
            $table->boolean('is_enabled')->default(true);
            $table->timestamps();
        });

        // Inserción del registro
        DB::table('worker_types')->insert([
            'name' => 'ESTABLE',
            'code' => '01',
            'is_enabled' => true,
        ],
        [
            'name' => 'CONTRATADO',
            'code' => '02',
            'is_enabled' => true,
        ],
        [
            'name' => 'EVENTUAL',
            'code' => '03',
            'is_enabled' => true,
        ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_types');
    }
};
