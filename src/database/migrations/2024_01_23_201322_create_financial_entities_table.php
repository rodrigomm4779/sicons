<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_entities', function (Blueprint $table) {
            $table->id();
            $table->char('code', 10)->unique();
            $table->string('name');
            $table->text('description')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });

        // Insertar los valores
        $financialEntities = [
            ['code' => '002', 'name' => 'BANCO DE CREDITO DEL PERU', 'description' => null],
            ['code' => '003', 'name' => 'INTERBANK', 'description' => null],
            ['code' => '007', 'name' => 'CITIBANK DEL PERU', 'description' => null],
            ['code' => '009', 'name' => 'SCOTIABANK PERU', 'description' => null],
            ['code' => '011', 'name' => 'BBVA BANCO CONTINENTAL', 'description' => null],
            ['code' => '018', 'name' => 'BANCO DE LA NACION', 'description' => null],
            ['code' => '020', 'name' => 'BANCO FALABELLA', 'description' => null],
            ['code' => '023', 'name' => 'BANCO DE COMERCIO', 'description' => null],
            ['code' => '035', 'name' => 'BANCO FINANCIERO DEL PERU', 'description' => null],
            ['code' => '038', 'name' => 'BANCO INTERAMERICANO DE FINANZAS', 'description' => null],
            ['code' => '043', 'name' => 'CREDISCOTIA FINANCIERA', 'description' => null],
            ['code' => '053', 'name' => 'BANCO GNB', 'description' => null],
            ['code' => '056', 'name' => 'SANTANDER', 'description' => null],
            ['code' => '057', 'name' => 'BANCO AZTECA', 'description' => null],
            ['code' => '058', 'name' => 'BANCO CENCOSUD', 'description' => null],
            ['code' => '059', 'name' => 'BANCO RIPLEY', 'description' => null],
            ['code' => '060', 'name' => 'ICBC PERU BANK', 'description' => null],
            ['code' => '070', 'name' => 'MIBANCO', 'description' => null],
            ['code' => '200', 'name' => 'FINANC.CREDINKA', 'description' => null],
            ['code' => '202', 'name' => 'FINANC.PROEMPRESA', 'description' => null],
            ['code' => '204', 'name' => 'FINANC.CONFIANZA', 'description' => null],
            ['code' => '206', 'name' => 'CREDIRAIZ', 'description' => null],
            ['code' => '208', 'name' => 'COMPARTAMOS FINANCIERA', 'description' => null],
            ['code' => '210', 'name' => 'FINANCIERA QAPAQ', 'description' => null],
            ['code' => '212', 'name' => 'FINANCIERA TFC S A', 'description' => null],
            ['code' => '214', 'name' => 'FINANCIERA EFECTIVA', 'description' => null],
            ['code' => '216', 'name' => 'AMERIKA FINANCIERA', 'description' => null],
            ['code' => '218', 'name' => 'FINANCIERA OH!', 'description' => null],
            ['code' => '800', 'name' => 'CAJA METROPOLITANA DE LIMA', 'description' => null],
            ['code' => '802', 'name' => 'CMAC TRUJILLO', 'description' => null],
            ['code' => '803', 'name' => 'CMAC AREQUIPA', 'description' => null],
            ['code' => '805', 'name' => 'CMAC SULLANA', 'description' => null],
            ['code' => '806', 'name' => 'CMAC CUSCO', 'description' => null],
            ['code' => '808', 'name' => 'CMAC HUANCAYO', 'description' => null],
            ['code' => '813', 'name' => 'CMAC TACNA', 'description' => null],
            ['code' => '820', 'name' => 'CMAC DEL SANTA', 'description' => null],
            ['code' => '822', 'name' => 'CMAC ICA', 'description' => null],
            ['code' => '824', 'name' => 'CMAC PIURA', 'description' => null],
            ['code' => '826', 'name' => 'CMAC MAYNAS', 'description' => null],
            ['code' => '828', 'name' => 'CMAC PAITA', 'description' => null],
            ['code' => '900', 'name' => 'CRAC SIPAN', 'description' => null],
            ['code' => '902', 'name' => 'CRAC DEL CENTRO', 'description' => null],
            ['code' => '904', 'name' => 'CRAC INCASUR', 'description' => null],
            ['code' => '906', 'name' => 'CRAC PRYMERA', 'description' => null],
            ['code' => '908', 'name' => 'CRAC LOS ANDES', 'description' => null],
        ];

        DB::table('financial_entities')->insert($financialEntities);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_entities');
    }
};
