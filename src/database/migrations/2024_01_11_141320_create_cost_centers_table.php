<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cost_centers', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->text('options')->nullable();
            $table->timestamps();
        });

        DB::table('cost_centers')->insert([
            ['code' => '01', 'name' => 'ADMINISTRACION', 'options' => null],
        ]);

        DB::statement("ALTER TABLE cost_centers COMMENT = 'Centro de costos'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cost_centers');
    }
};
