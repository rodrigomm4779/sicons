<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_employees', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Insertar los valores
        DB::table('public_employees')->insert([
            ['code' => 'EC', 'name' => 'EMPLEADO DE CONFIANZA', 'description' => ''],
            ['code' => 'FP', 'name' => 'FUNCIONARIO PÚBLICO', 'description' => ''],
            ['code' => 'SP', 'name' => 'SERVIDOR PÚBLICO', 'description' => ''],
        ]);

        DB::statement("ALTER TABLE public_employees COMMENT = 'Tipos de empleado público'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public_employees');
    }
};
