<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('road_Types', function (Blueprint $table) {
            $table->id();
            $table->string('code', 4)->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->boolean('is_enabled')->default(true);
            $table->timestamps();
        });

        // Insertar valores predefinidos
        DB::table('road_Types')->insert([
            ['code' => '01', 'name' => 'AVENIDA'],
            ['code' => '02', 'name' => 'JIRÓN'],
            ['code' => '03', 'name' => 'CALLE'],
            ['code' => '04', 'name' => 'PASAJE'],
            ['code' => '05', 'name' => 'ALAMEDA'],
            ['code' => '06', 'name' => 'MALECÓN'],
            ['code' => '07', 'name' => 'OVALO'],
            ['code' => '08', 'name' => 'PARQUE'],
            ['code' => '09', 'name' => 'PLAZA'],
            ['code' => '10', 'name' => 'CARRETERA'],
            ['code' => '11', 'name' => 'BLOCK'],
        ]);

        DB::statement("ALTER TABLE road_Types COMMENT = 'TABLA 5: VÍA'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('road_Types');
    }
};
