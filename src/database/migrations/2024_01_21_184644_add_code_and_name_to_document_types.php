<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_types', function (Blueprint $table) {
            // Agrega los nuevos campos
            $table->string('code', 4)->nullable();
            $table->string('name')->nullable();
        });

        // Inserta los nuevos registros
        DB::table('document_types')->insert([
            ['code' => '01', 'name' => 'DNI', 'description' => 'DOC. NACIONAL DE IDENTIDAD', 'is_enabled' => true],
            ['code' => '04', 'name' => 'CE', 'description' => 'CARNÉ DE EXTRANJERÍA', 'is_enabled' => true],
            ['code' => '06', 'name' => 'RUC', 'description' => 'REG. ÚNICO DE CONTRIBUYENTES', 'is_enabled' => true],
            ['code' => '07', 'name' => 'PASAPORTE', 'description' => 'PASAPORTE', 'is_enabled' => true],
            ['code' => '11', 'name' => 'PARTIDA DE NACIMIENTO', 'description' => 'PARTIDA DE NACIMIENTO', 'is_enabled' => true]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_types', function (Blueprint $table) {
            // Remueve los campos agregados
            $table->dropColumn('code');
            $table->dropColumn('name');
        });
    }
};
