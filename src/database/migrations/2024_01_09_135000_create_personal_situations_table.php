<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     * Situaciones de Personal
     * @return void
     */
    public function up()
    {
        Schema::create('personal_situations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('code')->nullable();
            $table->boolean('is_active')->default(1);
            $table->string('options')->nullable();
            $table->timestamps();
        });

        DB::table('personal_situations')->insert([
            ['name' => 'VIGENTE'],
            ['name' => 'VACACIONES'],
            ['name' => 'DESCANSO PRE/POS NATAL'],
            ['name' => 'LICENCIA Y/O ENFERMEDAD'],
            ['name' => 'CESADO'],
            ['name' => 'INCAPACIDAD TEMPORAL'],
            ['name' => 'A CESAR'],
            // ... otras situaciones de personal
        ]);

        DB::statement("ALTER TABLE personal_situations COMMENT = 'Situación de Personal VIGENTE/VACACIONES/...etc'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_situations');
    }
};
