<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_types', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->string('description');
            $table->text('options')->nullable();
            $table->timestamps();
        });

        DB::table('contract_types')->insert([
            ['code' => '01', 'name' => 'CAS', 'description' => 'CONTRATO ADMINISTRATIVO DE SERVICIOS', 'options' => null],
        ]);

        DB::statement("ALTER TABLE contract_types COMMENT = 'Tipos de Contratos'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_types');
    }
};
