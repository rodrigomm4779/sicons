<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_certificates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('generate_user_id');
            $table->unsignedBigInteger('user_id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('token')->nullable();
            $table->string('filename')->nullable();
            $table->enum('certificate_type', ['individual', 'mass']);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->text('allowances')->nullable();
            $table->text('deductions')->nullable();
            $table->text('legend')->nullable();
            $table->unsignedBigInteger('agency_id')->nullable();
            $table->unsignedBigInteger('designation_id')->nullable();
            $table->string('niv')->nullable();
            $table->string('mag')->nullable();
            $table->string('ref_exp_number')->nullable();
            $table->date('issue_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_certificates');
    }
};
