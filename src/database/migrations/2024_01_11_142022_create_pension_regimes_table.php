<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pension_regimes', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->boolean('is_active')->default(true);
            $table->enum('type', ['SNP', 'SPP', 'SRP']);
            $table->text('options')->nullable();
            $table->timestamps();
        });

        DB::table('pension_regimes')->insert([
            ['code' => 'DL19990', 'name' => 'DECRETO LEY 19990 - SISTEMA NACIONAL DE PENSIONES', 'type' => 'SNP'],
            ['code' => 'DL20530', 'name' => 'DECRETO LEY 20530', 'type' => 'SNP'],
            ['code' => 'CBSSP', 'name' => 'CAJA DE BENEFICIOS DE SEGURIDAD SOCIAL DEL PESCADO', 'type' => 'SNP'],
            ['code' => 'CPMIL', 'name' => 'CAJA DE PENSIONES MILITAR', 'type' => 'SNP'],
            ['code' => 'CPPOL', 'name' => 'CAJA DE PENSIONES POLICIAL', 'type' => 'SNP'],
            ['code' => 'ORPEN', 'name' => 'OTROS REGIMENES PENSIONARIOS', 'type' => 'SNP'],
            ['code' => 'RSDR', 'name' => 'REGIMEN DEL SERVICIO DIPLOMATICO DE LA REPUBLICA', 'type' => 'SNP'],
            ['code' => 'SPPINT', 'name' => 'SPP INTEGRA', 'type' => 'SPP'],
            ['code' => 'SPPHOR', 'name' => 'SPP HORIZONTE', 'type' => 'SPP'],
            ['code' => 'SPPPRO', 'name' => 'SPP PROFUTURO', 'type' => 'SPP'],
            ['code' => 'SPPPRI', 'name' => 'SPP PRIMA', 'type' => 'SPP'],
            ['code' => 'SRP', 'name' => 'SIN REGIMEN PENSIONARIO', 'type' => 'SRP'],
        ]);

        DB::statement("ALTER TABLE pension_regimes COMMENT = 'Regímenes de Pensiones'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pension_regimes');
    }
};
