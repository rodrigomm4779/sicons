<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marital_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('code', 20);
            $table->string('name', 100);
            $table->timestamps();
        });

        // Insertar los estados civiles
        DB::table('marital_statuses')->insert([
            ['code' => 'S', 'name' => 'Soltero/a'],
            ['code' => 'C', 'name' => 'Casado/a'],
            ['code' => 'D', 'name' => 'Divorciado/a'],
            ['code' => 'V', 'name' => 'Viudo/a'],
            ['code' => 'CV', 'name' => 'Conviviente']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marital_statuses');
    }
};
