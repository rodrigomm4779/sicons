<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('occupational_groups', function (Blueprint $table) {
            $table->id();
            $table->char('code', 4);
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Insertar los valores proporcionados en la imagen
        DB::table('occupational_groups')->insert([
            ['code' => '01', 'name' => 'FUNCIONARIOS Y DIRECTIVOS', 'description' => null],
            ['code' => '02', 'name' => 'PROFESIONALES', 'description' => null],
            ['code' => '03', 'name' => 'TÉCNICOS', 'description' => null],
            ['code' => '04', 'name' => 'AUXILIARES', 'description' => null],
            ['code' => '10', 'name' => 'PROFESIONALES DE LA SALUD', 'description' => null]
        ]);

        DB::statement("ALTER TABLE occupational_groups COMMENT = 'Tipos de grupos ocupacionales'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('occupational_groups');
    }
};
