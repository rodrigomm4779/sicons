<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     * Categorías de trabajador
     * @return void
     */
    public function up()
    {
        Schema::create('worker_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('code')->nullable();
            $table->boolean('is_active')->default(1);
            $table->string('options')->nullable();
            $table->timestamps();
        });

        DB::table('worker_categories')->insert([
            ['name' => 'Trabajador'],
            ['name' => 'Pensionista'],
            ['name' => 'Personal en formación laboral'],
            ['name' => 'Personal de Terceros'],            
            // ... otras categorías de trabajador
        ]);

        DB::statement("ALTER TABLE worker_categories COMMENT = 'Categoría de trabajador. Dependiendo de la categoría que seleccionada se activarán diferentes pantallas para completar la información laboral'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_categories');
    }
};
