<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sunat_pe_t22', function (Blueprint $table) {
            $table->char('codigo', 4)->primary();
            $table->text('descripcion');
            $table->string('essalud_seguro_regular_trabajador', 20)->nullable();
            $table->string('essalud_cbs_sp_seg_trabajador_pesquero', 20)->nullable();
            $table->string('essalud_seguro_agrario_acuicultor', 20)->nullable();
            $table->string('essalud_sctr', 20)->nullable();
            $table->string('impuesto_extraord_de_solidaridad', 20)->nullable();
            $table->string('fondo_derechos_sociales_del_artista', 20)->nullable();
            $table->string('senati', 20)->nullable();
            $table->string('sistema_nacional_de_pensiones_19990', 20)->nullable();
            $table->string('sistema_privado_de_pensiones', 20)->nullable();
            $table->string('renta_5ta_categoria_retenciones', 20)->nullable();
            $table->string('essalud_seguro_regular_pensionista', 20)->nullable();
            $table->string('contrib_solidaria_asistencia_prev', 20)->nullable();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE concept_airshp COMMENT = 'SUNAT - Planilla Electrónica T22: INGRESOS, TRIBUTOS Y DESCUENTOS'");

        // Assuming the SQL file is in "database/sunat_pe_t22.sql" relative to the migration
        $sqlPath = database_path('sunat_pe_t22.sql');

        // Read the SQL file
        $sql = file_get_contents($sqlPath);

        // Check if file was read successfully
        if ($sql === false) {
            throw new Exception('Unable to open the SQL file for reading.');
        }

        // Run the SQL commands
        DB::unprepared($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sunat_pe_t22');
    }
};
