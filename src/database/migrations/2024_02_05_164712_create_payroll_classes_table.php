<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_classes', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->text('description')->nullable();
            $table->boolean('is_active');
            $table->timestamps();
        });

        // Inserción de datos
        DB::table('payroll_classes')->insert([
            ['code' => '01', 'name' => 'HABERES', 'is_active' => false],
            ['code' => '02', 'name' => 'CAFAE', 'is_active' => false],
            ['code' => '03', 'name' => 'CAS', 'is_active' => true],
            ['code' => '04', 'name' => 'FAG', 'is_active' => false],
            ['code' => '05', 'name' => 'SECIGRA', 'is_active' => false],
            ['code' => '06', 'name' => 'PRACTICAS PRE-PROFESIONALES', 'is_active' => false],
            ['code' => '15', 'name' => 'PROMOTORAS', 'is_active' => false],
            ['code' => '17', 'name' => 'OCASIONALES', 'is_active' => false],
            ['code' => '07', 'name' => 'PENSIONISTAS', 'is_active' => false],
            ['code' => '08', 'name' => 'VIUDEZ', 'is_active' => false],
            ['code' => '09', 'name' => 'ORFANDAD', 'is_active' => false],
            ['code' => '10', 'name' => 'DEPENDENCIA', 'is_active' => false],
            ['code' => '11', 'name' => 'MONTEPIO', 'is_active' => false],
            ['code' => '12', 'name' => 'ALIMENTOS', 'is_active' => false],
            ['code' => '13', 'name' => 'DEVENGADOS', 'is_active' => false],
            ['code' => '16', 'name' => 'LIQUIDACIONES', 'is_active' => false],
            ['code' => '18', 'name' => 'REINTEGROS', 'is_active' => false],
            ['code' => '19', 'name' => 'PROVISIONAL', 'is_active' => false]
        ]);

        DB::statement("ALTER TABLE payroll_types COMMENT = 'Clases de planilla'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_classes');
    }
};
