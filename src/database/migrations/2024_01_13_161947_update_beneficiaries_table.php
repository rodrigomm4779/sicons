<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beneficiaries', function (Blueprint $table) {
            $table->unsignedBigInteger('concept_airshp_id')->nullable()->comment('ID de tipo de concepto AIRSHP');
            $table->unsignedBigInteger('affection_law_id')->nullable()->comment('ID de tipo de concepto AIRSHP');
            $table->char('sunat_pe_t22_codigo', 4)->nullable()->comment('SUNAT Planilla Electrónica Tabla 22');
            $table->enum('modality', ['Permanente', 'Ocacional'])->nullable()->comment('Modalidad de entrega');
            $table->enum('is_taxable', ['SI', 'NO'])->nullable()->comment('Indica SI/NO es Imponible');
            //PARA LOS REGISTROS DE TABLA QUE SON DE SISTEMA (is_system debe ser true para tomar en cuenta)
            $table->boolean('is_system')->default(false)->comment('Para conceptos establecidor por sistema');
            $table->string('affection_law', 50)->nullable(); //ESSALUD, SNP, IES, SENATI , SCTR , CUARTA CAT, OTRA LEY, AFP, EPS , FUTURA LEY
        });

        // Assuming the SQL file is in "database/beneficiaries.sql" relative to the migration
        $sqlPath = database_path('beneficiaries.sql');

        // Read the SQL file
        $sql = file_get_contents($sqlPath);

        // Check if file was read successfully
        if ($sql === false) {
            throw new Exception('Unable to open the SQL file for reading.');
        }

        // Run the SQL commands
        DB::unprepared($sql);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beneficiaries', function (Blueprint $table) {

            $table->dropColumn('concept_airshp_id');
            $table->dropColumn('affection_law_id');
            $table->dropColumn('sunat_pe_t22_codigo');
            $table->dropColumn('modality');
            $table->dropColumn('is_taxable');
            $table->dropColumn('is_system');
            $table->dropColumn('affection_law');

        });
    }
};
