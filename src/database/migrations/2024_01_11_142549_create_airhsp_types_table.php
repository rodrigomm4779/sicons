<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airhsp_types', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->string('description')->nullable(); 
            $table->timestamps();
        });

        DB::table('airhsp_types')->insert([
            ['code' => '1', 'name' => 'Activos', 'description' => 'Descripción para Activos'], 
            ['code' => '2', 'name' => 'Pensionistas', 'description' => 'Descripción para Pensionistas'], 
            ['code' => '3', 'name' => 'Sobrevivientes', 'description' => 'Descripción para Sobrevivientes'], 
            ['code' => '4', 'name' => 'CAS', 'description' => 'Descripción para CAS'] 
        ]);

        DB::statement("ALTER TABLE airhsp_types COMMENT = 'Tipos de Registro AIRHSP'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airhsp_types');
    }
};
