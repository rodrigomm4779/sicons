<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payruns', function (Blueprint $table) {
            $table->string('airhsp_file_name')->nullable();
            $table->string('process_year');
            $table->string('process_month');
            $table->unsignedBigInteger('payroll_type_id');
            $table->unsignedBigInteger('payroll_class_id');
            $table->string('sequence_number')->nullable();
            $table->integer('record_count');
            $table->decimal('total_income_amount', 15, 2);
            $table->decimal('total_deductions_amount', 15, 2);
            $table->decimal('total_contributions_amount', 15, 2);
            $table->unsignedBigInteger('finance_source_id');
            
            // Foreign keys for payroll_type_id and payroll_class_id might be added if they relate to other tables
            // $table->foreign('payroll_type_id')->references('id')->on('payroll_types');
            // $table->foreign('payroll_class_id')->references('id')->on('payroll_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payruns', function (Blueprint $table) {
            $table->dropColumn('airhsp_file_name');
            $table->dropColumn('process_year');
            $table->dropColumn('process_month');
            $table->dropColumn('payroll_type_id');
            $table->dropColumn('payroll_class_id');
            $table->dropColumn('sequence_number');
            $table->dropColumn('record_count');
            $table->dropColumn('total_income_amount');
            $table->dropColumn('total_deductions_amount');
            $table->dropColumn('total_contributions_amount');
            $table->dropColumn('finance_source_id');
        });
    }
};
