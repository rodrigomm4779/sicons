<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->string('code')->nullable();
            $table->string('ruc_number')->nullable();
            // $table->string('first_name')->nullable();
            // $table->string('paternal_last_name')->nullable();
            // $table->string('maternal_last_name')->nullable();
            $table->foreignId('document_type_id')->nullable()->constrained('document_types');
            $table->string('document_number')->nullable();
            $table->foreignId('marital_status_id')->nullable()->constrained('marital_statuses');
            // $table->string('gender')->nullable();
            $table->foreignId('personal_situation_id')->nullable()->constrained('personal_situations');
            // $table->foreignId('nationality_id')->constrained('nationalities');
            $table->foreignId('worker_category_id')->nullable()->constrained('worker_categories');
            $table->foreignId('education_level_id')->nullable()->constrained('education_levels');
            $table->date('entry_date')->nullable();
            $table->boolean('disability')->default(false);
            $table->boolean('unionized')->default(false);
            $table->boolean('resident')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            // Eliminar las columnas agregadas y las relaciones foráneas
            $table->dropForeign(['document_type_id']);
            $table->dropColumn('document_type_id');
            $table->dropForeign(['marital_status_id']);
            $table->dropColumn('marital_status_id');
            $table->dropForeign(['personal_situation_id']);
            $table->dropColumn('personal_situation_id');
            $table->dropForeign(['worker_category_id']);
            $table->dropColumn('worker_category_id');
            $table->dropForeign(['education_level_id']);
            $table->dropColumn('education_level_id');
            $table->dropColumn(['code', 'ruc_number', 'document_number', 'entry_date', 'disability', 'unionized', 'resident']);
        });
    }
};
