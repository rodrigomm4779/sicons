<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_frequencies', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->string('description');
            $table->text('options')->nullable();
            $table->timestamps();
        });

        DB::table('payment_frequencies')->insert([
            ['code' => '01', 'name' => 'MENSUAL', 'description' => 'MENSUAL', 'options' => null],
            ['code' => '02', 'name' => 'QUINCENAL', 'description' => 'QUINCENAL', 'options' => null],
            // ... otros registros
        ]);

        DB::statement("ALTER TABLE payment_frequencies COMMENT = 'Periodicidades de Pago'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_frequencies');
    }
};
