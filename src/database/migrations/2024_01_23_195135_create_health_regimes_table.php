<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_regimes', function (Blueprint $table) {
            $table->id();
            $table->string('code', 4)->unique();
            $table->string('name');
            $table->text('description')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });

        // Insertar los valores
        $healthRegimeValues = [
            ['code' => '00', 'name' => 'ESSALUD REGULAR(EXCLUSIVAMENTE)', 'description' => null],
            ['code' => '01', 'name' => 'ESSALUD REGULAR Y EPS/SERV. PROPIOS', 'description' => null],
            ['code' => '02', 'name' => 'ESSALUD TRABAJADORES PESQUEROS', 'description' => null],
            ['code' => '03', 'name' => 'ESSALUD TRABAJADORES PESQUEROS Y EPS(SERV.PROPIOS)', 'description' => null],
            ['code' => '04', 'name' => 'ESSALUD AGRARIO / ACUICOLA', 'description' => null],
            ['code' => '05', 'name' => 'ESSALUD PENSIONISTAS', 'description' => null],
            ['code' => '20', 'name' => 'SANIDAD DE FFAA Y POLICIALES', 'description' => null],
            ['code' => '21', 'name' => 'SIS – MICROEMPRESA', 'description' => null],
        ];

        DB::table('health_regimes')->insert($healthRegimeValues);

        DB::statement("ALTER TABLE health_regimes COMMENT = 'REGIMEN DE ASEGURAMIENTO DE SALUD'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_regimes');
    }
};
