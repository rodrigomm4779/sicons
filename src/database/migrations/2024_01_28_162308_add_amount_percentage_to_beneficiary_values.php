<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beneficiary_values', function (Blueprint $table) {
            $table->double('amount_percentage')->default(0)->nullable()->after('is_percentage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beneficiary_values', function (Blueprint $table) {
            $table->dropColumn('amount_percentage');
        });
    }
};
