<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_information', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('agency_id')->constrained('agencies')->comment('ID de la agencia');
            $table->foreignId('department_id')->nullable()->constrained('departments')->comment('ID del departamento');
            $table->foreignId('designation_id')->nullable()->constrained('designations')->comment('ID de la designación');
            $table->foreignId('worker_type_id')->constrained('worker_types')->comment('ID del tipo de trabajador');
            $table->foreignId('cost_center_id')->nullable()->constrained('cost_centers')->comment('ID del centro de costos');
            $table->foreignId('pension_regime_id')->nullable()->constrained('pension_regimes')->comment('ID del régimen de pensiones');
            $table->string('cussp')->nullable()->comment('Código Único del Sistema Privado de Pensiones');
            $table->enum('commission', ['flujo', 'mixta'])->nullable()->comment('Tipo de comisión: flujo o mixta');
            $table->foreignId('contract_type_id')->nullable()->constrained('contract_types')->comment('ID del tipo de contrato');
            $table->foreignId('labor_regime_id')->nullable()->constrained('labor_regimes')->comment('ID del régimen laboral');
            $table->foreignId('payment_frequency_id')->nullable()->constrained('payment_frequencies')->comment('ID de la periodicidad de pago');
            $table->unsignedBigInteger('life_insurance_id')->nullable()->comment('ID del seguro de vida ley');
            $table->foreignId('payment_currency_id')->constrained('payment_currencies')->comment('ID de la moneda de pago');
            $table->float('biweekly_percentage')->nullable()->comment('Porcentaje para pagos quincenales');
            $table->float('variable_commission_percentage')->nullable()->comment('Porcentaje de comisiones variables');
            $table->unsignedBigInteger('job_rating_id')->nullable()->comment('ID de la calificación del puesto');
            $table->date('job_rating_date')->nullable()->comment('Fecha de la calificación del puesto');
            // $table->boolean('integral_salary')->nullable()->default(false)->comment('Indica si el salario es integral');
            // $table->boolean('exempt_fifth_category_income')->default(false)->comment('Indica si está exento de rentas de quinta categoría');
            $table->boolean('is_contract')->default(false)->comment('Indica si es un contrato');
            $table->date('contract_start_date')->nullable()->comment('Fecha de inicio del contrato');
            $table->date('contract_end_date')->nullable()->comment('Fecha de fin del contrato');
            $table->foreignId('airhsp_type_id')->nullable()->constrained('airhsp_types')->comment('ID del tipo de registro AIRHSP');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE payroll_information COMMENT = 'Información de Planilla'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_information');
    }
};
