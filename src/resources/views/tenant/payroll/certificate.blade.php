@extends('layout.tenant')

@section('title', __t('payslip'))

@section('contents')
    <app-certificate payrun="{{ request()->get('payrun') }}"></app-certificate>
@endsection

