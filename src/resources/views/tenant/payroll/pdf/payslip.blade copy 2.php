<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ property_exists($settings, 'payment_receipt_title') ? $settings->payment_receipt_title.', ' : '' }}</title>
    <link rel="stylesheet" href="{{ url('css/payslip.css') }}">

<style id="modelo-boleta-pago_6147_Styles">
@page {
  size: A4;
  margin: 4mm;
}

body {
  margin: 0;
  padding: 0;
}

.xl686147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl696147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl706147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl716147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:right;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl726147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl736147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl746147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl756147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl766147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #00CCFF;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl776147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl786147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #00CCFF;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl796147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #00CCFF;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl806147
	{padding:0px;
	mso-ignore:padding;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl816147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl826147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl836147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl846147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl856147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl866147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl876147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl886147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid #969696;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl896147
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid #969696;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl906147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid #969696;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl916147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl926147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl936147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl946147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #00CCFF;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl956147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl966147
	{color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\,\#\#0";
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl976147
	{color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\,\#\#0";
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl986147
	{color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl996147
	{color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl1006147
	{color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl1016147
	{color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl1026147
	{color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"Short Date";
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl1036147
	{color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"Short Date";
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl1046147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #00CCFF;
	border-bottom:none;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1056147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1066147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:1.0pt solid #33CCCC;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid #33CCCC;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1076147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:1.0pt solid #33CCCC;
	border-right:none;
	border-bottom:none;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1086147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:1.0pt solid #33CCCC;
	border-right:1.0pt solid #33CCCC;
	border-bottom:none;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1096147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid #33CCCC;
	border-left:1.0pt solid #33CCCC;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1106147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid #33CCCC;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1116147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #33CCCC;
	border-bottom:1.0pt solid #33CCCC;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1126147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:0000;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid #33CCCC;
	border-right:1.0pt solid #33CCCC;
	border-bottom:none;
	border-left:1.0pt solid #33CCCC;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1136147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:0000;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #33CCCC;
	border-bottom:1.0pt solid #33CCCC;
	border-left:1.0pt solid #33CCCC;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1146147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.5pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1156147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.5pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1166147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #44B3E1;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1176147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #44B3E1;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1186147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #44B3E1;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #44B3E1;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1196147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:right;
	vertical-align:middle;
	border-top:.5pt solid #44B3E1;
	border-right:.5pt solid #44B3E1;
	border-bottom:none;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1206147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #44B3E1;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1216147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #44B3E1;
	border-left:.5pt solid #44B3E1;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1226147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:.5pt solid #44B3E1;
	border-bottom:.5pt solid #44B3E1;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1236147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #44B3E1;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1246147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #00CCFF;
	border-bottom:.5pt solid #44B3E1;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1256147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #44B3E1;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1266147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #44B3E1;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1276147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:right;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #44B3E1;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1286147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #44B3E1;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1296147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #44B3E1;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1306147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #44B3E1;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1316147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #44B3E1;
	border-bottom:.5pt solid #44B3E1;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1326147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid #44B3E1;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1336147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:normal;}
.xl1346147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1356147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #44B3E1;
	border-bottom:none;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1366147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1376147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #44B3E1;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1386147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #44B3E1;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1396147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:none;
	border-bottom:.5pt solid #44B3E1;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1406147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #44B3E1;
	border-left:.5pt solid #44B3E1;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1416147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid #44B3E1;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1426147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #44B3E1;
	border-right:.5pt solid #00CCFF;
	border-bottom:.5pt solid #44B3E1;
	border-left:.5pt solid #00CCFF;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1436147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border:.5pt solid #44B3E1;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1446147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border:.5pt solid #44B3E1;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1456147
	{padding:0px;
	mso-ignore:padding;
	color:white;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:.5pt solid #44B3E1;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1466147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid #44B3E1;
	border-right:.5pt solid #44B3E1;
	border-bottom:.5pt solid #44B3E1;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1476147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid #44B3E1;
	border-right:.5pt solid #00CCFF;
	border-bottom:.5pt solid #44B3E1;
	border-left:.5pt solid #44B3E1;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1486147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #44B3E1;
	border-right:.5pt solid #61CBF3;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1496147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #61CBF3;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1506147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #44B3E1;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1516147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid #44B3E1;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #44B3E1;
	border-left:.5pt solid #44B3E1;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1526147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #44B3E1;
	border-right:.5pt solid #61CBF3;
	border-bottom:none;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1536147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.5pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1546147
	{color:#0066CC;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl1556147{
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:0;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl1566147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #44B3E1;
	border-left:none;
	background:#CCFFFF;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1576147
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1586147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #61CBF3;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1596147
	{color:#0066CC;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:Standard;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #61CBF3;
	border-bottom:none;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}
.xl1606147
	{padding:0px;
	mso-ignore:padding;
	color:#0066CC;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #00CCFF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1616147
	{color:#333399;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"Short Date";
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #00CCFF;
	border-right:.5pt solid #61CBF3;
	border-bottom:.5pt solid #00CCFF;
	border-left:.5pt solid #00CCFF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	padding-left:15px;
	mso-char-indent-count:1;}

</style>
</head>

<body>

<div id="modelo-boleta-pago_6147">

<table border=0 cellpadding=0 cellspacing=0 class=xl686147
 style='border-collapse:collapse;table-layout:fixed;width:100%'>

 <tr height=18 style='height:13.8pt'>
  <td rowspan=3 height=65 width=120 style='height:49.2pt;width:90pt'
  align=left valign=top>
  <span style='position:absolute;z-index:1;margin-left:4px;margin-top:10px;width:116px;height:57px'>
  <img src="{{ property_exists($payslip_settings, 'logo') && $payslip_settings->logo
                        ? asset($payslip_settings->logo)
                        : (property_exists($settings, 'tenant_logo') ? asset($settings->tenant_logo) : asset('images/logo/default-logo.png')) }}"
                 alt="logo"
                 class="img-fluid mb-2"
                 style="max-height: 100px; max-width: 150px;"
            />
	</span>
	<span>
		<table cellpadding=0 cellspacing=0>
		<tr>
			<td rowspan=3 height=65 class=xl1056147 width=120 style='height:49.2pt;
			width:90pt'><a name="RANGE!A1:H26"></a></td>
		</tr>
		</table>
  </span>
</td>
  <td class=xl696147 width=86 style='width: 14%;'></td>
  <td class=xl696147 width=142 style='width: 14%;'></td>
  <td class=xl696147 width=76 style='width: 14%;'></td>
  <td class=xl696147 width=120 style='width: 4%;'></td>
  <td class=xl696147 width=76 style='width: 14%;'></td>
  <td class=xl696147 width=120 style='width: 12%;'></td>
  <td class=xl696147 width=156 style='width: 14%;'></td>
 </tr>
 <tr height=23 style='height:17.4pt'>
  <td height=23 class=xl706147 style='height:17.4pt'></td>
  <td class=xl706147></td>
  <td class=xl706147></td>
  <td colspan=3 rowspan=2 class=xl1066147 style='border-right:1.0pt solid #33CCCC;
  border-bottom:1.0pt solid #33CCCC'>BOLETA DE PAGO C.A.S.   N°</td>
  <td rowspan=2 class=xl1126147 style='border-bottom:1.0pt solid #33CCCC'>  </td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl706147 style='height:18.0pt'></td>
  <td class=xl716147 width=142 style='width:106pt'></td>
  <td class=xl706147></td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl726147 style='height:13.2pt'>RUC: {{ property_exists($settings, 'nro_ruc') ? $settings->nro_ruc : '' }}</td>  
  <td class=xl736147>&nbsp;</td>
  <td class=xl736147>&nbsp;</td>
  <td class=xl736147>&nbsp;</td>
  <td class=xl736147>&nbsp;</td>
  <td class=xl736147>&nbsp;</td>
  <td class=xl736147>&nbsp;</td>
  <td class=xl736147>&nbsp;</td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td colspan=8 height=18 class=xl1146147 style='border-right:.5pt solid #61CBF3; height:13.2pt'>{{ property_exists($settings, 'long_name') ? $settings->long_name : '' }}</td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl746147 style='height:13.2pt;border-top:none'>&nbsp;</td>
  <td class=xl746147 style='border-top:none'>&nbsp;</td>
  <td class=xl746147 style='border-top:none'>&nbsp;</td>
  <td class=xl746147 style='border-top:none'>&nbsp;</td>
  <td class=xl746147 style='border-top:none'>&nbsp;</td>
  <td class=xl746147 style='border-top:none'>&nbsp;</td>
  <td class=xl746147 style='border-top:none'>&nbsp;</td>
  <td class=xl1576147 style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl756147 style='height:13.2pt;border-top:none'>TRABAJADOR</td>
  <td colspan=3 class=xl986147 style='border-left:none'>{{ $payslip->user->first_name }} {{ $payslip->user->last_name }} {{ $payslip->user->middle_name }}</td>
  <td class=xl766147 style='border-top:none'>&nbsp;</td>
  <td class=xl776147 style='border-top:none;border-left:none'>MES</td>
  <td class=xl786147 style='border-top:none'>&nbsp;</td>
  <td class=xl1546147 style='border-top:none;border-left:none'>
  {{ ucfirst(\Carbon\Carbon::parse($payslip->start_date)->locale('es_PE')->isoFormat('MMMM')) }} - 
  {{ \Carbon\Carbon::parse($payslip->start_date)->locale('es_PE')->isoFormat('Y') }}
    </td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl756147 style='height:13.2pt;border-top:none'>CARGO</td>
  <td colspan=3 class=xl986147 style='border-left:none'>{{ $payslip->user->designation->name }}</td>
  <td class=xl796147 style='border-top:none'>&nbsp;</td>
  <td class=xl776147 style='border-top:none;border-left:none'>CTA. CTE Nº</td>
  <td class=xl786147 style='border-top:none'>&nbsp;</td>
  <td class=xl1586147 style='border-left:none;padding-left: 15px;'>
    @if (!empty($payslip->user->bank_details))
        <?php $firstBankDetail = reset($payslip->user->bank_details); ?>
        {{ $firstBankDetail['value']['account_number'] }}
    @else
        {{-- Manejo de caso en el que el array está vacío --}}
    @endif
</td> 
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl756147 style='height:13.2pt;border-top:none'>DNI/CE</td>
  <td colspan=3 class=xl966147 style='border-left:none'>{{ $payslip->user->profile->document_number }}</td>
  <td class=xl796147 style='border-top:none'>&nbsp;</td>
  <td class=xl776147 style='border-top:none;border-left:none'>ONP / A.F.P.</td>
  <td class=xl786147 style='border-top:none'>&nbsp;</td>
  <td class=xl1596147 style='border-left:none'>
    {{ $payslip->user->payrollInformation->pensionRegime->code ?? '' }}
    </td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl756147 style='height:13.2pt;border-top:none'>RUC Nº</td>
  <td colspan=3 class=xl986147 style='border-left:none'>{{ $payslip->user->profile->ruc_number }}</td>
  <td class=xl796147 style='border-top:none'>&nbsp;</td>
  <td class=xl776147 style='border-top:none;border-left:none'>C.U.S.P.P.</td>
  <td class=xl786147 style='border-top:none'>&nbsp;</td>
  <td class=xl1606147>{{ $payslip->user->profile->cussp }}</td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl776147 style='height:13.2pt;border-top:none'>REGIMEN LABORAL</td> 
  <td colspan=3 class=xl1006147>{{ $payslip->user->payrollInformation->laborRegime->name }}</td>
  <td class=xl796147 style='border-top:none'>&nbsp;</td>
  <td class=xl776147 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl786147 style='border-top:none'>&nbsp;</td>
  <td class=xl1616147 style='border-top:none;border-left:none'>&nbsp;</td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl776147 style='height:13.2pt;border-top:none'>FECHA DE INGRESO</td>
  <td colspan=3 class=xl1026147>{{ \Carbon\Carbon::parse($payslip->user->profile->entry_date)->format('d/m/Y') }}</td>
  <td class=xl796147 style='border-top:none'>&nbsp;</td>
  <td class=xl806147 colspan=2 style='border-right:.5pt solid #00CCFF'></td>
  <td class=xl1556147 style='border-top:none;border-left:none'></td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl816147 style='height:13.2pt'></td>
  <td class=xl1376147 style='border-top:none'>&nbsp;</td>
  <td class=xl1376147 style='border-top:none'>&nbsp;</td>
  <td class=xl1376147 style='border-top:none'>&nbsp;</td>
  <td class=xl1386147 style='border-top:none'>&nbsp;</td>
  <td class=xl1386147 style='border-top:none'>&nbsp;</td>
  <td class=xl1396147 style='border-top:none'>&nbsp;</td>
  <td class=xl1386147 style='border-top:none'>&nbsp;</td>
 </tr>

 <tr height=18 style='height:13.2pt'>
  <td colspan=2 rowspan=2 height=36 class=xl1326147 style='border-right:.5pt solid #00CCFF;
  border-bottom:.5pt solid #44B3E1;height:26.4pt'>INGRESOS</td>
  <td colspan=2 rowspan=2 class=xl1336147 width=218 style='border-right:.5pt solid #00CCFF;
  border-bottom:.5pt solid #00CCFF;width:163pt'>DESCUENTOS<br>
    APORTES DEL TRABAJADOR</td>
  <td colspan=2 rowspan=2 class=xl1336147 style='border-right:.5pt solid #44B3E1;
  border-bottom:.5pt solid #00CCFF'>APORTES DEL <br> EMPLEADOR</td>
  <td class=xl1366147>&nbsp;</td>
  <td class=xl1526147 style='border-top:none'>&nbsp;</td>
 </tr>


 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl1406147 style='height:13.2pt;border-left:none'>&nbsp;</td>
  <td class=xl1566147>&nbsp;</td>
 </tr>

  <!-- Iterar sobre los beneficiarios y organizarlos en las columnas correspondientes -->
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl1186147 style='height:13.2pt;border-top:none' colspan="2">
        
        <table style='border-collapse:collapse;table-layout:fixed;width:100%'>
            <tbody>
            @foreach($beneficiaries as $beneficiary)
                @if($beneficiary->beneficiary->type == 'allowance')
                    @if($beneficiary->is_percentage == 1)
                        <tr>
                            <td class="small">{{ $beneficiary->beneficiary->name }} ({{ $beneficiary->amount }}
                                %)
                            </td>
                            <td class="small text-right currency-symbol">
                                {{ trans('default.like_'.$settings->currency_position, [
                                    'symbol' => $settings->currency_symbol,
                                    'amount' => round(($salaryAmount/100) * $beneficiary->amount)
                                        ]) }}
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td class="small">{{ $beneficiary->beneficiary->name }}</td>
                            <td class="small text-right currency-symbol ">
                                {{ trans('default.like_'.$settings->currency_position, [
                                    'symbol' => $settings->currency_symbol,
                                    'amount' =>  $beneficiary->amount
                                        ]) }}
                            </td>
                        </tr>
                    @endif
                @endif
            @endforeach
            </tbody>
        </table>

    </td>

  <td class=xl826147 style='border-top:none' colspan="2">
    <table style='border-collapse:collapse;table-layout:fixed;width:100%'>
        <tbody>
        @foreach($beneficiaries as $beneficiary)
            @if($beneficiary->beneficiary->type == 'deduction')
                @if($beneficiary->is_percentage == 1)
                    <tr>
                        <td class="small">{{ $beneficiary->beneficiary->name }} ({{ $beneficiary->amount }}
                            %)
                        </td>
                        <td class="small text-right currency-symbol">
                            {{ trans('default.like_'.$settings->currency_position, [
                                'symbol' => $settings->currency_symbol,
                                'amount' =>  round(($salaryAmount/100) * $beneficiary->amount, 2)
                                    ]) }}
                        </td>
                    </tr>
                @else
                    @php
                        settings()
                    @endphp
                    <tr>
                        <td class="small">{{ $beneficiary->beneficiary->name }}</td>
                        <td class="small text-right currency-symbol">
                            {{ trans('default.like_'.$settings->currency_position, [
                                'symbol' => $settings->currency_symbol,
                                'amount' =>  $beneficiary->amount
                                    ]) }}
                        </td>
                    </tr>
                @endif
            @endif
        @endforeach
        </tbody>
    </table>


</td>

  <td class=xl816147 colspan="2">

    <table style='border-collapse:collapse;table-layout:fixed;width:100%'>
        <tbody>
        @foreach($beneficiaries as $beneficiary)
            @if($beneficiary->beneficiary->type == 'contribution')
                @if($beneficiary->is_percentage == 1)
                    <tr>
                        <td class="small">{{ $beneficiary->beneficiary->name }} ({{ $beneficiary->amount }}
                            %)
                        </td>
                        <td class="small text-right currency-symbol">
                            {{ trans('default.like_'.$settings->currency_position, [
                                'symbol' => $settings->currency_symbol,
                                'amount' =>  round(($salaryAmount/100) * $beneficiary->amount, 2)
                                    ]) }}
                        </td>
                    </tr>
                @else
                    @php
                        settings()
                    @endphp
                    <tr>
                        <td class="small">{{ $beneficiary->beneficiary->name }}</td>
                        <td class="small text-right currency-symbol">
                            {{ trans('default.like_'.$settings->currency_position, [
                                'symbol' => $settings->currency_symbol,
                                'amount' =>  $beneficiary->amount
                                    ]) }}
                        </td>
                    </tr>
                @endif
            @endif
        @endforeach
        </tbody>
    </table>


  </td>

  <td class=xl846147></td>
  <td class=xl1486147 style='border-top:none'>&nbsp;</td>
 </tr>

 <!-- <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl1176147 style='height:13.2pt'>&nbsp;</td>
  <td class=xl1206147>&nbsp;</td>
  <td class=xl816147>AFP - APORTE 10%</td>
  <td class=xl1266147 align=right>0.00</td>
  <td class=xl816147></td>
  <td class=xl1296147>&nbsp;</td>
  <td class=xl836147></td>
  <td class=xl1496147>&nbsp;</td>
 </tr> -->
 
 <!-- <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl1216147 style='height:13.2pt'>&nbsp;</td>
  <td class=xl1226147>&nbsp;</td>
  <td class=xl696147></td>
  <td class=xl1456147>&nbsp;</td>
  <td class=xl696147></td>
  <td class=xl1316147>&nbsp;</td>
  <td class=xl1166147>&nbsp;</td>
  <td class=xl1506147>&nbsp;</td>
 </tr> -->

 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl1436147 style='height:13.2pt;border-top:none'>TOTAL
  INGRESOS</td>
  <td class=xl1476147 align=right style='border-top:none;border-left:none'>5,100.00</td>
  <td class=xl1426147 style='border-left:none'>TOTAL DESCUENTOS</td>
  <td class=xl1466147 align=right style='border-left:none'>663.00</td>
  <td class=xl1436147 style='border-left:none'>TOTAL APORTES</td>
  <td class=xl1446147 align=right style='border-top:none;border-left:none'>103.95</td>
  <td class=xl1436147 style='border-top:none;border-left:none'>TOTAL NETO S/.</td>
  <td class=xl1516147 align=right style='border-top:none;border-left:none'>4,437.00</td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl1416147 style='height:13.2pt;border-top:none'>&nbsp;</td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl1416147 style='border-top:none'>&nbsp;</td>
  <td class=xl1416147 style='border-top:none'>&nbsp;</td>
  <td class=xl1416147 style='border-top:none'>&nbsp;</td>
  <td class=xl1416147 style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl856147 style='height:13.2pt'></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl866147></td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl856147 style='height:13.2pt'></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
  <td class=xl696147></td>
 </tr>
 <tr height=18 style='height:13.2pt'>
  <td height=18 class=xl876147 style='height:13.2pt'></td>
  <td class=xl886147>&nbsp;</td>
  <td class=xl896147>EMPLEADOR</td>
  <td class=xl906147>&nbsp;</td>
  <td class=xl696147></td>
  <td class=xl916147></td>
  <td class=xl926147></td>
  <td class=xl696147></td>
 </tr>



 <tr height=0 style='display:none'>
  <td width=120 style='width:90pt'></td>
  <td width=86 style='width:64pt'></td>
  <td width=142 style='width:106pt'></td>
  <td width=76 style='width:57pt'></td>
  <td width=120 style='width:90pt'></td>
  <td width=76 style='width:57pt'></td>
  <td width=120 style='width:90pt'></td>
  <td width=156 style='width:117pt'></td>
 </tr>



</table>

</div>


</body>

</html>
