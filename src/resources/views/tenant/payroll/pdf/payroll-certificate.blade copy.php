<!DOCTYPE html>
<html lang="es"><head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{$user->apellidos}} {{$user->nombres}} - {{$name}}</title>
	<link href="{{asset('/fonts/poppins.ttf')}}" rel="stylesheet">
	<style id="const_Styles">
		@page {
            size: A4;
           	/* margin: 8mm; */
        }
		
		body {
        	font-family: 'Courier', sans-serif;
    	}

		.xl1528501 {
			padding: 0px;
			color: black;
			font-size: 9.5pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: top;
			white-space: nowrap;
		}

		.xl6328501 {
			padding: 0px;
			color: black;
			font-size: 10.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: middle;
			white-space: nowrap;
		}

		.xl6328599 {
			padding: 0px;
			color: black;
			font-size: 9.5pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: top;
			white-space: nowrap;
		}

		.xl6428501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: center;
			vertical-align: middle;
			border: .5pt solid #000000;
			white-space: nowrap;
		}

		.xl6528501 {
			padding: 3px;
			color: black;
			font-size: 8.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: middle;
			border: .5pt solid #000000;
			line-height: 1.5;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}

		.xl6628501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 700;
			font-style: normal;
			text-decoration: none;
			text-align: center;
			vertical-align: middle;
			border: .5pt solid #000000;
			white-space: nowrap;
		}

		.xl6728501 {
			padding: 0px;
			color: black;
			font-size: 8.0pt;
			font-weight: 700;
			font-style: normal;
			text-decoration: none;
			text-align: center;
			vertical-align: middle;
			border: .5pt solid #000000;
			white-space: nowrap;
			overflow: hidden; /* Oculta el contenido que sobresale del ancho del elemento */
			text-overflow: ellipsis;
		}

		.xl6828501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: middle;
			border-top: .5pt solid #000000;
			border-right: none;
			border-bottom: none;
			border-left: none;
			white-space: nowrap;
		}

		.xl6928501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: center;
			vertical-align: middle;
			border-top: .5pt solid #000000;
			border-right: .5pt solid #000000;
			border-bottom: none;
			border-left: none;
			white-space: nowrap;
		}

		.xl7028501 {
			padding: 0px;
			color: black;
			font-size: 10.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: center;
			vertical-align: bottom;
			border-top: none;
			border-right: .5pt solid #000000;
			border-bottom: none;
			border-left: none;
			white-space: nowrap;
		}

		.xl7128501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: general;
			vertical-align: middle;
			white-space: nowrap;
		}

		.xl7228501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: general;
			vertical-align: middle;
			border-top: none;
			border-right: none;
			border-bottom: none;
			border-left: .5pt solid #000000;
			white-space: nowrap;
		}

		.xl7328501 {
			padding: 0px;
			color: black;
			font-size: 9.5pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: top;
			border-top: none;
			border-right: none;
			border-bottom: none;
			border-left: .5pt solid #000000;
			white-space: nowrap;
		}

		.xl7428501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: general;
			vertical-align: bottom;
			border-top: .5pt solid #000000;
			border-right: none;
			border-bottom: none;
			border-left: .5pt solid #000000;
			white-space: nowrap;
		}

		.xl7528501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: general;
			vertical-align: bottom;
			border-top: .5pt solid #000000;
			border-right: none;
			border-bottom: none;
			border-left: none;
			white-space: nowrap;
		}

		.xl7628501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: general;
			vertical-align: bottom;
			border-top: .5pt solid #000000;
			border-right: .5pt solid #000000;
			border-bottom: none;
			border-left: none;
			white-space: nowrap;
		}

		.xl7728501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: general;
			vertical-align: bottom;
			border-top: none;
			border-right: none;
			border-bottom: .5pt solid #000000;
			border-left: .5pt solid #000000;
			white-space: nowrap;
		}

		.xl7828501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: general;
			vertical-align: bottom;
			border-top: none;
			border-right: none;
			border-bottom: .5pt solid #000000;
			border-left: none;
			white-space: nowrap;
		}

		.xl7928501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: general;
			vertical-align: bottom;
			border-top: none;
			border-right: .5pt solid #000000;
			border-bottom: .5pt solid #000000;
			border-left: none;
			white-space: nowrap;
		}

		.xl8028501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: middle;
			border-top: .5pt solid #000000;
			border-right: none;
			border-bottom: none;
			border-left: none;
			white-space: nowrap;
		}

		.xl8128501 {
			padding-left: 5px;
			color: black;
			font-size: 9.5pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: top;
			white-space: nowrap;
		}

		.xl8228501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: center;
			vertical-align: bottom;
			white-space: nowrap;
		}

		.xl8328501 {
			padding-left: 4px;
			color: black;
			font-size: 10.0pt;
			font-weight: 700;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: middle;
			border-top: none;
			border-right: none;
			border-bottom: none;
			border-left: .5pt solid #000000;
			white-space: nowrap;
		}

		.xl8428501 {
			color: black;
			font-size: 11.0pt;
			font-weight: 700;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: middle;
			white-space: nowrap;
		}

		.xl8528501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: middle;
			border-top: none;
			border-right: .5pt solid #000000;
			border-bottom: none;
			border-left: none;
			white-space: nowrap;
		}

		.xl8628501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: middle;
			white-space: nowrap;
		}

		.xl8728501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: left;
			vertical-align: middle;
			border-top: none;
			border-right: .5pt solid #000000;
			border-bottom: none;
			border-left: none;
			white-space: nowrap;
		}

		.xl8828501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			text-align: justify;
			vertical-align: bottom;
			white-space: normal;
		}

		.xl8928501 {
			padding: 0px;
			color: black;
			font-size: 11.0pt;
			font-weight: 700;
			font-style: normal;
			text-decoration: none;
			text-align: center;
			vertical-align: middle;
			border: .5pt solid #000000;
			white-space: normal;
		}
	</style>
</head>
<body>
		
	<div id="const" align="center">
		<table border="0" cellpadding="0" cellspacing="0" width="713" style="border-collapse:collapse;table-layout:fixed;width:533pt">
			<colgroup>
				<col width="83" span="2" style="width:62pt">
				<col width="106" style="width:79pt">
				<col width="96" style="width:72pt">
				<col width="87" style="width:65pt">
				<col width="139" style="width:104pt">
				<col width="119" style="width:89pt">
			</colgroup>
			<tbody>
				<tr height="60" style="height:45.0pt">
					<td colspan="7" height="60" width="713" style="height:45.0pt;width:533pt" align="left" valign="top">
						<span style="position:absolute;z-index:1;margin-left:29px;margin-top:0px;width:660px;
							height:54px">
							<img width="660" height="40" src="{{asset('/images/logo-constancia.jpg')}}">
						</span>
						<span>
							<table cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td colspan="7" height="60" class="xl8228501" width="713" style="height:45.0pt;
											width:533pt"></td>
									</tr>
								</tbody>
							</table>
						</span>
					</td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td colspan="7" class="xl6328501" style="font-size: 13.0pt;font-weight: 700;text-align: center;white-space: normal;">
						{{$name}}
					</td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td height="19" class="xl1528501" style="height:14.4pt"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td height="19" class="xl6328501" style="height:14.4pt">APELLIDOS: </td>
					<td colspan="6" class="xl6328501" style="padding-left: 10.0pt;font-weight: 700;">
						{{$user->apellidos}}
					</td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td height="19" class="xl6328501" style="height:14.4pt">NOMBRES: </td>
					<td colspan="6" class="xl6328501" style="padding-left: 10.0pt;font-weight: 700;">
						{{$user->nombres}} 
					</td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td height="19" class="xl1528501" style="height:14.4pt"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
				</tr>
				<tr height="19">
					<td rowspan="2" class="xl8928501" width="83" style="width:62pt">MES</td>
					<td rowspan="2" class="xl8928501" width="83" style="width:62pt">AÑO</td>
					<td colspan="3" class="xl6628501" style="border-left:none;letter-spacing: 3px;">REMUNERACIONES</td>
					<td class="xl6628501" style="border-left:none">DESCUENTOS</td>
					<td class="xl6728501" style="border-left:none" rowspan="2">OBSERVACIONES</td>
				</tr>
				<tr>
					<td class="xl6528501" width="106" style="border-top:none;border-left:none;width:79pt">
						@php
							$letra = 'a';
						@endphp

						@foreach($remuneraciones as $rem)
							<span>{{ strtoupper($letra++) }}) {{ strtoupper($rem->name) }}</span><br> 
						@endforeach
					</td>
					<td class="xl6528501" width="96" style="border-top:none;border-left:none;width:72pt">
						OTRAS <br> REMUNERACIONES</td>
					<td class="xl6528501" width="87" style="border-top:none;border-left:none;width:65pt">
						TOTAL <br> REMUNERACION</td>
					<td class="xl6528501" width="139" style="border-top:none;border-left:none;width:104pt">
						@php
							$letra = 'a';
						@endphp

						@foreach($descuentos as $dscto)
							<span>{{ strtoupper($letra++) }}) {{ strtoupper($dscto->name) }}</span><br> 
						@endforeach
						<!-- a) DL 20530 <br> 
						b) DL 19990 <br> 
						c) DL 25897 <br> 
						d) MONTEPIO  -->
					</td>
					<!-- <td class="xl6428501" style="border-top:none;border-left:none">&nbsp;</td> -->
				</tr>
				<tr height="19" style="height:14.4pt">
					<td height="19" class="xl7228501" style="height:14.4pt">&nbsp;</td>
					<td class="xl7128501"></td>
					<td class="xl6828501" style="border-top:none">&nbsp;</td>
					<td class="xl6828501" style="border-top:none">&nbsp;</td>
					<td class="xl6828501" style="border-top:none">&nbsp;</td>
					<td class="xl6828501" style="border-top:none">&nbsp;</td>
					<td class="xl6928501" style="border-top:none">&nbsp;</td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td colspan="2" height="19" class="xl8328501" style="height:14.4pt">CENTRO DE TRABAJO </td>
					<td colspan="5" class="xl6328501" style="border-right:.5pt solid black">
						{{$user->establecimiento}}
					</td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td colspan="2" height="19" class="xl8328501" style="height:14.4pt">CARGO</td>
					<td colspan="5" class="xl6328501" style="border-right:.5pt solid black">
						{{$user->cargo}}
					</td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td colspan="2" height="19" class="xl8328501" style="height:14.4pt">NIV. MAG.</td>
					<td colspan="5" class="xl6328501" style="border-right:.5pt solid black">
						<!-- 4 - 28 -->
					</td>
				</tr>
				<tr height="19" style="height:14.4pt;border-bottom: solid 1px black;">
					<td height="19" class="xl7328501" style="height:14.4pt">&nbsp;</td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl7028501">&nbsp;</td>
				</tr>

				<tr height="19" style="height:14.4pt">
					<td height="19" class="xl7328501" style="height:14.4pt">&nbsp;</td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl6328599"></td>
					<td class="xl6328599"></td>
					<td class="xl6328599"></td>
					<td class="xl7028501">&nbsp;</td>
				</tr>

				@for ($i = 0; $i < 50; $i++)
				@foreach($payslips as $payslip)
				<tr height="19" style="height:14.4pt">
					<td height="19" class="xl7328501" style="height:14.4pt; padding-left:3pt;">{{ $payslip->mes }}</td> 
					<td class="xl1528501" align="right">{{ $payslip->anio }}</td> 
					<!-- Basica o Contrato -->
					<td class="xl1528501">
						@foreach($payslip->allowances_filtered as $allowance)
							<p style="margin: 0;">
							{{ $headRemuneraciones[$allowance->beneficiary_id] }} {{ number_format($allowance->amount, 2) }}
							</p>
						@endforeach
					</td>
					<!-- Otras remuneraciones Se resta total bruto - a) contrato-->
					<td class="xl8128501">
						{{ number_format($payslip->otras_remunerac, 2) }}
					</td> 
					<!-- Total de remuneracion -->
					<td class="xl8128501">
						{{ number_format($payslip->total_bruto, 2) }}
					</td> 
					<!-- Descuentos seleccionados -->
					<td class="xl6328599">
						@foreach($payslip->deductions_filtered as $deduction)
							<p style="margin: 0;">
							{{ $headDescuentos[$deduction->beneficiary_id] }} {{ number_format($deduction->amount, 2) }}
							</p>
						@endforeach
					</td> 
					<!-- Obs. -->
					<td class="xl7028501">&nbsp;</td>
				</tr>
				@endforeach
				@endfor

				<!-- <tr height="19" style="height:14.4pt">
					<td height="19" class="xl7328501" style="height:14.4pt">ENERO</td>
					<td class="xl1528501" align="right">2018</td>
					<td class="xl1528501">a) 50.11</td>
					<td class="xl8128501">1,308.41</td>
					<td class="xl8128501">1,358.41</td>
					<td class="xl6328599">a) 221.15</td>
					<td class="xl7028501">&nbsp;</td>
				</tr> -->

				<tr height="19" style="height:14.4pt">
					<td height="19" class="xl7728501" style="height:14.4pt">&nbsp;</td>
					<td class="xl7828501">&nbsp;</td>
					<td class="xl7828501">&nbsp;</td>
					<td class="xl7828501">&nbsp;</td>
					<td class="xl7828501">&nbsp;</td>
					<td class="xl7828501">&nbsp;</td>
					<td class="xl7928501">&nbsp;</td>
				</tr>
				<tr height="61">
					<td colspan="7" height="61" class="xl8828501" width="713" style="height:45.6pt;
						width:533pt">Se expide la presente constancia, según talones de pago que adjunta y planillas unicas de pagos que existe en archivo de Tesorería Administración de la DREP, a petición de el/la interesado/a para fines que estime por conveniente 
						@if($ref_exp_number)
							<span>, en atención al expediente Nro. {{$ref_exp_number}}</span>
						@endif
						. 
					</td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td height="19" class="xl1528501" style="height:14.4pt"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
					<td class="xl1528501"></td>
				</tr>
				<tr height="19" style="height:14.4pt">
					<td colspan="7" height="19" class="xl8228501" style="height:14.4pt">
						Puno, {{$issue_date_formated}}
					</td>
				</tr>
			</tbody>
		</table>
	</div>

</body></html>