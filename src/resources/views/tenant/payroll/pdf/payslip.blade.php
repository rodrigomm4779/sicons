<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport"
			content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>{{ property_exists($settings, 'payment_receipt_title') ? $settings->payment_receipt_title.', ' : '' }}</title>
		<!-- <link rel="stylesheet" href="{{ url('css/payslip.css') }}"> -->
		<style>
            @page {
				size: A4;
				margin: 4mm;
            }

			/* Font Definitions */
			body {
				font-family: 'Courier', sans-serif;
			}
			/* Style Definitions */
			p.MsoNormal, li.MsoNormal, div.MsoNormal
			{
				margin:0cm;
				font-size:11.0pt;
			}
			p.MsoTitle, li.MsoTitle, div.MsoTitle
			{
			margin-top:6.35pt;
			margin-right:44.45pt;
			margin-bottom:0cm;
			margin-left:8.45pt;
			text-align:center;
			font-size:14.0pt;
			
			font-weight:bold;}
			p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
			{
			margin:0cm;
			font-size:7.0pt;
			}
			p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
			{
			margin:0cm;
			font-size:11.0pt;
			}
			p.TableParagraph, li.TableParagraph, div.TableParagraph
			{
			margin-top:1.1pt;
			margin-right:0cm;
			margin-bottom:0cm;
			margin-left:3.0pt;
			line-height:6.5pt;
			font-size:11.0pt;
			}
			@page WordSection1
			{
        size:595.0pt 842.0pt;
			margin:14.0pt 11.0pt 14.0pt 11.0pt;
    }
			div.WordSection1
			{page:WordSection1;}
		</style>
		<style>
			/* Style Definitions */
			table.MsoNormalTable
			{
			font-size:11.0pt;
			}
			table.MsoTableGrid
			{
			border:solid windowtext 1.0pt;
			font-size:11.0pt;
			}
			table.TableNormal
			{
			font-size:11.0pt;
			}
		</style>
	</head>
	<body style='word-wrap:break-word'>
		<div class=WordSection1>
			<p class=MsoBodyText style='margin-top:.45pt'>
				<span  style='font-size:
					2.0pt;'>
					
				</span>
			</p>
			<p class=MsoBodyText style='margin-top:.45pt'>
				<span  style='font-size:
					2.0pt;'>
					
				</span>
			</p>
			<p class=MsoBodyText style='margin-top:.45pt'>
				<span  style='font-size:
					2.0pt;'>
					
				</span>
			</p>
			<table class=TableNormal border=0 cellspacing=0 cellpadding=0 style='margin-left:6.5pt;border-collapse:collapse; width: 202mm;'>
				<tr style='height:18.95pt'>
					<td width=237 style='width:178.05pt;padding:0cm 0cm 0cm 0cm;height:18.95pt'>
						<p class=MsoBodyText style='margin-left:35.45pt'>
							<span style='font-size:10.0pt;'>
            <img src="{{ property_exists($payslip_settings, 'logo') && $payslip_settings->logo != ''
                        ? asset($payslip_settings->logo)
                        : (property_exists($settings, 'tenant_logo') ? asset($settings->tenant_logo) : asset('images/logo/default-logo.png')) }}"
                 alt="logo"
                 class="img-fluid mb-2"
                 style="max-height: 100px; max-width: 150px;"
            />
							</span>
							<span style='font-size:10.0pt;'>
							</span>
						</p>
                        <!-- Nombre entidad -->
						<p class=MsoBodyText align=center style='margin-top:4.75pt;margin-right:1.9pt;
							margin-bottom:0cm;margin-left:12.8pt;margin-bottom:.0001pt;text-align:center;
							line-height:8.05pt;'>
							<span>{{ property_exists($settings, 'short_name') ? $settings->short_name : '' }}</span>
						</p>
                        <!-- Dirección -->
						<p class=MsoBodyText align=center style='margin-top:0cm;margin-right:1.9pt;margin-bottom:0cm;margin-left:12.9pt;margin-bottom:.0001pt;text-align:center'>
                            <span>{{ property_exists($settings, 'address') ? $settings->address : '' }}</span>
                            
                        </p>

                        <!-- Departamento -->
						<p class=MsoBodyText align=center style='margin-top:0cm;margin-right:1.9pt;margin-bottom:0cm;margin-left:12.9pt;margin-bottom:.0001pt;text-align:center'>
                            <span>{{ property_exists($settings, 'state') ? $settings->state : '' }} - {{ property_exists($settings, 'city') ? $settings->city : '' }}</span>
                            
                        </p>
					</td>
					<td width=511 style='width:382.95pt;padding:0cm 0cm 0cm 0cm;height:18.95pt'>
						<p class=MsoNormal align=center style='margin-top:3.7pt;margin-right:44.45pt;
							margin-bottom:0cm;margin-left:12.45pt;margin-bottom:.0001pt;text-align:center'>
							<b>
								<span  style='font-size:10.0pt;'>
                                {{ property_exists($settings, 'long_name') ? $settings->long_name : '' }}
								</span>
							</b>
						</p>
						<p class=MsoNormal align=center style='margin-top:6.95pt;margin-right:44.45pt;
							margin-bottom:0cm;margin-left:12.45pt;margin-bottom:.0001pt;text-align:center'>
							<span style='font-size:14.0pt;'> R.U.C.: 
                                <span style='letter-spacing:-.1pt'>{{ property_exists($settings, 'nro_ruc') ? $settings->nro_ruc : '' }}</span>
							</span>
						</p>
                        <!-- Título de boleta de pago -->
						<p class=MsoTitle>
							<span >
                            {{ property_exists($settings, 'payment_receipt_title') ? $settings->payment_receipt_title : '' }}
							</span>
						</p>
                        <!-- Periodo TODO si una boleta no corresponde a un mes completo -->
						<p class=MsoNormal style='margin-top:4.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:129.65pt;margin-bottom:.0001pt'>
							<b>
								<span >
									{{$payslip->mes_periodo}}
                                <!-- {{ ucfirst(\Carbon\Carbon::parse($payslip->start_date)->locale('es_PE')->isoFormat('MMMM')) }} -  -->
                                <!-- {{ \Carbon\Carbon::parse($payslip->start_date)->locale('es_PE')->isoFormat('Y') }} -->
								</span>
							</b>
						</p>
						<!-- <p class=MsoNormal align=right style='margin-top:4.45pt;margin-right:0cm;
							margin-bottom:0cm;margin-left:129.65pt;margin-bottom:.0001pt;text-align:right'>
							<b>
                                <span  style='font-size:8.0pt;'>Fecha de Pago:</span>
                            </b>
                            <span  style='font-size:8.0pt;letter-spacing:-.1pt'>
                                {{ $payslip->created_at->format('d/m/Y') }}
                            </span>
						</p> -->
					</td>
				</tr>
			</table>

			<p class=MsoBodyText style='margin-top:1mm'>
				<span  style='font-size:2.0pt;'></span>
			</p>

			<table class=TableNormal border=1 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none;width: 202mm;'>
				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;'>
								Apellidos y Nombres
							</span>
						</p>
					</td>
					<td width=508 colspan=3 valign=top style='width:381.0pt;border:solid windowtext 1.0pt;
						border-left:none;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
							<span  style='font-size:7.5pt;text-transform: uppercase;'>
                                {{ $payslip->user->last_name }} {{ $payslip->user->middle_name }} {{ $payslip->user->first_name }}
							</span>
						</p>
					</td>
				</tr>
				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;'>
								Documento de Identidad
							</span>
						</p>
					</td>
					<td width=267 valign=top style='width:200.0pt;border-top:none;border-left:
						none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
							<span  style='font-size:7.5pt;letter-spacing:-.1pt'>({{ $payslip->tipo_doc }}) {{$payslip->nro_doc}}</span>
						</p>
					</td>
					<td width=241 colspan=2 valign=top style='width:181.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.95pt;line-height:9.05pt;'>
							<span style='font-size: 9.0pt;'>Fecha de Nacimiento:
								<span style='margin-left: 0.5mm;'>
									@if (!is_null($payslip->user->profile->date_of_birth))
										{{ \Carbon\Carbon::parse($payslip->user->profile->date_of_birth)->format('d/m/Y') }}
									@endif
								</span>
							</span>
						</p>
					</td>
				</tr>

				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;letter-spacing:-.1pt'>Establecimiento</span>
						</p>
					</td>
					<td width=508 colspan=3 valign=top style='width:381.0pt;border-top:none;
						border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
							<span  style='font-size:7.5pt;'>
								{{$payslip->establecimiento}}
							</span>
						</p>
					</td>
				</tr>
				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;letter-spacing:-.1pt'>Cargo</span>
						</p>
					</td>
					<td width=508 colspan=3 valign=top style='width:381.0pt;border-top:none;
						border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
                            <!-- //TODO codigo - cargo -->
							<span  style='font-size:7.5pt;'>
                                {{ $payslip->cargo ?? '' }}
							</span>
						</p>
					</td>
				</tr>
				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;letter-spacing:-.1pt'>Régimen laboral</span>
						</p>
					</td>
					<td width=508 colspan=3 valign=top style='width:381.0pt;border-top:none;
						border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
                            <!-- //TODO codigo - cargo -->
							<span  style='font-size:7.5pt;'>
                                {{ $payslip->regimen_laboral }}
							</span>
						</p>
					</td>
				</tr>
				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;'>
								Grupo Ocupacional
							</span>
						</p>
					</td>
					<td width=267 valign=top style='width:200.0pt;border-top:none;border-left:
						none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
							<span  style='font-size:7.5pt;letter-spacing:-.1pt'>{{ $payslip->grupo_ocupacional }}</span>
						</p>
					</td>
					<td width=241 colspan=2 valign=top style='width:181.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.95pt;line-height:9.05pt;'>
							<span  style='font-size:9.0pt;'>Jornada Laboral:<span style='margin-left:0.5mm;'>{{ $payslip->jornada_laboral }}</span></span>
						</p>
					</td>
				</tr>
				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;'>
								RUC
							</span>
						</p>
					</td>
					<td width=267 valign=top style='width:200.0pt;border-top:none;border-left:
						none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
							<span  style='font-size:7.5pt;letter-spacing:-.1pt'>{{ $payslip->num_ruc }}</span>
						</p>
					</td>
					<td width=241 colspan=2 valign=top style='width:181.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.95pt;line-height:9.05pt;'>
							<span  style='font-size:9.0pt;'>CTA:<span style='margin-left:0.5mm;'>{{ $payslip->num_cta }}</span></span>
						</p>
					</td>
				</tr>
				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;letter-spacing:-.1pt'>Contrato/Vigencia</span>
						</p>
					</td>
					<td width=508 colspan=3 valign=top style='width:381.0pt;border-top:none;
						border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
							<span style='font-size: 7.5pt;'>
								Inicio: {{ $payslip->inicio_contrato ? \Carbon\Carbon::parse($payslip->inicio_contrato)->format('d/m/Y') : ' / / ' }} &nbsp;&nbsp;&nbsp;&nbsp;
								Fin: {{ $payslip->fin_contrato ? \Carbon\Carbon::parse($payslip->fin_contrato)->format('d/m/Y') : ' / / ' }}
							</span>
						</p>
					</td>
				</tr>
                <tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;'>
							Regimen Pensionario
							</span>
						</p>
					</td>
					@if(optional($payslip->user->payrollInformation)->cussp)
					<td width=267 valign=top style='width:200.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
					@else
					<td width=267 colspan=3 valign=top style='width:200.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
					@endif
					
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
							<span  style='font-size:7.5pt;letter-spacing:-.1pt'>
                            {{ $payslip->regimen_pension }}
							@if(!is_null($payslip->tipo_regimen))
								({{$payslip->tipo_regimen}})
							@endif
                            </span>
						</p>
					</td>
					@if(optional($payslip->user->payrollInformation)->cussp)
					<td width=241 colspan=2 valign=top style='width:181.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.95pt;line-height:9.05pt;'>
							<span  style='font-size:9.0pt;'>C.U.S.P.P.: 
								<span style='margin-left:0.5mm;'>{{ $payslip->user->payrollInformation->cussp ?? '' }}</span>
							</span>
						</p>
					</td>
					@endif
				</tr>
				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;letter-spacing:-.1pt'>Días Laborados</span>
						</p>
					</td>
					<td width=508 colspan=3 valign=top style='width:381.0pt;border-top:none;
						border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:2.3pt;line-height:7.7pt;'>
							<span  style='font-size:7.5pt;'>
                                {{ $payslip->dias_lab }}
							</span>
						</p>
					</td>
				</tr>

				<tr style='height:11.0pt'>
					<td width=240 colspan=2 valign=top style='width:180.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:.45pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:9.55pt;'>
							<span  style='font-size:9.0pt;'>
								Fecha de <span style='letter-spacing:-.1pt'>Ingreso/Reingreso</span>
							</span>
						</p>
					</td>
					<td width=508 colspan=3 valign=top style='width:381.0pt;border-top:none;
						border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph style='margin-top:1.8pt;line-height:8.2pt;'>
							<span  style='font-size:7.5pt;letter-spacing:-.1pt'>
                            {{ \Carbon\Carbon::parse($payslip->user->profile->entry_date)->format('d/m/Y') }}
                            </span>
						</p>
					</td>
				</tr>

                <!-- //TODO -->
                @if($payslip->consider_type != 'none')
                    @if($payslip->net_salary - ($totalAllowance - $totalContribution) > $salaryAmount)
                        <div class="custom-row basic-salary-info border-bottom mt-3">
                            <div class="column">
                                <p class="mb-1">{{ __t('overtime_earning') }}</p>
                            </div>
                            <div class="column text-right">
                                <p class="mb-1 currency-symbol">
                                    {{ trans('default.like_'.$settings->currency_position, [
                                        'symbol' => $settings->currency_symbol,
                                        'amount' => round(($payslip->net_salary - ($totalAllowance - $totalContribution)) - $salaryAmount, 2)
                                        ]) }}
                                </p>
                            </div>
                        </div>
                    @endif
                    @if($payslip->net_salary - ($totalAllowance - $totalContribution) < $salaryAmount)
                        <div class="custom-row basic-salary-info border-bottom mt-3">
                            <div class="column">
                                <p class="mb-1">{{ __t('unpaid_leave_deduction') }}</p>
                            </div>
                            <div class="column text-right">
                                <p class="mb-1 currency-symbol">
                                    {{ trans('default.like_'.$settings->currency_position, [
                                        'symbol' => $settings->currency_symbol,
                                        'amount' => round($salaryAmount - ($payslip->net_salary - ($totalAllowance - $totalContribution)), 2)
                                        ]) }}
                                </p>
                            </div>
                        </div>
                    @endif
                    <div class="custom-row basic-salary-info border-bottom mt-3">
                        <div class="column">
                            <p class="mb-1">{{ __t('total_earning') }}
                                <small>({{__t('based_on') .' '. __t($payslip->consider_type)}}{{
                                        ($payslip->net_salary - ($totalAllowance - $totalContribution)) >= $salaryAmount
                                        ? ', '.($payslip->consider_overtime ? __t('included_overtime') : __t('excluded_overtime'))
                                        : ''}})
                                </small>
                            </p>
                        </div>
                        <div class="column text-right">
                            <p class="mb-1 currency-symbol">
                                {{ trans('default.like_'.$settings->currency_position, [
                                    'symbol' => $settings->currency_symbol,
                                    'amount' => round($payslip->net_salary - ($totalAllowance - $totalContribution), 2)
                                    ]) }}
                            </p>
                        </div>
                    </div>
                @endif
                <!-- //ENDTODO -->

                <!-- //Haberes y descuentos -->

                @if(count($beneficiaries) != 0)
				<tr style='height:11.0pt'>
					<td width=748 colspan=5 valign=top style='width:561.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						background:#D1CCCC;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph align=center style='margin-top:2.0pt;margin-right:
							210.3pt;margin-bottom:0cm;margin-left:211.25pt;margin-bottom:.0001pt;
							text-align:center;line-height:7.95pt;'>
							<span
								 style='font-size:8.0pt;color:black;
								letter-spacing:-.1pt'>INGRESOS</span>
							<span 
								style='font-size:8.0pt;'>
								
							</span>
						</p>
					</td>
				</tr>
				<tr style='height:9.0pt'>
					<td width=133 valign=top style='width:100.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph align=center style='margin-top:.9pt;margin-right:
							35.95pt;margin-bottom:0cm;margin-left:36.9pt;margin-bottom:.0001pt;
							text-align:center;line-height:7.1pt;'>
							<span style='font-size:7.5pt;letter-spacing:-.1pt;'> </span>
						</p>
					</td>
					<td width=440 colspan=3 valign=top style='width:330.0pt;border-top:none;
						border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph style='margin-top:.9pt;line-height:7.1pt;'>
							<span  style='font-size:7.5pt;letter-spacing:-.1pt;'>Descripción</span>
						</p>
					</td>
					<td width=175 valign=top style='width:131.0pt;border-top:none;border-left:
						none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph style='margin-top:.4pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:49.85pt;margin-bottom:.0001pt;line-height:7.6pt;'>
							<span  style='font-size:7.5pt;'>Monto<span style='letter-spacing:-.3pt'> </span><span
								style='letter-spacing:-.25pt'>{{$settings->currency_symbol}}</span></span>
							<span  style='font-size:
								7.5pt;'>
								
							</span>
						</p>
					</td>
				</tr>
                <!-- //Haberes -->
                @foreach($beneficiaries as $beneficiary)
                    @if($beneficiary->beneficiary->type == 'allowance')                    
                        

                        @if($beneficiary->is_percentage == 1)
                        <tr style='height:9.0pt'>
                            <td width=133 valign=top style='width:100.0pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=center style='margin-top:1.1pt;margin-right:40.75pt;margin-bottom:0cm;margin-left:41.7pt;margin-bottom:.0001pt;text-align:center;line-height:6.9pt;'>
                                    <span style='font-size:7.0pt;letter-spacing:-.25pt'>+</span>
                                </p>
                            </td>
                            <td width=440 colspan=3 valign=top style='width:330.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph style='margin-top:1.35pt;line-height:6.65pt;'>
                                    <span  style='font-size:6.5pt;'>
                                        {{ $beneficiary->beneficiary->name }} (%)
                                        <!-- {{ $beneficiary->beneficiary->name }} ({{ $beneficiary->amount }} %) -->
                                    </span>
                                </p>
                            </td>
                            <td width=175 valign=top style='width:131.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:-1.45pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right'>
                                    <span  style='font-size:6.5pt;letter-spacing:-.1pt'>
                                        {{ trans('default.like_'.$settings->currency_position, [
                                        'symbol' => $settings->currency_symbol,
                                        'amount' => $beneficiary->amount
                                            ]) }}
                                    </span>
                                </p>
                            </td>
                        </tr>
                        @else

                        <tr style='height:9.0pt'>
                            <td width=133 valign=top style='width:100.0pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=center style='margin-top:1.1pt;margin-right:40.75pt;margin-bottom:0cm;margin-left:41.7pt;margin-bottom:.0001pt;text-align:center;line-height:6.9pt;'>
                                    <span style='font-size:7.0pt;letter-spacing:-.25pt'> + </span>
                                </p>
                            </td>
                            <td width=440 colspan=3 valign=top style='width:330.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph style='margin-top:1.35pt;line-height:6.65pt;'>
                                    <span  style='font-size:6.5pt;'>
                                        {{ $beneficiary->beneficiary->name }}
                                    </span>
                                </p>
                            </td>
                            <td width=175 valign=top style='width:131.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:-1.45pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right'>
                                    <span  style='font-size:6.5pt;letter-spacing:-.1pt'>
                                        {{ trans('default.like_'.$settings->currency_position, [
                                        'symbol' => $settings->currency_symbol,
                                        'amount' =>  $beneficiary->amount
                                            ]) }}
                                    </span>
                                </p>
                            </td>
                        </tr>
                        @endif
                    @endif
                @endforeach
                <!-- Totales haberes -->
                <tr style='height:9.0pt'>
					<td width=573 colspan=4 valign=top style='width:430.0pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph style='line-height:6.9pt;'>
							<span style='font-size:7.0pt;letter-spacing:-.1pt'>TOTAL:</span>
						</p>
					</td>
					<td width=175 valign=top style='width:131.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:-1.45pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right'>
							<b><span  style='font-size:6.5pt;'>
                            {{ trans('default.like_'.$settings->currency_position, [
                                'symbol' => $settings->currency_symbol,
                                'amount' =>  $payslip->total_bruto
                                    ]) }}
                            </span></b>
						</p>
					</td>
				</tr>
                <!-- Totales haberes -->
                
				<tr style='height:11.0pt'>
					<td width=748 colspan=5 valign=top style='width:561.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						background:#D1CCCC;padding:0cm 0cm 0cm 0cm;height:11.0pt'>
						<p class=TableParagraph align=center style='margin-top:2.0pt;margin-right:
							210.3pt;margin-bottom:0cm;margin-left:211.25pt;margin-bottom:.0001pt;
							text-align:center;line-height:7.95pt;'>
							<span
								 style='font-size:8.0pt;color:black;'>DESCUENTOS</span>
							<span
								 style='font-size:8.0pt;'>
								
							</span>
						</p>
					</td>
				</tr>
				<tr style='height:9.0pt'>
					<td width=133 valign=top style='width:100.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph align=center style='margin-top:.9pt;margin-right:
							35.95pt;margin-bottom:0cm;margin-left:36.9pt;margin-bottom:.0001pt;
							text-align:center;line-height:7.1pt;'>
							<span
								 style='font-size:7.5pt;letter-spacing:-.1pt;'></span>
							<span  style='font-size:7.5pt;
								'>
								
							</span>
						</p>
					</td>
					<td width=440 colspan=3 valign=top style='width:330.0pt;border-top:none;
						border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph style='margin-top:.9pt;line-height:7.1pt;'>
							<span  style='font-size:7.5pt;
								letter-spacing:-.1pt;'>Descripción</span>
							<span 
								style='font-size:7.5pt;'>
								
							</span>
						</p>
					</td>
					<td width=175 valign=top style='width:131.0pt;border-top:none;border-left:
						none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph style='margin-top:.4pt;margin-right:0cm;margin-bottom:
							0cm;margin-left:49.85pt;margin-bottom:.0001pt;line-height:7.6pt;'>
							<span  style='font-size:7.5pt;'>Monto<span style='letter-spacing:-.3pt'> </span><span
								style='letter-spacing:-.25pt'>S/</span></span>
							<span  style='font-size:
								7.5pt;'>
								
							</span>
						</p>
					</td>
				</tr>
                <!-- //Descuentos -->
                @foreach($beneficiaries as $beneficiary)
                    @if($beneficiary->beneficiary->type == 'deduction')
                        @if($beneficiary->is_percentage == 1)

                        <tr style='height:9.0pt'>
                            <td width=133 valign=top style='width:100.0pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=center style='margin-top:1.1pt;margin-right:40.75pt;margin-bottom:0cm;margin-left:41.7pt;margin-bottom:.0001pt;text-align:center;line-height:6.9pt;'>
                                    <span style='font-size:7.0pt;letter-spacing:-.25pt'> - </span>
                                </p>
                            </td>
                            <td width=440 colspan=3 valign=top style='width:330.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph style='margin-top:1.35pt;line-height:6.65pt;'>
                                    <span  style='font-size:6.5pt;'>
                                    <!-- {{ $beneficiary->beneficiary->name }} ({{ $beneficiary->amount }}%) -->
                                    {{ $beneficiary->beneficiary->name }} (%)
                                    </span>
                                </p>
                            </td>
                            <td width=175 valign=top style='width:131.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:-1.45pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right'>
                                    <span  style='font-size:6.5pt;letter-spacing:-.1pt'>
                                        {{ trans('default.like_'.$settings->currency_position, [
                                        'symbol' => $settings->currency_symbol,
                                        'amount' =>  $beneficiary->amount
                                            ]) }}
                                    </span>
                                </p>
                            </td>
                        </tr>
                        @else
                            @php
                                settings()
                            @endphp

                        <tr style='height:9.0pt'>
                            <td width=133 valign=top style='width:100.0pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=center style='margin-top:1.1pt;margin-right:40.75pt;margin-bottom:0cm;margin-left:41.7pt;margin-bottom:.0001pt;text-align:center;line-height:6.9pt;'>
                                    <span style='font-size:7.0pt;letter-spacing:-.25pt'> - </span>
                                </p>
                            </td>
                            <td width=440 colspan=3 valign=top style='width:330.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph style='margin-top:1.35pt;line-height:6.65pt;'>
                                    <span  style='font-size:6.5pt;'>
                                        {{ $beneficiary->beneficiary->name }}
                                    </span>
                                </p>
                            </td>
                            <td width=175 valign=top style='width:131.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:-1.45pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right'>
                                    <span  style='font-size:6.5pt;letter-spacing:-.1pt'>
                                        {{ trans('default.like_'.$settings->currency_position, [
                                        'symbol' => $settings->currency_symbol,
                                        'amount' =>  $beneficiary->amount
                                            ]) }}
                                    </span>
                                </p>
                            </td>
                        </tr>
                        @endif
                    @endif
                @endforeach

				<tr style='height:9.0pt'>
					<td width=573 colspan=4 valign=top style='width:430.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph style='line-height:6.9pt;'>
							<span
								 style='font-size:7.0pt;letter-spacing:-.1pt'>TOTAL:</span>
							<span
								 style='font-size:7.0pt;'>
								
							</span>
						</p>
					</td>
					<td width=175 valign=top style='width:131.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:-1.4pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right'>
							<b><span  style='font-size:6.5pt;'>
                            {{ trans('default.like_'.$settings->currency_position, [
                                'symbol' => $settings->currency_symbol,
                                'amount' =>   $payslip->total_dscto
                                    ]) }}

							<!-- 'amount' =>   number_format(round($totalContribution, 2), 2, '.', '') -->
                            </span></b>
						</p>
					</td>
				</tr>
                @endif

			</table>
			<p class=MsoBodyText style='margin-top:.45pt'>
				<span  style='font-size:4.5pt;'>
					
				</span>
			</p>
			<p class=MsoNormal align=center style='margin-top:5.0pt;margin-right:0cm;
				margin-bottom:0cm;margin-left:109.0pt;margin-bottom:.0001pt;text-align:center'>
				<b>
					<span  style='font-size:7.5pt;'>
						
					</span>
				</b>
			</p>
            <!-- //Neto a pagar -->
            <!-- //Neto a pagar -->
            <!-- //Neto a pagar -->
			<table class=TableNormal border=1 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none;width: 202mm;'>
				<tr style='height:18.95pt'>
					<td width=573 style='width:430.0pt;border:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:18.95pt'>
						<p class=TableParagraph align=right style='text-align:right;line-height:6.9pt;
							'>
							<span class=GramE><b><span style='font-size:9.0pt;letter-spacing:-.1pt'>
                            NETO A PAGAR</span></b></span>
                            <span  style='font-size:7.0pt;letter-spacing:-.1pt'>:</span>
						</p>
					</td>
					<td width=175 style='width:131.0pt;border:solid windowtext 1.0pt;border-left:
						none;
						padding:0cm 0cm 0cm 0cm;height:18.95pt'>
						<p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:
							-1.45pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:
							right'>
							<b><span  style='font-size:9.0pt;letter-spacing:-.1pt'>
                                {{ trans('default.like_'.$settings->currency_position, [
                            'symbol' => $settings->currency_symbol,
                            'amount' =>  $payslip->total_liquido
                                ]) }}
                            </span></b>
						</p>
					</td>
				</tr>
			</table>
			<p class=MsoNormal align=center style='margin-top:5.0pt;margin-bottom:0cm;text-align:center;width: 202mm;'>
				<b>
					<span  style='font-size:7.5pt;'>
						
					</span>
				</b>
			</p>
			<table class=TableNormal border=1 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none;width: 202mm;'>
				@if($beneficiaries->some(function($beneficiary) {
					return $beneficiary->type === 'contribution';
				}))
					<!-- Al menos un objeto cumple con la condición -->

				<tr style='height:11.0pt'>
					<td width=748 colspan=3 valign=top style='width:561.0pt;border:solid windowtext 1.0pt;
						background:#D1CCCC;padding:0cm 0cm 0cm 0cm;
						height:11.0pt'>
						<p class=TableParagraph align=center style='margin-top:2.0pt;margin-right:
							210.25pt;margin-bottom:0cm;margin-left:211.25pt;margin-bottom:.0001pt;
							text-align:center;line-height:7.95pt;'>
							<span style='font-size:8.0pt;color:black;'>APORTACIONES DEL EMPLEADOR</span>
						</p>
					</td>
				</tr>

				<tr style='height:9.0pt'>
					<td width=133 valign=top style='width:100.0pt;border:solid windowtext 1.0pt;
						border-top:none;
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph align=center style='margin-top:.9pt;margin-right:
							35.95pt;margin-bottom:0cm;margin-left:36.9pt;margin-bottom:.0001pt;
							text-align:center;line-height:7.1pt;'>
							<span style='font-size:7.5pt;letter-spacing:-.1pt;'> </span>
						</p>
					</td>
					<td width=440 valign=top style='width:330.0pt;border-top:none;border-left:
						none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph style='margin-top:.9pt;line-height:7.1pt;'>
							<span  style='font-size:7.5pt;
								letter-spacing:-.1pt;'>Descripción</span>
							<span 
								style='font-size:7.5pt;'>
								
							</span>
						</p>
					</td>
					<td width=175 valign=top style='width:131.0pt;border-top:none;border-left:
						none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						
						padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph align=center style='margin-top:.4pt;margin-right:
							48.25pt;margin-bottom:0cm;margin-left:49.2pt;margin-bottom:.0001pt;
							text-align:center;line-height:7.6pt;'>
							<span
								 style='font-size:7.5pt;'>Monto<span
								style='letter-spacing:-.3pt'> </span><span style='letter-spacing:-.25pt'>S/</span></span>
							<span
								 style='font-size:7.5pt;'>
								
							</span>
						</p>
					</td>
				</tr>
                @else
					<!-- Ningún objeto cumple con la condición -->
				@endif             
                <!-- //Aportes del empleador -->
                @foreach($beneficiaries as $beneficiary)
                    @if($beneficiary->beneficiary->type == 'contribution')
                        @if($beneficiary->is_percentage == 1)
                        <tr style='height:9.0pt'>
                            <td width=133 valign=top style='width:100.0pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=center style='margin-top:1.1pt;margin-right:40.75pt;margin-bottom:0cm;margin-left:41.75pt;margin-bottom:.0001pt;text-align:center;line-height:6.9pt;'>
                                    <span style='font-size:7.0pt;letter-spacing:-.25pt'> </span>
                                </p>
                            </td>
                            <td width=440 valign=top style='width:330.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph style='margin-top:1.35pt;line-height:6.65pt;'>
                                    <span  style='font-size:6.5pt;'>
                                    {{ $beneficiary->beneficiary->name }} (%)
                                    <!-- {{ $beneficiary->beneficiary->name }} ({{ $beneficiary->amount }} %) -->
                                    </span>
                                </p>
                            </td>
                            <td width=175 valign=top style='width:131.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:-1.45pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right'>
                                    <span  style='font-size:6.5pt;letter-spacing:-.1pt'>
                                    {{ trans('default.like_'.$settings->currency_position, [
                                                'symbol' => $settings->currency_symbol,
                                                'amount' =>  $beneficiary->amount
                                                    ]) }}
                                    </span>
                                </p>
                            </td>
                        </tr>
                        @else

                        @php
                            settings()
                        @endphp

                        <tr style='height:9.0pt'>
                            <td width=133 valign=top style='width:100.0pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=center style='margin-top:1.1pt;margin-right:40.75pt;margin-bottom:0cm;margin-left:41.75pt;margin-bottom:.0001pt;text-align:center;line-height:6.9pt;'>
                                    <span style='font-size:7.0pt;letter-spacing:-.25pt'> </span>
                                </p>
                            </td>
                            <td width=440 valign=top style='width:330.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph style='margin-top:1.35pt;line-height:6.65pt;'>
                                    <span  style='font-size:6.5pt;'>
                                    {{ $beneficiary->beneficiary->name }}
                                    </span>
                                </p>
                            </td>
                            <td width=175 valign=top style='width:131.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
                                <p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:-1.45pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right'>
                                    <span  style='font-size:6.5pt;letter-spacing:-.1pt'>
                                    {{ trans('default.like_'.$settings->currency_position, [
                                        'symbol' => $settings->currency_symbol,
                                        'amount' =>  $beneficiary->amount
                                            ]) }}
                                    </span>
                                </p>
                            </td>
                        </tr>
                        @endif
                    @endif
                @endforeach


				<tr style='height:9.0pt;display:none;'>
					<td width=573 colspan=2 valign=top style='width:430.0pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph style='line-height:6.9pt;'>
							<span style='font-size:7.0pt;letter-spacing:-.1pt'>TOTAL:</span>
						</p>
					</td>
					<td width=175 valign=top style='width:131.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm;height:9.0pt'>
						<p class=TableParagraph align=right style='margin-top:1.5pt;margin-right:-1.45pt;margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right'>
							<b><span style='font-size:6.5pt;'>
                                {{ trans('default.like_'.$settings->currency_position, [
                                'symbol' => $settings->currency_symbol,
                                'amount' =>   number_format(round($totalContribution, 2), 2, '.', '')
                                    ]) }}
                                </span>
                            </b>
						</p>
					</td>
				</tr>

			</table>
			
            @if(property_exists($payslip_settings, 'note') && $payslip_settings->note)
                <!-- <hr class="mb-0"> -->
                <small class="font-italic" Style="font-size:7.5pt;color:#6c757d;"><b>* </b>{{ $payslip_settings->note }}</small>
            @endif

			<p class=MsoNormal align=center style='margin-top:18mm;margin-right:0cm;
				margin-bottom:0cm;margin-left:7.1pt;margin-bottom:.0001pt;text-align:center'>
				<span
					 style='font-size:7.5pt;'>
					--------------------------------------------
					
				</span>
			</p>
            <p class=MsoNormal align=center style='margin-top:.75pt;margin-right:0cm;
				margin-bottom:0cm;margin-left:7.1pt;margin-bottom:.0001pt;text-align:center'>
				<span style='font-size:7.5pt;'>
                EMPLEADOR
				</span>
			</p>
			<p class=MsoNormal style='margin-top:.75pt;margin-right:0cm;margin-bottom:0cm;
				margin-left:170.7pt;margin-bottom:.0001pt'>
				<span  style='font-size:7.5pt;'>
					
				</span>
			</p>
			<p class=MsoNormal style='margin-top:.75pt;margin-right:0cm;margin-bottom:0cm;
				margin-left:170.7pt;margin-bottom:.0001pt'>
				<span  style='font-size:7.5pt;'>
					Boleta emitida según D. S. <span class=SpellE>N°</span>
					009-2011-TR y D.L <span class=SpellE>N°</span> 1310-<span style='letter-spacing:-.2pt'>2016</span>
				</span>
			</p>
		</div>
	</body>
</html>