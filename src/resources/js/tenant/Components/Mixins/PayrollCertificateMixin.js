import {PAYROLL_CERTIFICATE} from "../../Config/ApiUrl";
import {getDateRangeString, isValidDate} from "../../../common/Helper/Support/DateTimeHelper";
import {DepartmentFilterMixin, UserFilterMixin} from "./FilterMixin";
import NavFilterMixin from "./NavFilterMixin";
import StatusMixin from "../../../common/Mixin/Global/StatusMixin";
import {formatCurrency, numberFormatter} from "../../../common/Helper/Support/SettingsHelper";
import {ucFirst} from "../../../common/Helper/Support/TextHelper";

export default {
    mixins:[DepartmentFilterMixin, UserFilterMixin, NavFilterMixin, StatusMixin],
    data(){
        return {
            numberFormatter,
            tableOptions: {
                tableData: 0,
                afterRequestSuccess: ({data}) => {
                    this.tableData = data.data.length;
                },
                name: 'Constancias',
                url: PAYROLL_CERTIFICATE,
                showHeader: true,
                enableRowSelect: false,
                showCount: false,
                showClearFilter: false,
                showFilter: false,
                showSearch: false,
                tablePaddingClass: "px-0",
                tableShadow: false,
                managePagination: true,
                datatableWrapper: false,
                filters: [],
                columns: [
                    {
                        title: 'Constancia',
                        type: 'text',
                        key: 'name',
                        isVisible: true,
                    },
                    {
                        title: 'Expediente',
                        type: 'text',
                        key: 'ref_exp_number',
                        isVisible: true,
                    },
                    {
                        title: 'Usuario',
                        type: 'component',
                        key: 'user',
                        componentName: 'app-attendance-employee-media-object',
                    },
                    {
                        title: 'Nro DNI/ Nro RUC',
                        type: 'custom-html',
                        key: 'user',
                        isVisible: true,
                        modifier: (val, row) => {
                            return `<span class="min-width-130 d-block">${val.profile.document_type.name}: ${val.profile.document_number}</span><p class="pt-1 font-size-90 text-muted">RUC: ${val.profile.ruc_number}</p>`;
                        }
                    },
                    {
                        title: 'Periodo',
                        type: 'custom-html',
                        key: 'user',
                        isVisible: true,
                        modifier: (val, row) => {
                            // Verifica si row.start_date y row.end_date son fechas válidas
                            if (isValidDate(row.start_date) && isValidDate(row.end_date)) {
                                // Si ambas son fechas válidas, devuelve el rango de fechas
                                return `<span class="min-width-130 d-block">${getDateRangeString(row.start_date, row.end_date)}</span>`;
                            } else {
                                // Si alguna de las fechas no es válida, devuelve "Total Registrado"
                                return `<span class="min-width-130 d-block">Total Registrado</span>`;
                            }
                        }
                    },
                    // {
                    //     title: 'Reg. Pen.',
                    //     type: 'custom-html',
                    //     key: 'payrun',
                    //     isVisible: true,
                    //     modifier: (val, row) => {
                    //         return `<p class="min-width-130 d-block">${row.regimen_pension}</p>`;
                    //     }
                    // },
                    // {
                    //     title: this.$t('payrun_period'),
                    //     type: 'custom-html',
                    //     key: 'payrun',
                    //     isVisible: true,
                    //     modifier: payrun => {
                    //         if(payrun.data){
                    //             return this.putIntoSpan(JSON.parse(payrun.data).period ? ucFirst(JSON.parse(payrun.data).period) : '-');
                    //         }
                    //     }
                    // },
                    // {
                    //     title: this.$t('payrun_type'),
                    //     type: 'custom-html',
                    //     key: 'payrun',
                    //     isVisible: true,
                    //     modifier: payrun => {
                    //         if(payrun.data){
                    //             return this.putIntoSpan(JSON.parse(payrun.data).type ? ucFirst(JSON.parse(payrun.data).type) : '-');
                    //         }
                    //     }
                    // },
                    // {
                    //     title: this.$t('status'),
                    //     type: 'custom-html',
                    //     key: 'status',
                    //     isVisible: true,
                    //     modifier: (status, row) => {
                    //         return `<div class="d-flex"><span class="mr-2 badge badge-sm badge-pill badge-${status.class}">
                    //             ${status.translated_name}
                    //         </span>${parseInt(row.conflicted) ?
                    //             `<span class="badge badge-sm badge-pill badge-danger">
                    //             ${this.$t('conflicted')}
                    //         </span>` : ''}</div>`
                    //     }
                    // },
                    {
                        title: 'Descargar',
                        type: 'button',
                        className: 'btn btn-success d-inline-flex align-items-center',
                        icon: 'file',
                        actionName: 'view',
                        modifier: (row) => {
                            return 'Ver';
                        }
                    },
                    // {
                    //     title: this.$t('email'),
                    //     type: 'button',
                    //     className: 'btn btn-secondary d-inline-flex align-items-center',
                    //     key: 'status',
                    //     icon: 'send',
                    //     actionName: 'send',
                    //     modifier: (status) => {
                    //         return status.name !== 'status_generated' ? this.$t('resend') : this.$t('send')
                    //     }
                    // },
                    {
                        title: this.$t('actions'),
                        type: 'action',
                        key: 'invoice',
                        isVisible: true
                    },
                ],
                paginationType: "pagination",
                responsive: true,
                rowLimit: 10,
                showAction: true,
                orderBy: 'desc',
                actionType: "dropdown",
                queryParams: false,
                actions: [
                    // {
                    //     title: this.$t('edit'),
                    //     actionName: 'edit',
                    //     modifier: row => this.$can('edit_payslip')
                    // },
                    // {
                    //     title: this.$t('manage_conflict'),
                    //     actionName: 'manage_conflict',
                    //     modifier: row => parseInt(row.conflicted) && this.$can('manage_payslip_confliction')
                    // },
                    // {
                    //     title: this.$t('view_pdf'),
                    //     actionName: 'view_pdf',
                    //     modifier: row => this.$can('view_payslip_pdf')
                    // },
                    // {
                    //     title: this.$t('download_pdf'),
                    //     actionName: 'download_pdf',
                    //     modifier: row => this.$can('view_payslip_pdf')
                    // },
                    {
                        title: this.$t('delete'),
                        actionName: 'delete',
                        modifier: row => this.$can('delete_payslip')
                    },
                ],
            },
            options: {
                showFilter: true,
                showSearch: true,
                showClearFilter: true,
                filters: [
                    {
                        title: this.$t('created'),
                        type: "range-picker",
                        key: "date",
                        option: ["today", "thisMonth", "last7Days", "thisYear"]
                    },
                    // {
                    //     title: this.$t('status'),
                    //     type: "multi-select-filter",
                    //     key: "status",
                    //     option:[],
                    //     listValueField: 'translated_name',
                    // },
                    {
                        title: 'Usuario',
                        type: "multi-select-filter",
                        key: "users",
                        option: [],
                        listValueField: 'full_name',
                        permission: this.$can('view_departments')
                    },
                ],
            },
            periodFilters: [
                {
                    id: 'thisMonth',
                    value: this.$t('this_month')
                },
                {
                    id: 'lastMonth',
                    value: this.$t('last_month')
                },
                {
                    id: 'thisYear',
                    value: this.$t('this_year')
                },
                {
                    id: 'lastYear',
                    value: this.$t('last_year')
                },
                {
                    id: 'total',
                    value: this.$t('total')
                },
            ]
        }
    },
    computed:{
        currencySymbol(){
            return window.settings.currency_symbol;
        }
    },
    methods:{
        putIntoSpan(data){
            return `<span class="min-width-100 d-block">${data}</span>`
        }
    }
}