import {DOCUMENT_TYPES} from '../../Config/ApiUrl'

export default {
    data() {
        return {
            options: {
                name: this.$t('leave_type'),
                url: DOCUMENT_TYPES,
                showHeader: true,
                tableShadow:false,
                tablePaddingClass:'pt-primary',
                columns: [
                    {
                        title: 'Nombre abrev.',
                        type: 'text',
                        key: 'name',
                        isVisible: true,
                    },
                    {
                        title: 'Descripción',
                        type: 'text',
                        key: 'description',
                        isVisible: true,
                    },
                    {
                        title: 'Código AIRHSP',
                        type: 'text',
                        key: 'airhsp_code',
                        isVisible: true,
                    },
                    {
                        title: this.$t('enabled'),
                        type: 'custom-html',
                        key: 'is_enabled',
                        modifier: (value) => {
                            return parseInt(value) === 1 ? this.$t("yes") : this.$t("no");
                        }
                    },
                    {
                        title: this.$t('actions'),
                        type: 'action',
                        isVisible: true
                    },
                ],
                filters: [
                    // {
                    //     title: this.$t('created'),
                    //     type: "range-picker",
                    //     key: "date",
                    //     option: ["today", "thisMonth", "last7Days", "thisYear"]
                    // },
                ],
                paginationType: "pagination",
                responsive: true,
                rowLimit: 10,
                showAction: true,
                orderBy: 'desc',
                actionType: "default",
                actions: [
                    {
                        title: this.$t('edit'),
                        icon: 'edit',
                        type: 'modal',
                        component: 'app-document-types-create-edit',
                        modalId: 'document-types-modal',
                        url: DOCUMENT_TYPES,
                        name: 'edit',
                        modifier: row => this.$can('update_leave_types')
                    },
                    {
                        title: this.$t('delete'),
                        name: 'delete',
                        icon: 'trash-2',
                        modalClass: 'warning',
                        url: DOCUMENT_TYPES,
                        modifier: row => this.$can('delete_leave_types')
                    },
                ],
            }
        }
    }
}
