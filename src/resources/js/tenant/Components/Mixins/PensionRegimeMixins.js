import {PENSION_REGIMES} from '../../Config/ApiUrl'

export default {
    data() {
        return {
            options: {
                name: this.$t('leave_type'),
                url: PENSION_REGIMES,
                showHeader: true,
                tableShadow:false,
                tablePaddingClass:'pt-primary',
                columns: [
                    {
                        title: 'CÓDIGO',
                        type: 'text',
                        key: 'code',
                        isVisible: true,
                    },
                    {
                        title: 'DESCRIPCIÓN',
                        type: 'text',
                        key: 'name',
                        isVisible: true,
                    },
                    {
                        title: 'TIPO',
                        type: 'text',
                        key: 'type',
                        isVisible: true,
                    },
                    {
                        title: 'APORTE OBLIGATORIO AL FONDO DE PENSIONES (%)',
                        type: 'text',
                        key: 'aporte_obligatorio',
                        isVisible: true,
                    },
                    {
                        title: 'COMISIÓN SOBRE FLUJO (%)',
                        type: 'text',
                        key: 'comision_flujo',
                        isVisible: true,
                    },
                    {
                        title: 'PRIMA DE SEGUROS (%)',
                        type: 'text',
                        key: 'prima_seguro',
                        isVisible: true,
                    },
                    {
                        title: 'REMUNERACIÓN MÁXIMA ASEGURABLE',
                        type: 'text',
                        key: 'maxima_asegurable',
                        isVisible: true,
                    },
                    {
                        title: 'HABILITADO',
                        type: 'custom-html',
                        key: 'is_active',
                        modifier: (value) => {
                            return parseInt(value) === 1 ? this.$t("yes") : this.$t("no");
                        }
                    },
                    {
                        title: this.$t('actions'),
                        type: 'action',
                        isVisible: true
                    },
                ],
                filters: [
                    // {
                    //     title: this.$t('created'),
                    //     type: "range-picker",
                    //     key: "date",
                    //     option: ["today", "thisMonth", "last7Days", "thisYear"]
                    // },
                ],
                paginationType: "pagination",
                responsive: true,
                rowLimit: 10,
                showAction: true,
                orderBy: 'desc',
                actionType: "default",
                actions: [
                    {
                        title: this.$t('edit'),
                        icon: 'edit',
                        type: 'modal',
                        component: 'app-document-types-create-edit',
                        modalId: 'document-types-modal',
                        url: PENSION_REGIMES,
                        name: 'edit',
                        modifier: row => this.$can('update_leave_types')
                    },
                    {
                        title: this.$t('delete'),
                        name: 'delete',
                        icon: 'trash-2',
                        modalClass: 'warning',
                        url: PENSION_REGIMES,
                        modifier: row => this.$can('delete_leave_types')
                    },
                ],
            }
        }
    }
}
