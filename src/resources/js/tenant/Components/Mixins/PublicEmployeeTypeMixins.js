import {PUBLIC_EMPLOYEE_TYPES} from '../../Config/ApiUrl'

export default {
    data() {
        return {
            options: {
                name: this.$t('leave_type'),
                url: PUBLIC_EMPLOYEE_TYPES,
                showHeader: true,
                tableShadow:false,
                tablePaddingClass:'pt-primary',
                columns: [
                    {
                        title: 'CÓDIGO',
                        type: 'text',
                        key: 'code',
                        isVisible: true,
                    },
                    {
                        title: 'NOMBRE',
                        type: 'text',
                        key: 'name',
                        isVisible: true,
                    },
                    {
                        title: 'DESCRIPCIÓN',
                        type: 'text',
                        key: 'description',
                        isVisible: true,
                    },
                    {
                        title: this.$t('actions'),
                        type: 'action',
                        isVisible: true
                    },
                ],
                filters: [
                    // {
                    //     title: this.$t('created'),
                    //     type: "range-picker",
                    //     key: "date",
                    //     option: ["today", "thisMonth", "last7Days", "thisYear"]
                    // },
                ],
                paginationType: "pagination",
                responsive: true,
                rowLimit: 10,
                showAction: true,
                orderBy: 'desc',
                actionType: "default",
                actions: [
                    {
                        title: this.$t('edit'),
                        icon: 'edit',
                        type: 'modal',
                        component: 'app-public-employee-types-create-edit',
                        modalId: 'public-employee-types-modal',
                        url: PUBLIC_EMPLOYEE_TYPES,
                        name: 'edit',
                        modifier: row => this.$can('update_leave_types')
                    },
                    {
                        title: this.$t('delete'),
                        name: 'delete',
                        icon: 'trash-2',
                        modalClass: 'warning',
                        url: PUBLIC_EMPLOYEE_TYPES,
                        modifier: row => this.$can('delete_leave_types')
                    },
                ],
            }
        }
    }
}
